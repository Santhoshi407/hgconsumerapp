/**
 * Created by sumit on 28/10/16.
 */
var udata;
var savedcards = {
    "statusCode":200,
    "message":"Success",
    "data":{
        "cards":[
            {
                "payfortId" : "bhe3452ws",
                "Digit" : "443913******0991",
                "_id":"789joijoil8485"
            },
            {
                "payfortId" : "bhe3452ws",
                "Digit" : "548913******0991",
                "_id":"789joijoil8485"
            },
            {
                "payfortId" : "bhe3452ws",
                "Digit" : "346913******0991",
                "_id":"789joijoil8485"
            }
        ]
    }
};

function getCardDetails(){
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "../index.html";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getAllMyCard", params, function(err,results){
        if(!err){
            var data = {}, html, cardArray = [];
            var cardNo = results.data.cards;
            if (cardNo.length > 0) {
                data.cards = results.data.cards;
                for (var k in data.cards) {
                    var cardData = {};
                    cardData["_id"] = data.cards[k]["_id"];
                    cardData["payfortId"] = data.cards[k]["payfortId"];
                    cardData["Digit"] = data.cards[k]["Digit"];
                    $('#checkout_card_number').val(cardData["Digit"]);
                    var $cardinput = $('#checkout_card_number');

                    $('#checkout_card_number').validateCreditCard(function(result) {
                        //console.log(result);
                        if (result.card_type != null)
                        {
                            switch (result.card_type.name)
                            {
                                case "visa":
                                    cardData["icon"] = "fa fa-cc-visa";
                                    break;

                                case "visa_electron":
                                    cardData["icon"] = "fa fa-cc-visa";
                                    break;

                                case "mastercard":
                                    cardData["icon"] = "fa fa-cc-mastercard";
                                    break;

                                case "maestro":
                                    cardData["icon"] = "fa fa-cc-maestro";
                                    break;

                                case "discover":
                                    cardData["icon"] = "fa fa-cc-discover";
                                    break;

                                case "amex":
                                    cardData["icon"] = "fa fa-cc-amex";
                                    break;

                                default:
                                    $cardinput.css('background-position', '3px 3px');
                                    break;
                            }
                        } else {
                            $cardinput.css('background-position', '3px 3px');
                        }
                    });
                    cardArray.push(cardData);
                }
                console.log(cardArray);
                data.cards = cardArray;
                html = Mustache.render(savedCard_template, data);
                $("#savedcards").html(html);
                $.LoadingOverlay("hide", true);
            } else {
                $("#savedcards").html("<p class='text-center' style='font-size:20px;color:#a2a2a2;'>You don't have any card saved.</p>");
                $.LoadingOverlay("hide", true);
            }
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function deleteSavedCards(id) {
    var data = new FormData();
    data.append("cardId",id);
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/deleteCards", data,function(err, results){
        if (!err) {
            getCardDetails();
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function updateDefaultCard(id) {
    var data = new FormData();
    data.append("cardId", id);
    data._method = "PUT";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/setDefaultCard", data, function(err, results){
        if (!err) {
            $.LoadingOverlay("hide", true);
            $.growl.notice({message : "You have set your default card"});
            var message = {"msg":"You have set your default card"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}
$(document).ready(function(){
    getCardDetails();
    $("#updateDefaultcard").click(function(){
        var cardid = $(this).attr("data-id");
        updateDefaultCard(cardid);
    });
});

var savedCard_template =
    '{{#cards}}<div class="col-xs-12 col-sm-12 col-md-12 column saved-cards-div">\
        <p>\
            <span>\
                <input type="radio" name="saved-card" value="saved-card" data-id="{{payfortId}}" data-card="{{_id}}">\
                <span class="card-number">{{Digit}}</span>\
            </span>\
            <span class="pull-right">\
                <span class="{{icon}} fa-2x card-icon"></span>\
                <button class="saved-card-delete" data-id="{{_id}}">DELETE</button>\
            </span>\
        </p>\
    </div>{{/cards}}';