
/**
 * Created by sumit on 13/10/16.
 */
var params = {}, date;


function updateProfile() {
    var data = new FormData();

    params.name = $("#name").val();
    params.countryCode = "+971";
    params.phoneNo = $("#phoneNo").val();
    params.language = $("#language").val();
    params.dob = ($("#dob").val()).split('-').reverse().join('-');
    params.nationality = $("#nationality").val();

    if (!params.name) {
        // $.growl.error({message : "Please enter your name"});
        var message = {"msg":"Please enter your name"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (!params.countryCode) {
        // $.growl.error({message : "Please enter your name"});
        var message = {"msg":"Please enter your name"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (!params.phoneNo) {
        // $.growl.error({message : "Please enter your phone no."});
        var message = {"msg":"Please enter your phone no."}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
     if (!params.language) {
        // $.growl.error({message : "Language cannot be empty"});
        var message = {"msg":"Language cannot be empty"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (!params.dob) {
        // $.growl.error({message : "Please enter your DOB"});
        var message = {"msg":"Please enter your DOB"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (!params.nationality) {
        // $.growl.error({message : "Please enter your nationality"});
        var message = {"msg":"Please enter your nationality"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if ($("#file-input").prop("files")[0]) {
        data.append("profilePic", $("#file-input").prop("files")[0]);
        $("#user-img").html($("#file-input").prop("files")[0]);
    }
    $.LoadingOverlay("show");
    data.append("name",params.name);
    data.append("countryCode",params.countryCode);
    data.append("phoneNo", params.phoneNo);
    data.append("language",params.language);
    data.append("dob",params.dob);
    data.append("nationality", params.nationality);
    data._method = "PUT";
    data.Auth = udata.data.accessToken;
    callApi.queryUrl(apiUrl + "/api/customer/updateProfile", data, function(err, response){
        if (!err){
            // $.growl.notice({message : msg.MESSAGE_PROFILE_UPDATE});
            var message = {"msg":msg.MESSAGE_PROFILE_UPDATE}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            localStorage.setItem("h_user",response);
            updateUserDetails();
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function updateUserDetails() {
    var userinfo = localStorage.getItem("h_user");
    //console.log(userinfo)
    if (userinfo && userinfo !== "null") {
        userinfo = JSON.parse(userinfo);
        userinfo = userinfo.data.userDetails;
        $.LoadingOverlay("show");
        if (userinfo.profilePicURL && userinfo.profilePicURL.thumbnail) {
            $("#user-img").attr("src",userinfo.profilePicURL.thumbnail);
        } else {
            $("#user-img").attr("src","../images/genieicon.png");
        }
        date = new Date(userinfo.dob).toISOString();
        date = date.split("T")[0].split('-').reverse().join('-');
        $("#name").val(userinfo.name);
        $("#countryCode").val(userinfo.countryCode);
        $("#phoneNo").val(userinfo.phoneNo);
        $("#language").val(userinfo.language);
        $("#dob").val(date);
        $("#nationality").val(userinfo.nationality);
        $("#email").val(userinfo.email);
        $.LoadingOverlay("hide", true);
    } else {
        window.location.href = "../index.html";
    }
    $("#dob").mouseenter(function(){
        $(this).attr("type", "date");
    });
    $("#dob").mouseleave(function(){
        //date = $("#dob").val();
        $(this).attr("type", "text");
        if (date != $("#dob").val() && $("#dob").val() != "") {
            $("#dob").val($("#dob").val().split('-').reverse().join('-'));
        } else {
            $("#dob").val(date);
        }
    });
}
$('#file-input').change( function(event) {
    $("#user-img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
});


$(document).ready(function(){
    $("#update-profile").click(function(){
        updateProfile();
    });
    updateUserDetails();
});
