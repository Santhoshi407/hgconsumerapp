


/**
 * Created by sumit on 19/10/16.
 */
var udata, bookingData,
    subcategory = [],
    monthArray = ["Jan", "Feb", "Mar", "Apr","May", "June","July", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dayArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri","Sat"];
var COMPLETED_JOB_TEXT = "The job has now been completed, for any complaints or feedback, please call HomeGenie Support Team on +97144489595 or support@homegenie.me",
    GENIE_NOT_ASSIGNED = "Apologies, we were not able to assign a Genie to your booking. We look forward to serving you again !",
    GENIE_ASSIGNED = "In case you wish to reach your Genie and are not able to get through, you can contact his supervisor SUPERVISIOR on +971PHONENO. For any complaints or feedback, please call HomeGenie Support Team on +97144489595 or support@homegenie.me",
    ADVANCE_PAY_NOTE = "There is an advance payment of AED AMOUNT applicable to this service which will be charged following the acceptance of estimate.";


function autocompleteSearch() {
    $('#search-ongoing, #search-completed').devbridgeAutocomplete({
        lookup: subcategory,
        minChars: 1,
        onSelect: function (suggestion) {
            var bookingsearchinput = suggestion.value;
            $('.ongoingbookingsearch').each(function () {
                var service = $(this).attr("data");
                var re = new RegExp(bookingsearchinput, 'gi');

                if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                    $(this).addClass('hidden');
                } else {
                    $(this).removeClass('hidden');
                }
                if ((service.trim()).match(re)) {
                    $(this).removeClass('hidden');
                }
            });
            $('.completedbookingsearch').each(function () {
                var service = $(this).attr("data");
                var re = new RegExp(bookingsearchinput, 'gi');
                if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                    $(this).addClass('hidden');
                }
                else {
                    $(this).removeClass('hidden');
                }
                if ((service.trim()).match(re)) {
                    $(this).removeClass('hidden');
                }
            });
        },
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Sorry, no matching results'
    });
}
function updateOngoingAndCompletedBooking(results) {
    var data = {}, htmlongoing, htmlcompleted, bookingOngoing, bookingPast;
    bookingOngoing = results.data.upcomingAppointment;
    bookingPast = results.data.pastBooking;
    for (var k in bookingOngoing) {
        var slot, date, month, day, year, bookingdate;
        slot = {
            "8":"8AM - 10AM",
            "10":"10AM - 12PM",
            "12":"12PM - 2PM",
            "14":"2PM - 4PM",
            "16":"4PM - 6PM",
            "18":"6PM - 8PM"
        };
        bookingOngoing[k]["slotBooked"]["slot"] = slot[bookingOngoing[k]["slotBooked"]["slot"]];
        date = new Date(bookingOngoing[k]["scheduleDate"]);
        month = monthArray[date.getMonth()];
        day = dayArray[date.getDay()];
        year = date.getFullYear();
        date = date.getDate();
        bookingOngoing[k]["scheduleDate"] = day + " " + date + " " + month + " " + year + " | " + bookingOngoing[k]["slotBooked"]["slot"];
        bookingOngoing[k]["dateScheduled"] = bookingOngoing[k]["slotBooked"]["slot"] + ", " + month + " " + date;
        bookingdate = bookingOngoing[k]["utc_timing"]["requestedTime"];
        bookingdate = new Date(bookingdate).toLocaleString();
        bookingdate = bookingdate.split(",");

        bookingOngoing[k]["utc_timing"]["requestedTime"] = bookingdate;
        if (bookingOngoing[k]["favouriteGenie"] === "FALSE") {
            bookingOngoing[k]["favouriteGenie"] = false;
        }
        //for total charges
        var totalCharge = bookingOngoing[k]["charges"]["totalCharges"];
        if (!totalCharge && bookingOngoing[k]["status"] !== "CANCELLED") {
            bookingOngoing[k]["charges"]["totalCharges"] = "To be decided";
        }
        if (bookingOngoing[k]["charges"]["unitCharges"]) {
            bookingOngoing[k]["services"] = "Fixed price";
            bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["unitCharges"];
        } else {
            bookingOngoing[k]["services"] = "Inspection based";
        }
        if (bookingOngoing[k]["charges"]["estimateCharges"]) {
            bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["estimateCharges"];
        }
        if (totalCharge == bookingOngoing[k]["charges"]["cancellationCharges"]) {
            bookingOngoing[k]["charges"]["totalCharges"] = totalCharge;
        }
        if (bookingOngoing[k]["payment"] && bookingOngoing[k]["payment"]["payment_type"] === "null") {
            bookingOngoing[k]["payment"]["payment_type"] = "On Completion";
        }


        bookingOngoing[k]["Payment"] = true;
        var status = bookingOngoing[k]["status"];
        switch(status) {
            case "REQUESTED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = true;
                break;
            case "ASSIGNED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                if(bookingOngoing[k].advancePayment && bookingOngoing[k]["charges"]["advanceCharges"]) {
                    bookingOngoing[k]["Cancel"] = false;
                    bookingOngoing[k]["Accept"] = true;
                } else {
                    bookingOngoing[k]["Cancel"] = true;
                }
                break;
            case "CANCELLED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                if(bookingOngoing[k]["charges"]["totalCharges"] == 0 || !bookingOngoing[k]["driverData"]) {
                    bookingOngoing[k]["Payment"] = false;
                }
                break;
            case "PAYMENT_PENDING":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = false;
                bookingOngoing[k]["Cancel"] = false;
                /*if(bookingOngoing[k]["charges"]["totalCharges"] == 0) {
                    bookingOngoing[k]["Cancel"] = true;
                }*/
                break;
            case "IN_SERVICE":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "RATING":
                bookingOngoing[k]["RateGenie"] = true;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = false;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "COMPLAINT_REGISTERED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "INSPECTION":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                if(bookingOngoing[k].advancePayment && !bookingOngoing[k]["charges"]["unitCharges"]) {
                    bookingOngoing[k]["Accept"] = true;
                }
                break;
            case "RESCHEDULED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "REJECTED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["callOutCharges"];
                break;
            case "ENROUTE":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = true;
                break;
            case "UNFINISHED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "REESTIMATE":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            default:
                break;
        }
    }
    for (var k in bookingPast) {
        var slot = {
            "8":"8AM - 10AM",
            "10":"10AM - 12PM",
            "12":"12PM - 2PM",
            "14":"2PM - 4PM",
            "16":"4PM - 6PM",
            "18":"6PM - 8PM"
        };
        if (bookingPast[k]["payment_status"] == "PENDING") {
            bookingPast[k]["payment_status"] = "SETTLED";
        }
        bookingPast[k]["slotBooked"]["slot"] = slot[bookingPast[k]["slotBooked"]["slot"]];
        var date = bookingPast[k]["scheduleDate"];
        date = new Date(date);
        month = monthArray[date.getMonth()];
        day = dayArray[date.getDay()];
        year = date.getFullYear();
        date = date.getDate();
        bookingPast[k]["scheduleDate"] = day + " " + date + " " + month + " " + year + " | " + bookingPast[k]["slotBooked"]["slot"];
        bookingPast[k]["dateScheduled"] = bookingPast[k]["slotBooked"]["slot"] + ", " + month + " " + date;
        var bookingdate = bookingPast[k]["utc_timing"]["requestedTime"];
        bookingdate = new Date(bookingdate).toLocaleString();
        bookingdate = bookingdate.split(",");

        if (subcategory.indexOf(bookingPast[k]["dateScheduled"]) === -1) {
            subcategory.push(bookingPast[k]["dateScheduled"]);
        }
        bookingPast[k]["utc_timing"]["requestedTime"] = bookingdate;
        if (bookingPast[k]["favouriteGenie"] === "FALSE") {
            bookingPast[k]["favouriteGenie"] = false;
        }
        if (bookingPast[k]["charges"]["unitCharges"]) {
            bookingPast[k]["services"] = "Fixed price";
        } else {
            bookingPast[k]["services"] = "Inspection based";
        }
        if (bookingPast[k]["payment"] && bookingPast[k]["payment"]["payment_type"] === "null") {
            bookingPast[k]["payment"]["payment_type"] = "";
        }
    }
    data.ongoing = bookingOngoing;
    data.completed = bookingPast;

    if ((data.ongoing).length > 0) {
        //console.log(data);
        //console.log("booking");
        htmlongoing = Mustache.render(ongoingBooking_template, data);
        $("#ongoing").html(htmlongoing);
    } else {
        $("#ongoing").html("<p class='booking-msg'>You have not booked any services as yet.</p>");
    }
    if ((data.completed).length > 0) {
        htmlcompleted = Mustache.render(completedBooking_template, data);
        $("#completed").html(htmlcompleted);
    } else {
        $("#completed").html("<p class='booking-msg'>You don't have any services fully delivered yet.</p>");
    }


    var params = {};
    callApi.getUrl(apiUrl + "/api/subcategory/getAllSubCategories",params,function(err, response){
        if (!err) {
            //console.log(response.data);
            var subcategoryArray = response.data;
            for(var k in subcategoryArray) {
                subcategory.push(subcategoryArray[k].subCategoryName);
            }
            autocompleteSearch();
        } else {
            callApi.error(err);
        }
    });

    initOngoingBookingSearch();
    $.LoadingOverlay("hide", true);

}

function getmybookings() {
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "/";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getmybookings", params, function(err, result){
        if (!err) {
            bookingData = result;
            var searchField = bookingData.data.upcomingAppointment;
            var searchField1 = bookingData.data.pastBooking;
            if (searchField.length > 0) {
                for (var k in searchField) {
                    if (searchField[k]["status"]) {
                        if (subcategory.indexOf(searchField[k]["status"]) === -1) {
                            subcategory.push(searchField[k]["status"]);
                        }
                    }
                    if (searchField[k]["uniqueCode"]) {
                        subcategory.push(searchField[k]["uniqueCode"]);
                    }
                    if (searchField[k] && searchField[k]["driverData"] && searchField[k]["driverData"]["name"]) {
                        if (subcategory.indexOf(searchField[k]["driverData"]["name"]) === -1) {
                            subcategory.push(searchField[k]["driverData"]["name"]);
                        }
                    }
                }
            }

            if (searchField1.length > 0) {
                for (var k in searchField1) {
                    if (searchField1[k]["status"]) {
                        if (subcategory.indexOf(searchField1[k]["status"]) === -1) {
                            subcategory.push(searchField1[k]["status"]);
                        }
                    }
                    if (searchField1[k]["uniqueCode"]) {
                        subcategory.push(searchField1[k]["uniqueCode"]);
                    }
                    if (searchField1[k] && searchField1[k]["driverData"] && searchField1[k]["driverData"]["name"]) {
                        if (subcategory.indexOf(searchField1[k]["driverData"]["name"]) === -1) {
                            subcategory.push(searchField1[k]["driverData"]["name"]);
                        }
                    }
                }
            }
            updateOngoingAndCompletedBooking(result);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function getBookingDetails(id, bookingstatus) {
    //$("body").addClass("modalOpen");
    var appntmntId = id;
    var params = new FormData();
    params.append("appointmentId",appntmntId);
    params.Auth = udata.data.accessToken;
    params._method = "POST";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/getJobDetails", params, function(err, response){
        if (!err) {

            var data = {}, notes, acceptRejectTemplate, addressInfo;
            acceptRejectTemplate = '{{#id}}<div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{id}}" onclick="acceptOrRejectJob(\'{{id}}\', \'APPROVE\', {{advancePayment}})" style="background: #67e9b4;">\
                    <p style="position:relative;top:7px;">ACCEPT</p>\
                </div>\
                <div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{id}}" onclick="acceptOrRejectJob(\'{{id}}\', \'REJECTED\')" data-toggle="modal" data-dismiss="modal" data-target="#reestimateOrCloseModal">\
                    <p style="position:relative;top:7px;">REJECT</p>\
                </div>{{/id}}';
            var results = JSON.parse(response);
            data.id = results.data[0]["_id"];
            data.pricedetails = results.data;
            data.servicedetails = results.data;
            data.Notes = ((results.data[0].subCategory.Notes).replace(/\n/g, '<br/>')).split("Note");
            notes = data.Notes;
            if (notes[0]) {
                data.Notes1 = notes[0];
            }
            if (notes[1]) {
                data.Notes2 = notes[1];
            }
            //console.log(notes);
            if (results.data[0].charges.unitCharges) {
                data.serviceBasedType = "FIXED PRICE SERVICE";
                data.seviceBasedPrice = results.data[0].charges.unitCharges;
                data.serviceCharge = "Service Charges : ";
            } else if(results.data[0].charges.callOutCharges) {
                data.serviceBasedType = "INSPECTION BASED SERVICE";
                data.seviceBasedPrice = results.data[0].charges.callOutCharges;
                data.hourlyCharges = results.data[0].charges.hourlyCharges;
                data.serviceCharge = "Call Out Charges* : ";
                data.totalCharges = "To be decided";
            } else if(results.data[0].charges.callOutCharges == 0) {
                data.serviceBasedType = "SURVEY BASED SERVICE";
                data.seviceBasedPrice = results.data[0].charges.callOutCharges;
                data.hourlyCharges = results.data[0].charges.hourlyCharges;
                data.serviceCharge = "Call Out Charges* : ";
                data.totalCharges = "To be decided";  
            }
            data.genie = results.data[0].driverData;
            if (data.genie && data.genie.profilePicURL && !data.genie.profilePicURL.thumbnail) {
                data.genie.profilePicURL.thumbnail = "../images/genieicon.png";
                data.genie.appointmentID = appntmntId;
            }
            data.address = results.data[0].address;
            addressInfo = results.data[0].address;
            var addressAuth = {};
            addressAuth.Auth = udata.data.accessToken;

            if (bookingstatus === "past") {
                var detailData = {}, resultdata;
                resultdata = results.data[0];
                var slot, date, month, day, year;
                slot = {
                    "8":"8AM - 10AM",
                    "10":"10AM - 12PM",
                    "12":"12PM - 2PM",
                    "14":"2PM - 4PM",
                    "16":"4PM - 6PM",
                    "18":"6PM - 8PM"
                };
                resultdata["slot"] = slot[resultdata["slot"]];
                resultdata.services = data.serviceBasedType;
                console.log(resultdata.services);
                if(resultdata.services == 'INSPECTION BASED SERVICE'){
                    resultdata.services = 'Inspection based service'
                } else if(resultdata.services == 'FIXED PRICE SERVICE'){
                    resultdata.services = 'Fixed price service'
                } else if(resultdata.services == 'SURVEY BASED SERVICE'){
                    resultdata.services = 'Survey based service'
                }
                if (resultdata["payment"]["payment_type"] == "null") {
                    resultdata["payment"]["payment_type"] = null;
                    //console.log("here")
                }

                if (resultdata["materials"] && resultdata["materials"].length > 0) {
                    resultdata.AdditionalCharge = [];
                    for (var k in resultdata["materials"]) {
                        var charge = {};
                        if (resultdata["materials"][k]["status"] === "IN_SERVICE") {
                            resultdata.AdditionalCharges = true;
                            charge.name = resultdata["materials"][k]["materialName"];
                            charge.price = resultdata["materials"][k]["materialPrice"];
                            (resultdata.AdditionalCharge).push(charge);
                        }
                    }
                }

                if(!resultdata.paymentPlan) {
                    resultdata.paymentPlan = "On completion";
                }

                var date = results.data[0]["scheduleDate"];
                date = new Date(date);
                month = monthArray[date.getMonth()];
                day = dayArray[date.getDay()];
                year = date.getFullYear();
                date = date.getDate();
                resultdata.scheduleDate = day + " " + date + " " + month + " " + year + " | " + resultdata.slot;
                resultdata["dateScheduled"] = resultdata["slot"] + ", " + month + " " + date;

                var bookingdate = resultdata["utc_timing"]["requestedTime"];
                bookingdate = new Date(bookingdate).toLocaleString();
                bookingdate = bookingdate.split(",");
                resultdata["utc_timing"]["requestedTime"] = bookingdate;
                var jobStatus = results.data[0].status;
                if (jobStatus == "CANCELLED") {
                    resultdata["payment_status"] = "SETTLED";
                }
                if (data.genie) {
                    data.genie["GenieStatus"] = COMPLETED_JOB_TEXT;
                    data.genie.phoneNo = false;
                    data.genie.GenieNotes = results.data[0].genieNotes;
                    data.genie["additionalGenieNote"] = results.data[0].additionalGenieNote;
                    if (results.data[0].genieNotes || results.data[0].additionalGenieNote) {
                        data.genie["showGenieNote"] = true;
                    }
                    var genieStar = Math.round(results.data[0].driverData.rating/results.data[0].driverData.ratingPersonNo);
                    var genieStarObj = {};
                    for (var count = 0; count < genieStar; count++) {
                        genieStarObj[count] = "star-filled";
                    }
                    var keys = Object.keys(genieStarObj);
                    if(keys.length < 5) {
                        var genieStarArrayLength = keys.length;
                        var leftStar = 5 - genieStarArrayLength;
                        for (var count = 0; count < leftStar; count++) {
                            genieStarObj[genieStarArrayLength + count] = "star-blank";
                        }
                    }
                    data.genie["genieStar"] = genieStarObj;
                    data.genie["noOfStar"] = genieStar;

                    if(jobStatus == "PAYMENT_PENDING" || jobStatus == "RATING") {
                        data.genie["GenieNotes"] = null;
                    }
                }
                detailData.viewDetails = resultdata;

                var detailHtml = Mustache.render(viewDetailsTemplate, detailData);
                //console.log(detailHtml);
                $("#popupmodal").html(detailHtml);
                if (jobStatus == "REJECTED") {
                    $("#" + id + "-total").html(resultdata.charges.callOutCharges);
                }
                var jobStatus = results.data[0].status;
                var problemImage = results.data[0].problemImages;
                var materialImages = results.data[0].materialImages;
                var problemData = {}, problemImageArray = [], problemImageTemplate;

                if (problemImage && problemImage.length > 0) {
                    for (var k in problemImage) {
                        var Imagedata = {};
                        Imagedata.thumbnail = problemImage[k].thumbnail;
                        Imagedata.original = problemImage[k].original;
                        problemImageArray.push(Imagedata);
                    }
                    problemData.problemImage = problemImageArray;
                }
                if (materialImages && materialImages.length > 0) {
                    for (var k in materialImages) {
                        var Imagedata = {};
                        Imagedata.thumbnail = materialImages[k].thumbnail;
                        Imagedata.original = materialImages[k].original;
                        problemImageArray.push(Imagedata);
                    }
                    problemData.problemImage = problemImageArray;
                }
                if ((problemImage && problemImage.length > 0) || (materialImages && materialImages.length > 0)) {
                    problemImageTemplate = '<p class="active-color">Uploaded Photo</p>\
                        {{#problemImage}}<div class="genie-box">\
                            <span class="jq_Impreview" data-loadimgjpreview="{{original}}" onclick="zoomImages(\'{{original}}\');">\
                                <img src="{{thumbnail}}" class="img-responsive" alt="{{genieName}}" style="width:100%;">\
                            </span>\
                        </div>{{/problemImage}}';
                    $(".problemImage-" + id).html(Mustache.render(problemImageTemplate,problemData));
                }

                $("#pricing-modal").html(Mustache.render(pricingtemplate, data));
                $("#address-" + id).html(Mustache.render(addressTemplate, data));
                //$(".payprice-" + id).html(Mustache.render(pricingtemplate, data));
                $("#service-modal").html(Mustache.render(serviceTemplate, data));
                //console.log(Mustache.render(pricingtemplate, data));
                $("#service-included").html(notes[0]);
                $("#serviceDetails-note").html("Notes " + notes[1]);
                if (data.serviceBasedType == "FIXED PRICE SERVICE") {
                    $("#pricingnote").html("<p style='margin-top:10px;'>For more information, check our <a href='http://www.homegenie.me/pricing-policies/' class='active-color'>pricing policy</a></p>");
                }
                $(".payservice-" + id).html(Mustache.render(serviceTemplate, data));
                $(".genie-" + id).html(Mustache.render(genieTemplate, data));
                $(".genie-" + id).removeClass("hidden");
                $("." + id + "-req").html(jobStatus);
                $("#popup").click();
                callApi.getUrl(apiUrl + "/api/customer/getAllAddress", addressAuth, function(err, result){
                    if (!err) {
                        var address = result.data;
                        for (var k in address) {
                            if (address[k]["_id"] == data.address["_id"]) {
                                data.address.community = address[k]["community"];
                                $("#address-" + id).html(Mustache.render(addressTemplate, data));
                                $.LoadingOverlay("hide", true);
                            }
                        }
                    } else {
                        $.LoadingOverlay("hide", true);
                        callApi.error(err);
                    }
                });
                // $.LoadingOverlay("hide", true);
                $(".crossImg").click(function(){
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });
            } else if (bookingstatus === "ongoing") {
                var detailData = {}, resultdata;
                resultdata = results.data[0];
                var slot, date, month, day, year;
                slot = {
                    "8":"8AM - 10AM",
                    "10":"10AM - 12PM",
                    "12":"12PM - 2PM",
                    "14":"2PM - 4PM",
                    "16":"4PM - 6PM",
                    "18":"6PM - 8PM"
                };
                resultdata["slot"] = slot[resultdata["slot"]];
                resultdata.services = data.serviceBasedType;

                if (resultdata["payment"]["payment_type"] == "null") {
                    resultdata["payment"]["payment_type"] = null;
                    //console.log("here")
                }

                // for showing additional charges
                if (resultdata["materials"] && resultdata["materials"].length > 0) {
                    resultdata.AdditionalCharge = [];
                    resultdata.charges["materialCharges"] = 0;
                    resultdata.charges["additionalCharges"] = 0;
                    for (var k in resultdata["materials"]) {
                        var charge = {};
                        if (resultdata["materials"][k]["status"] === "IN_SERVICE") {
                            resultdata.AdditionalCharges = true;
                            charge.name = resultdata["materials"][k]["materialName"];
                            charge.price = resultdata["materials"][k]["materialPrice"];
                            resultdata.charges["additionalCharges"] += resultdata["materials"][k]["materialPrice"];
                            (resultdata.AdditionalCharge).push(charge);
                        }
                        if (resultdata["materials"][k]["status"] === "INSPECTION") {
                            resultdata.charges["materialCharges"] += resultdata["materials"][k]["materialPrice"];
                        }
                    }
                }

                // for displayling advance amount
                if(resultdata.advancePayment) {
                    detailData.paymentPlan = "ADVANCE";
                    if (resultdata.status == "IN_SERVICE" || resultdata.status == "PAYMENT_PENDING") {
                        resultdata["advanceAvail"] = true;
                    }
                    if (resultdata.status == "ASSIGNED" && resultdata.charges.unitCharges && resultdata.advance_payment.payment_type == "null") {
                        resultdata["advanceAvail"] = false;
                    }

                } else {
                    detailData.paymentPlan = "Upon completion";
                }

                var date = results.data[0]["scheduleDate"];
                date = new Date(date);
                month = monthArray[date.getMonth()];
                day = dayArray[date.getDay()];
                year = date.getFullYear();
                date = date.getDate();
                resultdata.scheduleDate = day + " " + date + " " + month + " " + year + " | " + resultdata.slot;
                resultdata["dateScheduled"] = resultdata["slot"] + ", " + month + " " + date;

                var bookingdate = resultdata["utc_timing"]["requestedTime"];
                bookingdate = new Date(bookingdate).toLocaleString();
                bookingdate = bookingdate.split(",");
                resultdata["utc_timing"]["requestedTime"] = bookingdate;
                detailData.viewDetails = resultdata;

                var detailHtml = Mustache.render(viewDetailsTemplate, detailData);
                //console.log(detailHtml);
                $("#popupmodal").html(detailHtml);

                var jobStatus = results.data[0].status;
                var problemImage = results.data[0].problemImages;
                var materialImages = results.data[0].materialImages;
                var problemData = {}, problemImageArray = [], problemImageTemplate;

                if (problemImage && problemImage.length > 0) {
                    for (var k in problemImage) {
                        var Imagedata = {};
                        Imagedata.thumbnail = problemImage[k].thumbnail;
                        Imagedata.original = problemImage[k].original;
                        problemImageArray.push(Imagedata);
                    }
                    problemData.problemImage = problemImageArray;
                }
                if (materialImages && materialImages.length > 0) {
                    for (var k in materialImages) {
                        var Imagedata = {};
                        Imagedata.thumbnail = materialImages[k].thumbnail;
                        Imagedata.original = materialImages[k].original;
                        problemImageArray.push(Imagedata);
                    }
                    problemData.problemImage = problemImageArray;
                }
                if ((problemImage && problemImage.length > 0) || (materialImages && materialImages.length > 0)) {
                    problemImageTemplate = '<p class="active-color">Uploaded Photo</p>\
                        {{#problemImage}}<div class="genie-box">\
                            <span class="jq_Impreview" data-loadimgjpreview="{{original}}" onclick="zoomImages(\'{{original}}\');">\
                                <img src="{{thumbnail}}" class="img-responsive" alt="{{genieName}}" style="width:100%;">\
                            </span>\
                        </div>{{/problemImage}}';
                    $(".problemImage-" + id).html(Mustache.render(problemImageTemplate,problemData));
                }
                $("#pricing-modal").html(Mustache.render(pricingtemplate, data));
                $("#address-" + id).html(Mustache.render(addressTemplate, data));
                //$(".payprice-" + id).html(Mustache.render(pricingtemplate, data));
                $("#service-modal").html(Mustache.render(serviceTemplate, data));
                //console.log(Mustache.render(pricingtemplate, data));
                $("#service-included").html(notes[0]);
                $("#serviceDetails-note").html("Notes " + notes[1]);
                if (data.serviceBasedType == "FIXED PRICE SERVICE") {
                    $("#pricingnote").html("<p style='margin-top:10px;'>For more information, check our <a href='http://www.homegenie.me/pricing-policies/' class='active-color'>pricing policy</a></p>");
                }
                $(".payservice-" + id).html(Mustache.render(serviceTemplate, data));
                // data.genie = result
                if (data.genie) {
                    var assignedGenieText = GENIE_ASSIGNED.replace("SUPERVISIOR", resultdata.companyName);
                    assignedGenieText = assignedGenieText.replace("PHONENO", resultdata.companyPhoneNo);
                    data.genie["GenieStatus"] = assignedGenieText;
                    data.genie["GenieNotes"] = results.data[0].genieNotes;
                    data.genie["additionalGenieNote"] = results.data[0].additionalGenieNote;
                    var genieStar = Math.round(results.data[0].driverData.rating/results.data[0].driverData.ratingPersonNo);
                    var genieStarObj = {};
                    for (var count = 0; count < genieStar; count++) {
                        genieStarObj[count] = "star-filled";
                    }
                    var keys = Object.keys(genieStarObj);
                    if(keys.length < 5) {
                        var genieStarArrayLength = keys.length;
                        var leftStar = 5 - genieStarArrayLength;
                        for (var count = 0; count < leftStar; count++) {
                            genieStarObj[genieStarArrayLength + count] = "star-blank";
                        }
                    }
                    data.genie["genieStar"] = genieStarObj;
                    data.genie["noOfStar"] = genieStar;
                    if (results.data[0].genieNotes || results.data[0].additionalGenieNote) {
                        data.genie["showGenieNote"] = true;
                    }
                    if(jobStatus == "PAYMENT_PENDING" || jobStatus == "RATING") {
                        data.genie["GenieNotes"] = null;
                    }
                    if ((results.data[0].advancePayment && results.data[0].status == "INSPECTION") || (results.data[0].advancePayment && results.data[0].status == "ASSIGNED" && results.data[0].charges.advanceCharges)) {
                        data.genie["advanceNote"] = ADVANCE_PAY_NOTE.replace("AMOUNT", resultdata.advancePayment);
                        data.genie["showGenieNote"] = true;
                    }

                }
                if (!data.genie) {
                    var genie = {};
                    genie.name = "Genie to be assigned";
                    genie.profilePicURL = {};
                    genie.profilePicURL.thumbnail = "../images/geniefade.png";
                    genie.GenieStatus = GENIE_NOT_ASSIGNED;
                    data.genie = genie;
                }

                $(".genie-" + id).html(Mustache.render(genieTemplate, data));
                $(".genie-" + id).removeClass("hidden");
                $("." + id + "-req").html(jobStatus);

                var cancelTemplate = '{{#id}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" onclick="cancelAppointment(\'{{id}}\',\'{{id}}-d\')">\
                            <p style="position:relative;top:7px;">CANCEL REQUEST</p>\
                        </div>{{/id}}';
                var paymentTemplate = '{{#id}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{id}}\', {{finalCharges}})" style="background: #67e9b4;">\
                    <p style="position:relative;top:7px;">PAY NOW</p>\
                </div>{{/id}}';
                var advanceFixedTemplate = '{{#id}}<div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{id}}\', \'FIXED\',{{advancePayment}})" style="background: #67e9b4;">\
                    <p style="position:relative;top:7px;">ACCEPT</p>\
                </div><div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="cancelAppointment(\'{{id}}\',\'{{id}}-d\')">\
                <p style="position:relative;top:7px;">CANCEL </p></div>{{/id}}';
                if (jobStatus == "PAYMENT_PENDING" || jobStatus == "CANCELLED") {
                    if (jobStatus == "PAYMENT_PENDING") {
                        data.finalCharges = resultdata.charges.finalCharges;
                    } else if (jobStatus == "CANCELLED") {
                        data.finalCharges = resultdata.charges.totalCharges;
                    }
                    console.log("final charges ", data.finalCharges);
                    if (data.finalCharges) {
                        $("#finalstatus").html(Mustache.render(paymentTemplate, data));
                        $("#" + id + "-view").html('<span class="PENDING">PAYMENT PENDING</span><button class="pay-now" data-id="{{_id}}" data-toggle="modal" onclick="getBookingDetails(\'' + id + '\', \'ongoing\')"><span class="booking-btn-align">PAY NOW</span></button>');
                    }
                    if (jobStatus == "PAYMENT_PENDING" && resultdata.advance_payment && resultdata.advance_payment.actual_payment) {
                        $("." + id + "-total").html(resultdata.charges.finalCharges);
                    }

                } else if (jobStatus == "COMPLAINT_REGISTERED" || jobStatus == "IN_SERVICE" || jobStatus == "RATING" || jobStatus == "UNFINISHED") {
                    $("." + id + "-total").html(results.data[0].charges.estimateCharges);
                    $("#finalstatus").html("");
                    if (jobStatus == "RATING") {
                        $("." + id + "-total").html(resultdata.charges.totalCharges);
                        $("#" + id + "-view").html('<span class="pull-right"><span class="active-color">RATING </span><button class="pay-now" onclick="window.location.href=\'/rateGenie.html?genieid=' + id +'\'">RATE GENIE</button></span><br/>\
                            <span class="pull-right"><span class="active-color"></span><button class="view-detail view-detail-ongoing" onclick="getBookingDetails(\'' + id + '\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>');
                    }
                    if (jobStatus == "IN_SERVICE") {
                        $("#" + id + "-cancel").html("");
                        $("." + id + "-total").html(resultdata.charges.estimateCharges - resultdata.advancePayment);
                    }
                    if (jobStatus == "COMPLAINT_REGISTERED") {
                        $("." + id + "-total").html("To be decided");
                    }
                } else if (jobStatus == "REJECTED") {
                    $("." + id + "-total").html(results.data[0].charges.callOutCharges);
                    data.totalCharges = results.data[0].charges.callOutCharges;
                    $("#finalstatus").html(Mustache.render(paymentTemplate, data));
                } else if (jobStatus == "INSPECTION" || jobStatus == "RESCHEDULED") {
                    if (results.data[0].isInspectionCompleted) {
                        var data = {};
                        data.id = id;
                        data.advancePayment = results.data[0].advancePayment;
                        var html = Mustache.render(acceptRejectTemplate, data);
                        $("#finalstatus").html(html);
                        $("." + id + "-total").html(results.data[0].charges.estimateCharges);
                        $("#" + id + "-cancel").html("");
                    } else {
                        $("#" + id + "-finalstatus").html("");
                        $("#" + id + "-cancel").html("");
                        $("." + id + "-total").html("To be decided");
                    }
                } else if (jobStatus == "REQUESTED" || jobStatus == "ASSIGNED" || jobStatus == "ENROUTE" || jobStatus == "REESTIMATE") {
                    data.advancePayment = results.data[0].advancePayment;
                    $("#finalstatus").html(Mustache.render(cancelTemplate, data));
                    if (data.serviceBasedType == "INSPECTION BASED SERVICE") {
                        $("." + id + "-total").html("To be decided");
                    } else {
                        $("." + id + "-total").html(results.data[0].charges.unitCharges);
                    }

                    if(jobStatus == "ASSIGNED" && results.data[0].charges.unitCharges && results.data[0].advancePayment) {
                        $("#finalstatus").html(Mustache.render(advanceFixedTemplate, data));
                    }
                    if (jobStatus == "ASSIGNED" && results.data[0].charges.unitCharges && results.data[0].advancePayment && results.data[0]["advance_payment"]["actual_payment"]) {
                        $("#finalstatus").html('');
                        //console.log("here")
                    }
                }
                // $.LoadingOverlay("hide", true);
                $("#popup").click();
                $(".crossImg").click(function(){
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });
                // image plugin

            }
            // $.LoadingOverlay("show");
            callApi.getUrl(apiUrl + "/api/customer/getAllAddress", addressAuth, function(err, result){
                if (!err) {
                    var address = result.data;
                    for (var k in address) {
                        if (address[k]["_id"] == addressInfo["_id"]) {
                            data.address = address[k];
                            $("#address-" + id).html(Mustache.render(addressTemplate, data));
                            $.LoadingOverlay("hide", true);
                        }
                    }
                } else {
                    $.LoadingOverlay("hide", true);
                    callApi.error(err);
                }
            });
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function contactGenie(id) {
    var contact = $("#contactGenie").attr("data-contact");
    $("#contactGenie").html("CONTACT GENIE ( <span class='hidden-sm hidden-md hidden-lg'><a href='tel:" + contact + "'>" + 0 + contact + "</a></span><span class='hidden-xs'>" + contact + "</span> )");
    /*var data = new FormData();
     data.Auth = udata.data.accessToken;
     data.append("appointmentID",id);
     data._method = "POST";
     $.growl.notice({message : "Genie will contact you soon."});
     $.LoadingOverlay("show");
     callApi.queryUrl(apiUrl + "/api/customer/calltoDriver", data, function(err, response){
     if (!err) {
     $.LoadingOverlay("hide", true);
     } else {
     $.LoadingOverlay("hide", true);
     callApi.error(err);
     }
     });*/
}

function zoomImages(img) {
    var e, d;
    e = 200;
    d = 200;
    var b, f, a, c;
    c = "<span class='closeWin'><img src='../images/closewindow.png' alt='img'></span>";
    a = c + "<img src='' alt='img' class='jQimagPreview_img_i'>";
    b = "<div id='jQimagPreview_cont' class='loading'><div class='con_in'>" + a + "</div></div>";
    f = "<div id='jQimagPreview'>" + b + "</div>";
    $("body").append(f);
    /*$(".jq_Impreview").click(function() {
     alert("hello");

     });*/
    var g;
    g = img;
    $("#jQimagPreview").fadeIn(e);
    $(".jQimagPreview_img_i").attr("src", "" + g);
    $(".jQimagPreview_img_i").hide();
    $(".jQimagPreview_img_i").fadeIn(1000).delay(2000);
    $("#jQimagPreview_cont").addClass();
    $("#jQimagPreview_cont .closeWin").click(function() {
        $("#jQimagPreview").fadeOut(d);
        // $(".jQimagPreview_img_i").attr("src", "../images");
    });
}

function cancelAppointment(id, modalid) {
    $.LoadingOverlay("show");
    if(modalid) {
        $("#closemodal").attr("onclick", "cancelPopup(true)");
    } else {
        $("#closemodal").attr("onclick", "cancelPopup()");
    }
    var data = {};
    data.Auth = udata.data.accessToken;
    callApi.getUrl(apiUrl + "/api/customer/JobCancelChargeCalculation?jobId=" + id, data, function(err, results){
        if (!err){
            $("#cancelCharge").html(results.data.cancelCharge);
            $("#cancel").click();
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
    $("#cancel-confirm").click(function(){
        $.LoadingOverlay("show");
        var params = new FormData();
        params._method = "PUT";
        params.Auth = udata.data.accessToken;
        params.append("jobId", id);
        callApi.queryUrl(apiUrl + "/api/customer/JobCancelCharge", params, function(err,results){
            if (!err) {
                $("#cancelConfirm").modal("hide");
                $("body").removeClass("modalOpen");
                $("#popupmodal").modal("hide");
                // $.growl.notice({message : "You have successfully cancel the appointment"});
                 var message = {"msg":"You have successfully cancel the appointment"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                getmybookings();
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    });
}

function cancelPopup(check) {
    $("#cancelConfirm").modal("hide");
    if(check) {
        setTimeout(function(){
            $("body").addClass("modal-open");
        },500);
    }
}

function hideCurrentModal(elem){
    //$("body").addClass("modalOpen");
    var id = ((((elem.parentNode).parentNode).parentNode).parentNode);
    id = $(id).attr("id");
    $("#" + id).modal("hide");
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
}

function acceptOrRejectJob(id, status, amount, elem) {
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("jobId",id);

    //console.log("amount is:" + amount);
    if (status == "APPROVE") {
        data.append("status",status);
        $.LoadingOverlay("show");
        $("#popupmodal").modal('toggle');

        if (amount) {
            $("#advancePrice").html(amount);
            $("#advancePayment").click();
            $.LoadingOverlay("hide", true);
            $("#doadvancePay").click(function(){
                makePayment(id, amount);
            });
        } else {
            callApi.putUrl(apiUrl + "/api/customer/acceptOrRejectedJobOnce/", data, function(err, results){
                if (!err){
                    // $.growl.notice({message : "Your job has been updated successfully."});
                    var message = {"msg":"Your job has been updated successfully."}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    $("#popupmodal").modal("hide");
                    $("body").removeClass("modalOpen");
                    $.LoadingOverlay("hide", true);
                    getmybookings();
                } else {
                    $.LoadingOverlay("hide", true);
                    callApi.error(err);
                }
            });
        }

    } else if (status == "FIXED" && amount) {
        data.append("status",status);
        $.LoadingOverlay("show");
        $("#popupmodal").modal('toggle');
        $("#advancePrice").html(amount);
        $("#advancePayment").click();
        $.LoadingOverlay("hide", true);
        $("#doadvancePay").click(function(){
            makePayment(id, amount);
        });
    } else if(status == "REJECTED" || status == "CLOSED") {
        $(".reestimateorclose").click(function(){
            var selectedStatus = $(this).attr("data-status");
            if(selectedStatus == "REESTIMATE") {
                status = "REJECTED";
            } else if (selectedStatus == "CLOSED") {
                status = "CLOSED";
            }
            data.append("status", status);
            callApi.putUrl(apiUrl + "/api/customer/acceptOrRejectedJobOnce/", data, function(err, results){
                if (!err){
                    // $.growl.notice({message : "Your job has been updated successfully."});
                    var message = {"msg":"Your job has been updated successfully."}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');

                    $("#popupmodal").modal("hide");
                    $("body").removeClass("modalOpen");
                    $.LoadingOverlay("hide", true);
                    getmybookings();
                } else {
                    $.LoadingOverlay("hide", true);
                    callApi.error(err);
                }
            });
            /*if(selectedStatus == "CLOSED") {
                setTimeout(function(){
                    cancelAppointment(id);
                },500);
            } else if(selectedStatus == "REESTIMATE") {

            }*/

        });
    }
}

function initOngoingBookingSearch(){
    $("#search-ongoing").keyup(function(){
        var bookingsearchinput = this.value;
        $('.ongoingbookingsearch').each(function () {
            var service = $(this).attr("data");
            var re = new RegExp(bookingsearchinput, 'gi');
            if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                $(this).addClass('hidden');
            } else {
                $(this).removeClass('hidden');
            }
            if ((service.trim()).match(re)) {
                $(this).removeClass('hidden');
            }
        });
    });
    $("#search-completed").keyup(function(){
        var bookingsearchinput = this.value;
        $('.completedbookingsearch').each(function () {
            var service = $(this).attr("data");
            var re = new RegExp(bookingsearchinput, 'gi');
            if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                $(this).addClass('hidden');
            }
            else {
                $(this).removeClass('hidden');
            }
            if ((service.trim()).match(re)) {
                $(this).removeClass('hidden');
            }
        });
    });

    $("#search-ongoing-clear").click(function(){
        $("#brandsearchinput").val('');
        $('.branditem').each(function () {
            $(this).removeClass('hidden');
        });
    });
}

function makePayment(id, amount) {
    var paymentdata = {};
    paymentdata.id = id;
    paymentdata.a = amount;
    paymentdata = JSON.stringify(paymentdata);
    createCookie("hg_p",paymentdata,1);
    window.location.href="../html/paymentmethod.html";
}

$(document).ready(function(){
    getmybookings();
    console.log(localStorage.notification);
    if(localStorage.notification == 'true'){
        console.log('in notification');
        getBookingDetails(localStorage.id,'ongoing');
        localStorage.setItem('notification','false');
    }
});

var ongoingBooking_template =
    '<div class="col-xs-12 col-sm-12 col-md-12 column search-tab-div padding0">\
        <div class="form-group">\
            <div class="icon-addon addon-md">\
                <input type="text" placeholder="Search" class="form-control" id="search-ongoing" style="border:0px solid #ccc;">\
                <label for="search-ongoing" class="glyphicon glyphicon-search" rel="tooltip" title="search-ongoing"></label>\
            </div>\
        </div>\
    </div>\
    {{#ongoing}}<div class="col-xs-12 col-sm-12 col-md-12 column padding0 ongoing-list-border ongoingbookingsearch" data="{{subcategory.subCategoryName}} {{uniqueCode}} {{status}} {{dateScheduled}}" style="margin-top:5px;">\
        <div class="col-xs-12 col-sm-7 col-md-6 column padding0 ongoing-list vertical-align">\
            <div class="xs-catimage">\
                <div class="service-img-div vertical-align">\
                    <img src="{{category.whiteImage.original}}" class="img-resposive">\
                </div>\
            </div>\
            <div class="service-booking-txt" style="margin-left:10px;">\
                <p class="active-color hidden-sm hidden-md hidden-lg">JOB ID : <span style="color:#FA7E7E;">{{uniqueCode}}</span> </p>\
                <p class="hidden-xs">{{subcategory.subCategoryName}}</p>\
                <p style="color:#a1a1a2;">{{dateScheduled}}</p>\
                <p>{{nickName}}</p>\
                <p class="active-color hidden-xs">JOB ID : <span style="color:#FA7E7E;">{{uniqueCode}}</span> </p>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-5 col-md-6 column padding0 ongoing-btn-grp">\
            <p class="xs-subcatName hidden-sm hidden-md hidden-lg">{{subcategory.subCategoryName}}</p>\
            <div class="btn-grp-pay pull-right">\
                {{#Accept}}<span class="" id="{{_id}}-accept"><span class="{{payment_status}} hidden-xs status-pos">Pay Advance</span><button class="pay-now" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'APPROVE\', {{advancePayment}})"><span class="booking-btn-align">ACCEPT</span></button></span><span class="hidden-xs"><br/></span>{{/Accept}}\
                {{#Payment}}<span class="" id="{{_id}}-view"><span class="{{payment_status}} status-pos">{{#payment_status}}Payment {{payment_status}}{{/payment_status}}</span><button class="pay-now" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-pay" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">PAY NOW</span></button></span><span class="hidden-xs"><br/></span>{{/Payment}}\
                {{#Cancel}}<span class="" id="{{_id}}-cancel"><span class="active-color status-pos"></span><button class="pay-now" onclick="cancelAppointment(\'{{_id}}\')" style="margin-top:0px;"><span class="booking-btn-align">CANCEL</span></button></span><span class="hidden-xs"><br/></span>{{/Cancel}}\
                {{#View}}<span class="" id="{{_id}}-view"><span class="active-color {{_id}}-req status-pos">{{status}}</span><button class="view-detail view-detail-ongoing" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>{{/View}}\
                {{#RateGenie}}<span class=""><span class="active-color status-pos">{{status}}</span><button class="pay-now" onclick="window.location.href=\'/rateGenie.html?genieid={{_id}}\'">RATE GENIE</button></span><span class="hidden-xs"><br/></span>\
                <span class=""><span class="active-color"></span><button class="view-detail view-detail-ongoing" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-d" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>{{/RateGenie}}\
                <span class="{{_id}}"></span>\
                <span class="{{_id}}-pay"></span>\
            </div>\
        </div>\
    </div>{{/ongoing}}';

var completedBooking_template =
    '<div class="col-xs-12 col-sm-12 col-md-12 column search-tab-div padding0">\
        <div class="form-group">\
            <div class="icon-addon addon-md">\
                <input type="text" placeholder="Search" class="form-control" id="search-completed" style="border:0px solid #ccc;">\
                <label for="search-completed" class="glyphicon glyphicon-search" rel="tooltip" title="search-completed"></label>\
            </div>\
        </div>\
    </div>\
    {{#completed}}<div class="col-xs-12 col-sm-12 col-md-12 column padding0 ongoing-list ongoing-list-border completedbookingsearch" data="{{subcategory.subCategoryName}} {{uniqueCode}} {{status}} {{dateScheduled}}">\
        <div class="col-xs-12 col-sm-7 col-md-6 column padding0 ongoing-list vertical-align">\
            <div class="xs-catimage">\
                <div class="service-img-div vertical-align">\
                    <img src="{{category.whiteImage.original}}" class="img-resposive">\
                </div>\
            </div>\
            <div class="service-booking-txt" style="margin-left:10px;">\
                <p class="active-color hidden-sm hidden-md hidden-lg">JOB ID : <span style="color:#FA7E7E;">{{uniqueCode}}</span> </p>\
                <p class="hidden-xs">{{category.whiteImage.name}} {{subcategory.subCategoryName}}</p>\
                <p style="color:#a1a1a2;">{{dateScheduled}}</p>\
                <p>{{nickName}}</p>\
                <p class="active-color hidden-xs">JOB ID : <span style="color:#FA7E7E;">{{uniqueCode}}</span> </p>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-5 col-md-6 column padding0">\
            <p class="xs-subcatName hidden-sm hidden-md hidden-lg">{{subcategory.subCategoryName}}</p>\
            <div class="btn-grp-pay completed-grp-pay pull-right">\
                <span class="pull-right"><span class="active-color status-pos">{{status}}</span>\
                <button class="view-detail completed-details" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-d" onclick="getBookingDetails(\'{{_id}}\', \'past\')"><span class="booking-btn-align">VIEW DETAILS</span></button>\
                </span>\
                <span class="{{_id}}"></span>\
            </div>\
        </div>\
    </div>{{/completed}}';

var pricingtemplate = '{{#pricedetails}}<div class="modal-dialog">\
                                <div class="modal-content containerSize">\
                                    <div class="row clearfix">\
                                        <div class="col-xs-12 col-md-12 col-sm-12 container-margin border-pricing-detail" style="margin-top:20px;padding-bottom:5px;">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail"><span class="fa fa-tags"></span>  PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 air-conditioning-position">\
                                                <p class="issue-booking air-conditioning">\
                                                    <img src="{{categoryImage.thumbnail}}" class="ac-img">\
                                                    {{categoryName}} ({{subCategory.subCategoryName}})\
                                                </p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 inspection vertical-align" style="height:30px;">\
                                                <p class="service-align" style="width:100%;">{{serviceBasedType}}</p>\
                                            </div>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 col-xs-12 margin-charges">\
                                            <p class="charges">Charges</p>\
                                            <p class="call-out-charges">{{serviceCharge}}<span class="aed" id="callOutCharges">{{seviceBasedPrice}} AED</span></p>\
                                            {{#hourlyCharges}}<p class="call-out-charges">Additional Hour Charges : <span class="active-color">{{hourlyCharges}} AED</span></p>{{/hourlyCharges}}\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 col-xs-12 note" id="pricingnote">\
                                            <p class="note-margin note-margin-pricing">Note:</p>\
                                            <p class="note-call-out-charges">* Call-out charge is a minimum charge that includes  1 hour of labor to inspect and diagnose the issue or/and perform minor repairs, if time permits\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12 column pointer geniemsg" onclick="hideCurrentModal(this);" style="padding:0px;">\
                                            <p class="text-center">CONTINUE</p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>{{/pricedetails}}';

var serviceTemplate = '{{#servicedetails}}<div class="modal-dialog">\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-xs-12 col-sm-12 col-md-12 border-service-detail" style="margin-top:20px;padding-bottom:5px;">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail"><span class="fa fa-info-circle"></span>  SERVICE DETAILS</p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 air-conditioning-position">\
                                                <p class="air-conditioning">\
                                                <img src="{{categoryImage.thumbnail}}" class="ac-img">\
                                                {{categoryName}} ({{subCategory.subCategoryName}})\
                                            </p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">SERVICE DETAILS</p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 fixed-service vertical-align" style="height:30px;">\
                                                <p class="service-align" style="width:100%;">{{serviceBasedType}}</p>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12">\
                                            <p class="charges whats-included-margin">What\'s Included</p>\
                                            <p class="whats-included" id="service-included">{{#Notes1}}-{{.}}<br/>{{/Notes1}}</p>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12 note note-font-servicedetails">\
                                            <p class="note-margin note-margin-top hidden">Note:-</p>\
                                            <p class="note-margin" id="serviceDetails-note" style="margin-top:0px;">Note{{#Notes2}}-{{.}}<br/>{{/Notes2}}</p>\
                                        </div>\
                                        <div class="col-xs-12 col-sm-12 col-md-12 column pointer geniemsg" onclick="hideCurrentModal(this)" style="padding:0px;">\
                                            <p class="text-center">CONTINUE</p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>{{/servicedetails}}';

var genieTemplate = '{{#genie}}<div class="genie-name-box detail-genie-name-box">\
                            <div class="col-xs-12 col-sm-2 col-md-2 column details-genie-img">\
                                <img src="{{profilePicURL.thumbnail}}" class="img-responsive genie-img">\
                            </div>\
                        <div class="hidden-xs col-sm-10 col-md-10 column">\
                            <p class="details-genie-name" style="margin-bottom:0px;">{{name}} {{#favouriteGenie}}(favourite genie){{/favouriteGenie}}</p>\
                            {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:15px;">\
                            <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:15px;">\
                            <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:15px;">\
                            <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:15px;">\
                            <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:15px;">\
                            <span style="position:relative;top:-3px;left:5px;">({{noOfStar}}/5)<span>{{/genieStar}}\
                            <br/>\
                        </div>\
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
                            <div class="row clearfix text-center">\
                                <div class="col-xs-12 column">\
                                    <p class="details-genie-name" style="margin-bottom:0px;">{{name}}</p>\
                                </div>\
                                {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:15px;">\
                                <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:15px;">\
                                <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:15px;">\
                                <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:15px;">\
                                <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:15px;">\
                                <span style="position:relative;top:-3px;left:5px;">({{noOfStar}}/5)</span>{{/genieStar}}\
                            </div>\
                        </div>\
                    </div>\
                    {{#phoneNo}}<button class="detail-contact genie-contact" id="contactGenie" data-contact="{{phoneNo}}" onclick="contactGenie(\'{{id}}\')">CONTACT GENIE</button>{{/phoneNo}}\
                    {{#GenieStatus}}<p style="font-size:10px;margin-top:5px;text-align:center">{{GenieStatus}}</p>{{/GenieStatus}}\
                    {{#showGenieNote}}<p style="font-size:12px;font-weight:600;word-wrap:break-word;">Note: {{#advanceNote}}<span>{{advanceNote}}</span><br/>{{/advanceNote}}{{#GenieNotes}}<span>{{GenieNotes}}</span></br>{{/GenieNotes}}{{#additionalGenieNote}}<span>{{additionalGenieNote}}</span>{{/additionalGenieNote}}</p>{{/showGenieNote}}\
                    {{/genie}}';

var addressTemplate = '{{#address}}<span>{{addressType}} {{apartmentNo}}, {{streetAddress}}, {{community}}, {{city}}, UAE</span>\
                    {{/address}}';

var viewDetailsTemplate =
    '{{#viewDetails}}<!--<div id="popupmodal{{#Accept}}{{_id}}-d{{/Accept}}{{#Details}}{{_id}}-d{{/Details}}{{#cancelRequest}}{{_id}}-d{{/cancelRequest}}{{#completeSettled}}{{_id}}-d{{/completeSettled}}{{#payNow}}{{_id}}-pay{{/payNow}}{{#paynow}}{{_id}}-d{{/paynow}}{{#Inspected}}{{_id}}-d{{/Inspected}}" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">-->\
        <div class="modal-dialog job-details-dialog">\
            <span class="cross" data-dismiss="modal"><img class="crossImg" src="../images/Cross.png"></span>\
            <div class="modal-content job-details-modal">\
            <div class="row clearfix" style="margin:0px;background:#fff;">\
                <div class="col-xs-12 col-sm-12 col-md-12 column job-assignee-row">\
                    <span class="job-detail-id">\
                        <p class="active-color text-center jobdetail-p p">JOB DETAILS<br/><span style="color:#FA7E7E;">(JOB ID: {{uniqueCode}})</span></p>\
                        <p class="text-center hidden-sm hidden-md hidden-lg" style="margin-bottom:0px;">STATUS: </span><span class="assigned {{_id}}-req">{{status}}</span></p>\
                        <p class="text-center hidden-sm hidden-md hidden-lg" style="font-size: 14px;"><span>Booking date + time : {{utc_timing.requestedTime}}</span></p>\
                        <p class="newstatus hidden-xs" style="font-size: 14px;"><span class="bookingdate">Booking date + time : {{utc_timing.requestedTime}}</span><span class="hidden-sm hidden-md hidden-lg"><br/></span><span class="newJobs"><span>STATUS: </span><span class="assigned {{_id}}-req">{{status}}</span></span></p>\
                    </span>\
                    \
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column issue-pricing-row jobdetails-pricing-row">\
                    <div class="col-xs-12 col-sm-12 col-md-7 column issue-instruction jobdetails-issue-instruction">\
                        <p class="service-p">\
                            <img src="{{categoryImage.original}}" class="service-imgs service-img-{{_id}} service-detail-img" id="service-img-{{_id}}"><span class="active-color hidden">Category</span><span class="pull-right ccname"> {{categoryName}}</span>\
                            <span class=""><br/></span><span class="issue-detail"><span class="active-color">Issue </span><span class="pull-right">{{subCategory.subCategoryName}}</span></span>\
                            <br/>\
                            <span class="datetime-left"><span class="active-color">Visit Date and time{{#payNow}} and Time{{/payNow}}{{#cancelRequest}} and Time{{/cancelRequest}}{{#Details}} and Time{{/Details}} </span><span class="pull-right">{{scheduleDate}}</span></span>\
                        </p>\
                        <div class="col-xs-12 col-sm-12 col-md-12 column instruction-row" style="padding-left:20px;padding-right:0px">\
                            {{#problemDetails}}<p class="active-color">Instructions</p>\
                            <p>{{problemDetails}}</p>{{/problemDetails}}\
                        <span class=problemImage-{{_id}}></span>\
                        <p class="active-color">Address (Default)</p>\
                        <span class="address-{{_id}} address-jobdetails pull-right" id="address-{{_id}}">{{references.addressID.addressType}}</span><br/>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-12 column genie-{{_id}} hidden" id="genie-{{_id}}" style="padding-bottom:10px;">\
                        <div class="genie-name-box detail-genie-name-box">\
                            <div class="col-xs-12 col-sm-2 col-md-2 column details-genie-img">\
                                <img src="{{genieImage}}" class="img-responsive genie-img">\
                            </div>\
                        <div class="hidden-xs col-sm-10 col-md-10 column">\
                            <p class="details-genie-name">{{genieName}} (favourite genie)</p>\
                            <fieldset class="rating rating-detail-genie">\
                                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5"></label>\
                                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4"></label>\
                                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3"></label>\
                                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2"></label>\
                                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1"></label>\
                            </fieldset>\
                            <br/>\
                        </div>\
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
                            <div class="row clearfix text-center">\
                                <div class="col-xs-12 column">\
                                    <p class="details-genie-name">{{genieName}}</p>\
                                </div>\
                                <div class="col-xs-12 column">\
                                    <fieldset class="rating rating-delete-genie">\
                                        <input type="radio" id="star11" name="rating" value="5" /><label class = "full" for="star11"></label>\
                                        <input type="radio" id="star10" name="rating" value="4" /><label class = "full" for="star10"></label>\
                                        <input type="radio" id="star9" name="rating" value="3" /><label class = "full" for="star9"></label>\
                                        <input type="radio" id="star7" name="rating" value="2" /><label class = "full" for="star7"></label>\
                                        <input type="radio" id="star6" name="rating" value="1" /><label class = "full" for="star6"></label>\
                                    </fieldset>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <button class="detail-contact genie-contact">CONTACT GENIE</button>\
                </div>\
            </div>\
            <div class="col-xs-12 col-sm-12 col-md-5 column summary-details-row">\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>SERVICE</span>\
                        <span class="pull-right active-color pointer align-tleft" data-toggle="modal" data-target="#service-modal{{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}">\
                            <span style="white-space: nowrap;margin-right: 17px;"><img src="../images/service-details.svg" style="max-width:18px;">View service details</span>\
                        </span>\
                        <div id="service-modal{{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}" class="modal fade {{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}" role="dialog" data-keyboard="false" data-backdrop="static">\
                            <div class="modal-dialog">\
                                <span class="cross"><img class="crossImg" src="../images/Cross.png"></span>\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-sm-12 col-md-12 border-service-detail">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail">SERVICE DETAILS</p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 air-conditioning-position">\
                                                <p class="air-conditioning">\
                                                <img src="" class="ac-img">\
                                                \
                                            </p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">SERVICE DETAILS</p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 fixed-service">\
                                                <p class=""></p>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12">\
                                            <p class="charges whats-included-margin">What\'s Included</p>\
                                            <p class="whats-included" id="service-included"></p>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 note note-font-servicedetails">\
                                            <p class="note-margin note-margin-top">Note:-</p>\
                                            <p class="note-margin" id="serviceDetails-note"></p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </p>\
                    <p style="margin-top:5px;"><span class="active-color">Type</span> <span class="pull-right" style="text-transform:capitalize;">{{services}}</span></p>\
                    <p><span class="active-color">Priority</span> <span class="pull-right">Standard</span></p>\
                    <p><span class="active-color">Frequency</span><span class="pull-right">One time</span></p>\
                    <p><span class="active-color">{{subCategory.question}}</span><span class="pull-right">{{subCategory.answers}}</span></p>\
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>PRICING</span>\
                        <span class="pull-right active-color pointer align-tleft" data-toggle="modal" data-target="#pricing-modal{{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}">\
                            <span><img src="../images/pricing.png"/ style="max-width:18px;"> View pricing info</span>\
                        </span>\
                        <div id="pricing-modal{{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}" class="modal fade {{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}" role="dialog" data-keyboard="false" data-backdrop="static">\
                            <div class="modal-dialog">\
                                <span class="cross"><img class="crossImg" src="../images/Cross.png"></span>\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-md-12 col-sm-12 container-margin border-pricing-detail">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 air-conditioning-position">\
                                                <p class="issue-booking air-conditioning">\
                                                    <img src="" class="ac-img">\
                                                    \
                                                </p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 inspection">\
                                                <p class=""></p>\
                                            </div>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 margin-charges">\
                                            <p class="charges">Charges</p>\
                                            <p class="call-out-charges">Call Out Charges*  :  <span class="aed" id="callOutCharges">{{charges.callOutCharges}} AED</span></p>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 note">\
                                            <p class="note-margin note-margin-pricing">Note:</p>\
                                            <p class="note-call-out-charges">* Call-out charge is a minimum charge that includes  1 hour of labor to inspect and diagnose the issue or/and perform minor repairs, if time permits\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </p>\
                    <p class="active-color" style="margin-top:5px;"><span>Item</span><span class="pull-right">AED</span></p>\
                    <p><span>Labor (Including callout)</span><span class="pull-right">{{charges.labourCharges}}</span></p>\
                    <p><span>Material</span><span class="pull-right">{{charges.materialCharges}}</span></p>\
                    {{#charges.unitCharges}}<p><span>Service Charges</span><span class="pull-right">{{charges.unitCharges}}</span></p>{{/charges.unitCharges}}\
                    {{#charges.additionalCharges}}<p><span>Other Charges</span><span class="pull-right">{{charges.additionalCharges}}</span></p>{{/charges.additionalCharges}}\
                    <p><span>(less) Discount (PROMO)</span><span class="pull-right">{{charges.discountCharges}}</span></p>\
                    {{#advanceAvail}}<p><span>(less) Advance</span><span class="pull-right">{{advancePayment}}</span>{{/advanceAvail}}\
                    <p><span>TOTAL {{#advanceAvail}}(due){{/advanceAvail}}</span><span class="pull-right active-color {{_id}}-total" id="{{_id}}-total">{{charges.totalCharges}}</span></p>\
                    <p>See detailed breakdown on email sent to registered email address.</p>\
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>PAYMENT</span>\
                    </p>\
                    <p style="margin-top:5px;"><span>Plan</span><span class="pull-right active-color">{{paymentPlan}}</span></p>\
                    {{#payment.payment_type}}<p><span>Method</span><span class="pull-right active-color">{{payment.payment_type}}</span></p>{{/payment.payment_type}}\
                    <p><span>Status</span><span class="pull-right active-color">{{payment_status}}</span></p>\
                </div>\
            </div>\
        </div>\
        <span id="finalstatus"><span>\
        {{#cancelRequest}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" onclick="cancelAppointment(\'{{_id}}\',\'{{#cancelRequest}}{{_id}}-d{{/cancelRequest}}\')">\
            <p style="position:relative;top:7px;">CANCEL REQUEST</p>\
        </div></span>{{/cancelRequest}}\
        {{#paynow}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{_id}}\', {{charges.totalCharges}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">PAY NOW</p>\
        </div></span>{{/paynow}}\
        {{#Accept}}<span id="{{_id}}-finalstatus"><div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'APPROVE\', {{advancePayment}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">ACCEPT</p>\
        </div>\
        <div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'REJECTED\', "", this)">\
            <p style="position:relative;top:7px;">REJECT</p>\
        </div></span>{{/Accept}}\
        {{#AdvanceFixed}}<span id="{{_id}}-finalstatus"><div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'APPROVE\', {{advancePayment}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">PAY</p>\
        </div>\
        <div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'REJECTED\')">\
            <p style="position:relative;top:7px;">CANCEL</p>\
        </div></span>{{/AdvanceFixed}}\
        {{#payNow}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{_id}}\', {{charges.totalCharges}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">PAY NOW</p>\
        </div></span>{{/payNow}}\
        <!--{{#completeSettled}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id={{_id}} style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">SETTLED</p>\
        </div>{{/completeSettled}}-->\
    </div>\
    </div></div>\
    <!--</div>-->{{/viewDetails}}';





