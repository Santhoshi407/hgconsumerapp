/**
 * Created by sumit on 13/10/16.
 */

// angular.module('starter',[])
// .controller('booking')
var udata, scheduleddate, addressID, allAddress, updateAddress,newURL,subcategoryCharge,
    promo = 0,latlong, favgenie = false, firstcharge = 0, villaCharge = 0, restUnitcharge = 0, addressType = "APARTMENT",additionalHourCharge,
    serviceType = [], priceBasedService, editlat, editlong, additionalCount = 0, selectedCategory, typetext = '',serviceBasedType = "SCHEDULED",emergencycharge;
var day = ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"],
    pricingPolicy = "<p style='margin-top:10px;'>For more information, check our <a href='http://www.homegenie.me/pricing-policies/' class='active-color'>pricing policy</a></p>";

function getAndUpdateCustomerAddress() {
    var params = {};
    params.Auth = udata.data.accessToken;
    callApi.getUrl(apiUrl + "/api/customer/getAllAddress",params, function(err, results){
        if (!err) {
            allAddress = results;
            var addressData = results.data;
            if (addressData.length > 0) {
                var addresses = {}, addressDetail, reviewaddress;
                addresses.address = Mustache.render("{{#data}}<option value='{{addressType}} {{apartmentNo}}, {{streetAddress}}, {{community}}, {{city}}, {{emirate}}' data-id='{{_id}}'>{{nickName}}</option>{{/data}}", results);
                $(".address-select").html(addresses.address);
                addressDetail = $(".address-select option:selected").val();
                addressDetail = (addressDetail).split(",");
                if (addressDetail[0].match(/VILLA/)) {
                    addressType = "VILLA";
                } else {
                    addressType = "APARTMENT"
                }
                addressDetail = addressDetail[0] + " ,</span><br/><span>" + addressDetail[1] + ", " + addressDetail[2] + " ,</span><br/><span>" + addressDetail[3] + " , " + addressDetail[4];

                $(".show-address").html("<span>" + addressDetail + "</span>");
                $(".show-addresses").html(reviewaddress);
                $(".address-select").on("change",function(){
                    addressDetail = $(".address-select option:selected").val();
                    addressDetail = (addressDetail).split(",");
                    if (addressDetail[0].match(/VILLA/)) {
                        addressType = "VILLA";
                    } else {
                        addressType = "APARTMENT"
                    }
                    addressDetail = addressDetail[0] + " ,</span><br/><span>" + addressDetail[1] + ", " + addressDetail[2] + " ,</span><br/><span>" + addressDetail[3] + " , " + addressDetail[4];
                    $(".show-address").html(addressDetail);
                    calculateCharges(subcategoryCharge);

                });
                calculateCharges();
                $(".delete-address, .edit-address").removeClass("hidden");
                $.LoadingOverlay("hide", true);
            } else {
                $(".address-select").html("<option>You have not added any addresses at yet.</option>");
                $(".show-address").html("");
                $(".delete-address, .edit-address").addClass("hidden");
                $.LoadingOverlay("hide", true);
            }

        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}
function getAllCategories() {
    console.log('into getAllCategories')
    var userInfo = JSON.parse(localStorage.getItem("h_user"));
    udata = userInfo;
    if (userInfo && userInfo !== "null") {
        var params = {};
        params.Auth = udata.data.accessToken;
        callApi.getUrl(apiUrl + "/api/category/getAllCategories", params, function(err, res) {
            if (!err) {
                console.log('! err');
                //clickEvents();
                var category = {}, select_Template, html, service = {}, serviceTypeArray=[], typeOfService = {}, serviceType;
                var cat = res.data;
                var serviceIssue, serviceBased;
                serviceIssue = (document.URL).split("?")[1];
                serviceIssue = serviceIssue.split("&");
                serviceIssue[0] = decodeURI(serviceIssue[0].split("=")[1]);
                if (serviceIssue[0] == "emergency" || serviceIssue[0] == "sameday") {
                    $("#datepicker").removeClass("pointer");
                    $("#datepicker").attr("id","");
                    select_Template = '<option value="">Choose need or issue</option>{{#serviceType}}<option value="{{subCategoryName}}" data-cat="{{category}}" data-emergency="{{emergencyCharge}}">{{subCategoryName}}</option>{{/serviceType}}';
                    var slot = {
                        "2":"2AM - 4AM", "4":"4AM - 6AM","6":"6AM - 8AM",
                        "8":"8AM - 10AM","10":"10AM - 12AM","12":"12AM - 2PM",
                        "14":"2PM - 4PM","16":"4PM - 6PM","18":"6PM - 8PM",
                        "20":"8PM - 10PM","22":"10PM - 12PM","0":"12PM - 2AM"
                    };
                    $('#category_img').attr('src','../images/homemaintenance.png');
                    var time = new Date().getHours();
                    if(serviceIssue[0] == "emergency") {
                        $("#priority").html("<span style='color:#FA7E7E;'>Emergency </span>");
                        serviceBasedType = "EMERGENCY";
                        time = time%2 == 0 ? time :time + 1;
                        $(".select-time").html("<option value='" + time + "'>" + slot[time] + "</option>");
                    } else if (serviceIssue[0] == "sameday") {
                        $("#priority").html("<span style='color:#FA7E7E;'>Sameday </span>");
                        serviceBasedType = "SAMEDAY";
                        time = time + 2;
                        if (time > 7 && time < 16) {
                            var option = ""
                            time = time%2 == 0 ? time : time + 1;
                            while(time < 16) {
                                option += "<option value='" + time + "'>" + slot[time] + "</option>";
                                time = time + 2;
                            }
                            $(".select-time").html(option);
                        } else {
                            // alert("There is no slot available to book an appointment..");
                            $("#showErrMsg").html("There is no slot available to book an appointment.");
                            $("#msgmodal").removeClass("hidden");
                            return;
                        }

                    }
                    //$(".select-time").html();
                    for(var k in cat) {
                        category[cat[k].name] = cat[k];
                    }
                    var emergencyCategory = Object.keys(category);
                    for(var k in emergencyCategory) {
                        var emergencySubcategory = category[emergencyCategory[k]].subcategories;
                        for(var j in emergencySubcategory) {
                            var data = {};
                            if (serviceIssue[0] == "emergency") {
                                if (emergencySubcategory[j].emergencyBookingAllowed) {
                                    data.subCategoryName = emergencySubcategory[j].subCategoryName;
                                    data.category = emergencyCategory[k];
                                    data.emergencyCharge = emergencySubcategory[j].callOutEmergencyVariable;
                                    serviceTypeArray.push(data);
                                }
                            } else if (serviceIssue[0] == "sameday") {
                                data.subCategoryName = emergencySubcategory[j].subCategoryName;
                                data.category = emergencyCategory[k];
                                serviceTypeArray.push(data);
                            }
                            emergencyCharge = emergencySubcategory[j]["callOutEmergencyVariable"];
                            if(emergencySubcategory[j]["unitCharges"]["firstUnitCharges"]) {
                                typeOfService[emergencySubcategory[j]["subCategoryName"]] = "fp";
                            } else if(emergencySubcategory[j]["callOutCharges"]) {
                                typeOfService[emergencySubcategory[j]["subCategoryName"]] = "ip";
                            } else if(emergencySubcategory[j]["callOutCharges"] == 0) {
                                typeOfService[emergencySubcategory[j]["subCategoryName"]] = "surveyBased";
                            }
                        }
                    }
                    service.serviceType = serviceTypeArray;
                    html = Mustache.render(select_Template, service);
                    $(".servicetype-select").html(html);
                    $(".servicetype-select").on("change", function(){
                        var subcategory, categoryname, emergencycharge;
                        subcategory = $(".servicetype-select").val();
                        emergencycharge = $(".servicetype-select option:selected").attr("data-emergency");
                        categoryname = $(".servicetype-select option:selected").attr("data-cat");
                        newURL = (window.location.href).split("=")[0];
                        newURL = newURL + "=" + categoryname + "&serviceType=" + subcategory + "&" + typeOfService[subcategory];
                        console.log(newURL);
                        for(var k in cat) {
                            if (cat[k].name == categoryname) {
                                category[cat[k].name] = cat[k];
                            }
                        }
                        clickEvents(category[categoryname]);
                        selectedCategory = category[categoryname];
                        console.log(typeOfService);
                        switch(typeOfService[subcategory]){
                            case "ip" :
                                $("#inspectionservice").click();
                                $("#totalcharges").html("To be decided");
                                break;
                            case "fp" :
                                $("#fixedservice").click();
                                break;
                            case "surveyBased" :
                                console.log("into survey")
                                $("#survey").click();
                                break;
                        }
                        $(".inspection-fixed-continue").click(function() {
                            setTimeout(function(){
                                if (emergencycharge && emergencycharge != "0") {
                                    $("#showErrMsg").html("An " + serviceIssue[0] + " charge of AED " + emergencycharge + " is applicable to this booking");
                                    $("#msgmodal").removeClass("hidden");
                                    $("body").addClass("body-flow");
                                    $("#emergencyCharge").html(emergencycharge);
                                    $("#emergencySameday").html(serviceIssue[0] + " Charges");
                                    $("#emergencyCharge").parent().removeClass("hidden");
                                }
                                getBookingInfo(category);
                            },500);
                        });
                        
                    });

                } else {
                    select_Template = '<option value="" disabled selected>Choose need or issue</option>{{#serviceType}}<option value="{{.}}">{{.}}</option>{{/serviceType}}';

                    for(var k in cat) {
                        if (cat[k].name == serviceIssue[0]) {
                            category[cat[k].name] = cat[k];
                            if(category[cat[k].name]['name'] === decodeURI(document.URL).split('?')[1].split('=')[1]){
                                categoryImages = category[cat[k].name]['imageURL']['original'];
                                console.log(categoryImages);
                            }
                        }
                        
                    }
                    if(categoryImages){
                        $('#category_img').attr('src',categoryImages);
                    } else{
                        $('#category_img').attr('src','../images/homemaintanance.png');
                    }
                    serviceType = category[serviceIssue[0]].subcategories;
                    for (var k in serviceType) {
                        if (serviceType[k]["unitCharges"]["firstUnitCharges"]) {
                            serviceTypeArray.push(serviceType[k]["subCategoryName"]);
                            typeOfService[serviceType[k]["subCategoryName"]] = "fp";//fp:fixed price
                        } else if (serviceType[k]["callOutCharges"]) {
                            serviceTypeArray.push(serviceType[k]["subCategoryName"]);
                            typeOfService[serviceType[k]["subCategoryName"]] = "ip";//ip = inspection based
                        } else if (serviceType[k]["callOutCharges"] == 0) {
                            serviceTypeArray.push(serviceType[k]["subCategoryName"]);
                            typeOfService[serviceType[k]["subCategoryName"]] = "surveyBased";
                        }
                    }
                    service.serviceType = serviceTypeArray;
                    html = Mustache.render(select_Template, service);
                    $(".servicetype-select").html(html);
                    $(".servicetype-select").on("change", function(){
                        $('.price-details-btn').hide();
                        var subcategory;
                        subcategory = $(".servicetype-select").val();
                        newURL = window.location.href + "&serviceType=" + subcategory + "&" + typeOfService[subcategory];
                        clickEvents(category[serviceIssue[0]]);
                        selectedCategory = category[serviceIssue[0]];
                        switch(typeOfService[subcategory]){
                            case "ip" :
                                $("#inspectionservice").click();
                                break;
                            case "fp" :
                                $("#fixedservice").click();
                                break;
                            case "surveyBased" :
                                $("#survey").click();
                                break;
                        }
                        $(".inspection-fixed-continue").click(function() {
                            getBookingInfo(category);
                        });
                    });
                }
            } else {
                callApi.error(err);
            }
        });
        callApi.getUrl(apiUrl + "/api/customer/getMyFavGenie", params, function(err,results){
            if (!err) {
                var favGenie = results.data;
                if (favGenie.length > 0) {
                    for(var k in favGenie) {
                        if (favGenie[k]["categoryID"]["name"] == serviceType[0]) {
                            favgenie = true;
                            break;
                        }
                    }

                }
            }
        });
    } else {
        localStorage.setItem("h_user",null);
        window.location.href = "../index.html";
    }
}
$('.promo-btn').click(function(){
    if(!$('.servicetype-select option:selected').val()){
        var message = {"msg":"Please choose subCategory first."}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
    }
});
$("#fileInput").click(function() {
    $("#file-input1").click();
});

function getBookingInfo(category) {
    console.log("into get booking info");
    var serviceIssue, issue = [],cat, subcat = {}, subcatArray = [],serviceBased,
        issue_template = '<select class="select-issue">{{#ques}}<option disabled selected value="">{{ques}}</option>{{/ques}}{{#answer}}<option ans-id="{{id}}">{{ans}}</option>{{/answer}}';
    console.log(newURL);
    serviceIssue = newURL.split("?")[1];
    serviceIssue = serviceIssue.split("&");
    serviceBased = (serviceIssue[serviceIssue.length - 1]);
    console.log("service based"+serviceBased);
    serviceIssue[0] = decodeURI(serviceIssue[0].split("=")[1]);
    serviceIssue[1] = decodeURI(serviceIssue[1].split("=")[1]);
    serviceType[0] = serviceIssue[0];
    serviceType[1] = serviceIssue[1];
    cat = category[serviceIssue[0]];
    subcat.subCatName = cat.subcategories;
    console.log(serviceBased);
    if (serviceBased == "ip") {
        console.log('into inspection based service');
        $(".baseService").html("INSPECTION BASED SERVICE");
        $("#labor-p").show();
        $("#labortext").html("Labor (including callout)");
        $("#material-p").hide();
        $("#service-p").hide();
        $("#other-p").hide();
        $("#type").html("Inspection based");
        $("#totalcharges").html("To be decided");
    } else if(serviceBased == "fp") {
        console.log('into service based service');
        $(".baseService, #type").html("FIXED PRICE SERVICE");
        $("#type").html("Fixed price");
        $("#service-p").show();
        $("#labor-p").hide();
        $("#material-p").hide();
        $("#other-p").hide();
        $("#labor-p", "#other-p","#material-p").hide();
    } else if(serviceBased == "surveyBased") {
        console.log(serviceBased);
        $(".baseService, #type").html("SURVEY BASED SERVICE");
        $("#type").html("Survey based");
        $("#labortext").html("Labor (survey charges)");
        $("#labor-p").show();
        $("#material-p").hide();
        $("#service-p").hide();
        $("#other-p").hide();
    }

    if (serviceBasedType == "EMERGENCY") {
        $("#other-p").show();
    }
    priceBasedService = serviceBased;

    for (var k in subcat.subCatName) {
        if (subcat.subCatName[k]["subCategoryName"] === serviceIssue[1]) {
            subcat.subcategories = subcat.subCatName[k];
            subcat.id = subcat.subCatName[k]["_id"];
            subcat.ques = subcat.subcategories.question;
            subcat.subCatAns = subcat.subcategories.answers;
            for (var id in subcat.subCatAns) {
                var data = {};
                if (subcat.subCatAns[id]["is_block"] == false) {
                    data.id = subcat.subCatAns[id]["_id"];
                    data.ans = subcat.subCatAns[id]["answer"];
                    subcatArray.push(data);
                }
            }
            if (subcat.subcategories && subcat.subcategories.Notes) {
                subcat.note = ((subcat.subcategories.Notes).replace(/\n/g, '<br/>')).split("Note");
            }
            if (subcat.note && subcat.note[0]) {
                subcat.note[0] = /*(subcat.note[0]).replace(/\n/g, '<br/>')*/subcat.note[0];
                $("#service-included").html(subcat.note[0]);
            }
            if (subcat.note && subcat.note[1]) {
                subcat.note[1] = /*(subcat.note[1]).replace(/\n/g, '<br/>')*/subcat.note[1];
                $("#serviceDetails-note").html("Note" + subcat.note[1]);
            }
            /*if (subcat.subcategories.unitCharges.firstUnitCharges) {
                firstcharge = subcat.subcategories.unitCharges.firstUnitCharges;
            }
            if (subcat.subcategories.unitCharges.villaCharges) {
                villaCharge = subcat.subcategories.unitCharges.villaCharges;
            }
            if (subcat.subcategories.unitCharges.restUnitCharges) {
                restUnitcharge = subcat.subcategories.unitCharges.restUnitCharges;
            }*/
            if (subcat.subcategories.hourlyCharges) {
                additionalHourCharge = subcat.subcategories.hourlyCharges;
            }

            $("#callOutCharges").html(subcat.subcategories.callOutCharges + " AED");
            $("#laborCharge").html(subcat.subcategories.callOutCharges);
            $("#materialCharge").html();
            $("#otherCharge").html();
            $("#promo").html();
            if (serviceBased == "fp") {
                $("#totalcharges, #totalchargess").html(subcat.subcategories.callOutCharges + 0 + 0 - promo);
            }
            $(".apply-promo").click(function(){
                var data = new FormData();
                data.append("customerID", udata.data.userDetails["_id"]);
                data.append("categoryID", cat["_id"]);
                data.append("subCategoryID", subcat.id);
                data._method = "POST";
                data.Auth = udata.data.accessToken;
                if ($(".enterPromo").val()) {
                    data.append("promoCode", $(".enterPromo").val());
                } else {
                    var message = {"msg":"Please enter promocode to apply."}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Please enter promocode to apply."});
                    return;
                }
                //$.LoadingOverlay("show");
                callApi.queryUrl(apiUrl + "/api/customer/applyPromoCode", data, function(err, results){
                    if (!err) {
                        $.LoadingOverlay("hide", true);
                        results = JSON.parse(results);
                        promo = results.data["_id"];
                        console.log(promo);
                        var message = {"msg":"Promocode applied successfully."}
                        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                        $('.msg-modal').modal('show');
                        //$("#totalcharges, #totalchargess").html(subcat.subcategories.callOutCharges + 0 + 0 - promo);
                        //$("#totalcharges, #totalchargess").html(subcat.subcategories.callOutCharges + 0 + 0 - promo);
                    } else {
                        $.LoadingOverlay("hide", true);
                        callApi.error(err);
                    }
                });
            });
        }
    }
    $(".issue-booking").attr("data-id", subcat.id);
    $(".issue-booking").html(serviceIssue[0] + " | " + serviceIssue[1]);
    $(".cat-img").attr("src", cat.imageURL.thumbnail);
    $(".air-conditioning").html('<img src="' + cat.imageURL.thumbnail + '" class="ac-img">' + serviceIssue[0] + " | " + serviceIssue[1]);
    subcat.answer = subcatArray;
    //console.log(subcat);
    $("#add-info").html(Mustache.render(issue_template, subcat));
    $(".select-issue").on("change",function(){
        var selectedSubCategory = selectedCategory;
        var issue = $(".servicetype-select option:selected").val();
        console.log("issues are : ", issue);
        for(var k in selectedSubCategory.subcategories) {
            if(selectedSubCategory["subcategories"][k]["subCategoryName"] == issue) {
                subcategoryCharge = selectedSubCategory["subcategories"][k];
                console.log(subcategoryCharge);
                calculateCharges(subcategoryCharge);
            }
        }
        if(issue != 'issue'){
            $('.price-details-btn').removeClass('hidden');
            $('.price-details-btn').show();
            return;
        }else{
            $('.price-details-btn').hide();
        }

    });
    getAndUpdateCustomerAddress();
}

function initMap(lat, long) {
    setTimeout(function() {
        var map_options = {
            center: new google.maps.LatLng(lat, long),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var myLatlng = new google.maps.LatLng(lat, long);
        var map = new google.maps.Map(document.getElementById("map"), map_options);
        var markers = new google.maps.Marker({
            position:myLatlng,
            draggable:true,
            map:map
        });

        markers.setMap(map);

        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(markers, 'dragend', function(event) {
            if (updateAddress) {
                editlat = this.getPosition().lat();
                editlong = this.getPosition().lng();
            }
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, markers);
                        var streetaddress = "";
                        for (var k in results[0]["address_components"]) {
                            if (results[0]["address_components"][k]["types"][0] == "locality") {
                                var city = results[0]["address_components"][k]["long_name"];
                                $("#city").val(city);
                            }
                        }
                    }
                }
            });
        });

        var input = document.getElementById("searchMap");
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);

        var marker = new google.maps.Marker({map: map, draggable:true});
        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'dragend', function(event) {
            if (updateAddress) {
                editlat = this.getPosition().lat();
                editlong = this.getPosition().lng();
            }
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, markers);
                        for (var k in results[0]["address_components"]) {
                            if (results[0]["address_components"][k]["types"][0] == "locality") {
                                var city = results[0]["address_components"][k]["long_name"];
                                $("#city").val(city);
                            }
                        }
                    }
                }
            });
        });

        var addressWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, "place_changed", function()
        {
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(15);
            }
            markers.setPosition(place.geometry.location);

            if (place["formatted_address"]) {
                addressWindow.setContent(place.formatted_address);
                var address;
                address = place.formatted_address;
                address = address.split("-");
                var addressLen = address.length;
                if (addressLen < 3) {
                    $("#city").val(address[1]);
                } else {
                    $("#city").val(address[addressLen-2]);
                }
            }
        });

        $("#map-modal").on("shown.bs.modal", function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter(myLatlng);
        });

    }, 3000);
}


$('#service-modal-continue').click(function(){
    $('#service-modal').modal('hide');
});
function hideCurrentModal(elem){
    //$("body").addClass("modalOpen");
    console.log(elem);
    var id = ((((elem.parentNode).parentNode).parentNode).parentNode);
    console.log(id);
    id = $(id).attr("id");
    console.log(id)
    $("#" + id).modal("hide");
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
}
$('#map-footer,.msg-modal-continue').click(function(){
    $("#map-modal").modal("hide");
    $('.msg-modal').modal('hide');
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
});
$('.crossImg').click(function(){
    setTimeout(function(){
        $("body").removeClass("modal-open");
    },500);
})

function calculateCharges(subcategoryCharge) {
    var problem = $(".select-issue option:selected").val();
    if(problem && subcategoryCharge) {
        if (priceBasedService == "ip" || priceBasedService == "surveyBased") {
            $("#otherCharge").html("0");
            $("#totalcharges, #totalchargess").html("To be decided");
            var showCallOut = subcategoryCharge.callOutCharges;
            $("#showPrice").html('<button class="price-details-btn hidden">' + showCallOut + ' AED<span class="glyphicon glyphicon-menu-down menu-down-size"></span></button>');
            var calloutVar = "";
            if (priceBasedService == "ip") {
                calloutVar = "Call Out Charges";
            } else if (priceBasedService == "surveyBased") {
                calloutVar = "Survey Charges";
            }
            //console.log("callout var : ",priceBasedService, calloutVar);
            $("#pricebased").html('<p class="call-out-charges">' + calloutVar + '* : <span class="active-color">' + subcategoryCharge.callOutCharges +' AED</span></p>');
            if (additionalCount == 0) {
                additionalCount++;
                if (additionalHourCharge) {
                    $("#additonalhourcharges").show();
                    $("#additonalhourcharges").html("Additional Hour Charges* : <span class='active-color'>" + additionalHourCharge + " AED</span>")
                }
            }
            $('.price-details-btn').click(function(){
                console.log('into price');
                $('#service-modal').modal('show');
            });
            
            if (serviceBasedType == "EMERGENCY" && subcategoryCharge.emergencyBookingAllowed) {
                $("#emergencycharges").html("Emergency Charges* : <span class='active-color'>" + subcategoryCharge.callOutEmergencyVariable + " AED</span>")
                $("#emergencycharges").removeClass("hidden");
                $('.price-details-btn').click(function(){
                console.log('into price');
                $('#service-modal').modal('show');
            });
            }
        } else if (priceBasedService == "fp") {
            $("#additonalhourcharges").hide();
            $("#totalcharges, #totalchargess").html("0");
            $("#pricingnote").html(pricingPolicy);
            var charge = $(".select-issue option:selected").val();
            var noOfunit = charge.match(/[0-9]+/g);
            var totalCharge = subcategoryCharge.unitCharges.firstUnitCharges + (parseInt(noOfunit)-1) * subcategoryCharge.unitCharges.restUnitCharges;
            if(addressType == "VILLA") {

                totalCharge = ( (totalCharge + subcategoryCharge.unitCharges.villaCharges) >= subcategoryCharge.unitCharges.minimumVillaCharges) ? (totalCharge + subcategoryCharge.unitCharges.villaCharges) : subcategoryCharge.unitCharges.minimumVillaCharges;
            }
            //console.log("totalCharge is: ", totalCharge);
            $("#showPrice").html('<button class="price-details-btn hidden">' + totalCharge + ' AED<span class="glyphicon glyphicon-menu-down menu-down-size"></span></button>');
            $("#pricebased").html('<p class="call-out-charges">Service Charges* : <span class="active-color">' + totalCharge + 'AED</span></p>');
            $('.price-details-btn').click(function(){
                console.log('into price');
                $('#service-modal').modal('show');
            });
            if (serviceBasedType == "EMERGENCY" && subcategoryCharge.callOutEmergencyVariable) {
                $("#emergencycharges").html("Emergency Charges* : <span class='active-color'>" + subcategoryCharge.callOutEmergencyVariable + " AED</span>")
                $("#emergencycharges").removeClass("hidden");
                $('.price-details-btn').click(function(){
                console.log('into price');
                $('#service-modal').modal('show');
            });
            }
            $("#totalcharges, #totalchargess, #otherCharge").html(totalCharge);
            console.log(totalCharge);
            console.log(emergencycharge);
            if (emergencycharge) {
                $("#totalcharges, #totalchargess").html(totalCharge + parseInt(emergencycharge));
            }
        }

        
    }
}


function getLocation() {
    if (navigator.geolocation) {
        console.log(navigator.geolocation);
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}
function showPosition(position) {
    initMap(position.coords.latitude,position.coords.longitude);
}

function clickEvents(selectedCategory) {
    return;
    var d = new Date();
    var date = new Date((d.getTime()) + 24*60*60*1000).toString();
    // console.log($("#datepicker").datepicker("getDate"));
    if (date.match(/Fri/gi)) {
        date = d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? ("0" + (d.getMonth() + 1)):((d.getMonth() + 1))) + "-" + (d.getDate() + 2);
    } else {
        date = d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? ("0" + (d.getMonth() + 1)):((d.getMonth() + 1))) + "-" + (d.getDate() + 1);
    }
    $("#datepickers").val(new Date(date).toDateString());
    $("#date-time, #date-times").html(new Date(date).toDateString() + " | " + $(".select-time option:selected").text());
    scheduleddate = date + "T06:30:00.000Z";
    // console.log(scheduleddate);

   /* $(".apply-promo").click(function(){
       var promoval = $(".enterPromo").val();
       if(!promoval) {
           alert("Please enter promocode to apply");
           return;
       } else if(promoval) {
           var checkSubCategory = $(".servicetype-select option:selected").val();
           if (!checkSubCategory) {
               alert("Please choose need or issue");
               return;
           }
       }
    });*/
    
}

document.addEventListener('keyup', function(e) {
    if (e.keyCode == 27) {
        $("#add-address").modal("hide");
    }
});
function getSubCategory(){
    var serviceType;
    serviceType = $('.servicetype-select option:selected').val();
    if (!serviceType) {
        showerror(msg.MESSAGE_SERVICE_TYPE_ERROR);
        return;
    }
    if(serviceType){
         if (fixedPriceServiceArray.indexOf(serviceType) !== -1) {
            servicetype = "fp";
            $("#fixedservice").click();
        } else {
            servicetype = "ip";
            $("#inspectionservice").click();
        }
    }

}
$("#hideerrorbox").click(function(){
    $("#msgmodal").addClass("hidden");
});
$(document).ready(function(){
    $('.price-details-btn').hide();
    $('.price-details-btn').css('display','none');
    $.LoadingOverlay("show");
    getAndUpdateCustomerAddress();
    console.log('into document');
    getAllCategories();
});




console.log((document.URL).split('?')[1].split('=')[1]);
var d = new Date();
var date = new Date(d.getTime()).toString();
var urlService =((document.URL).split("?")[1]).split("=")[1];
if (urlService == "emergency" || urlService == "sameday"){
    serviceBasedType = urlService.toUpperCase();
}
if (serviceBasedType == "SCHEDULED") {
    date = new Date((d.getTime()) + 24*60*60*1000).toString();
}
// console.log($("#datepicker").datepicker("getDate"));
if (date.match(/Fri/gi) && serviceBasedType == "SCHEDULED" ) {
    date = d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? ("0" + (d.getMonth() + 1)):((d.getMonth() + 1))) + "-" + (d.getDate() + 2);
} else if (serviceBasedType == "SCHEDULED") {
    date = d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? ("0" + (d.getMonth() + 1)):((d.getMonth() + 1))) + "-" + (d.getDate() + 1);
} else {
    date = d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? ("0" + (d.getMonth() + 1)):((d.getMonth() + 1))) + "-" + (d.getDate());
}
$("#datepickers").val(new Date(date).toDateString());
$("#date-time, #date-times").html(new Date(date).toDateString() + " | " + $(".select-time option:selected").text());
scheduleddate = date + "T06:30:00.000Z";
console.log(scheduleddate);
$("#datepicker").click(function(){
    $( "#datepicker" ).datepicker({
        minDate: 1,
        beforeShowDay: function(date){ return [date.getDay() == 0 ||date.getDay() == 0 ||date.getDay() == 1||date.getDay() == 2||date.getDay() == 3||date.getDay() == 4||date.getDay() == 6,""]},
        onSelect:function(){
            scheduleddate = ($("#datepicker").datepicker("getDate"));
            scheduleddate = scheduleddate.getTime();
            scheduleddate += 12*60*60*1000;
            scheduleddate = new Date(scheduleddate).toISOString();
            $("#datepickers").val($("#datepicker").datepicker("getDate").toDateString());
            $("#datepicker").datepicker("destroy");
            $("#datepicker").html('<span class="fa fa-calendar fa-2x"></span>');
            $("#date-time, #date-times").html($("#datepickers").val() + " | " + $(".select-time option:selected").text());
        }
    });
});


$(".select-time").on("change",function(){
    var dateTime = "";
    if ($("#datepickers").val()) {
        dateTime += $("#datepickers").val() + " | ";
    }
    dateTime +=  $(".select-time option:selected").text();
    $("#date-time, #date-times").html(dateTime);
});
$('.select-issue').on('click',function(){
    if(!($('.servicetype-select option:selected').val())){
        var message = {"msg":"Please select subcategory first"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
});
$(".select-issue").on("change",function(){
    
    var selectedSubCategory = selectedCategory;
    var issue = $(".servicetype-select option:selected").val();
    for(var k in selectedSubCategory.subcategories) {
        if(selectedSubCategory["subcategories"][k]["subCategoryName"] == issue) {
            subcategoryCharge = selectedSubCategory["subcategories"][k]
            calculateCharges(subcategoryCharge);
        }
    }
    var checkAnswer = $(".select-issue option:selected").val();
    console.log(checkAnswer);
    if(checkAnswer != 'issue'){
        $('.price-details-btn').removeClass('hidden');
        $('.price-details-btn').show();
        return;
    }else{
        $('.price-details-btn').hide();
    }
});
$(".submit-address").click(function(){
    var data = new FormData();
    var name, apartmentNo, streetAddress, community, city, emirate, lat, long;
    name = $("#nickName").val();
    apartmentNo = $("#apartmentNo").val();
    streetAddress = $("#streetAddress").val();
    community = $("#community").val();
    city = $("#city").val();
    emirate = /*$("#emirate").val()*/"UAE";
    $('#add-address').css('overflow-y','scroll');
    if (city) {
        if (!streetAddress) {
            var message = {"msg":"Please enter your street address"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.error({message : "Please enter your street address"});
            return;
        }
        /*if (!emirate) {
            $.growl.error({message : "Please enter your emirate"});
            return;
        }*/
        var geocoder =  new google.maps.Geocoder();
        var place = city + ", " + emirate;
        geocoder.geocode( { 'address':place }, function(results, status) {
            if ( results[0]) {
                lat = results[0].geometry.location.lat();
                long  = results[0].geometry.location.lng();
                if (!name) {
                    var message = {"msg":"Please enter a nickname"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Please enter a nickname"});
                    return;
                }
                if (!apartmentNo) {
                    var message = {"msg":"Please enter your Apartment No."}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Please enter your Apartment No."});
                    return;
                }
                if (!community) {
                    var message = {"msg":"Please enter your community"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Please enter your community"});
                    return;
                }
                if (!lat) {
                    var message = {"msg":"Sorry, Not able to find your latitude. Please enter the correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Sorry, Not able to find your latitude. Please enter the correct city"});
                    return;
                }
                if (!long) {
                    var message = {"msg":"Sorry, Not able to find your longitude. Please enter the correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Sorry, Not able to find your longitude. Please enter the correct city"});
                    return;
                }
                data.append("nickName", name);
                data.append("apartmentNo", apartmentNo);
                data.append("streetAddress", streetAddress);
                data.append("communtity", community);
                data.append("city", city);
                data.append("emirate", emirate);
                data.append("addressType",$("#addressType option:selected").val());
                data.append("IsdefaultAddress",$("#defaultAddress option:selected").val());
                $.LoadingOverlay("show");
                if (updateAddress) {
                    data.append("locationLat", editlat);
                    data.append("locationLong", editlong);
                    data._method = "PUT";
                    data.Auth = udata.data.accessToken;
                    data.append("addressesID",addressId);
                    callApi.queryUrl(apiUrl + "/api/customer/editAddress", data, function(err, result){
                        if (!err) {
                            var message = {"msg":"Your address has been updated."}
                            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                            $('.msg-modal').modal('show');
                            //$.growl.notice({message : "Your address has been updated."});
                            getAndUpdateCustomerAddress();
                            $("#add-address").modal('hide');
                        } else {
                            $.LoadingOverlay("hide",true);
                            callApi.error(err);
                        }
                    });
                } else {
                    data.append("locationLat",lat);
                    data.append("locationLong",long);
                    data._method = "POST";
                    data.Auth = udata.data.accessToken;
                    callApi.queryUrl(apiUrl + "/api/customer/addNewAddress", data, function(err, result){
                        if (!err) {
                            $.LoadingOverlay("hide",true);
                            var message = {"msg":"Your address has been added."}
                            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                            $('.msg-modal').modal('show');
                           // $.growl.notice({message : "Your address has been added."});
                            getAndUpdateCustomerAddress();
                            $("#add-address").modal('hide');
                        } else {
                            $.LoadingOverlay("hide",true);
                            callApi.error(err);
                        }
                    });
                }
            } else {
                if (!city) {
                    var message = {"msg":"Please enter correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    //$.growl.error({message : "Please enter correct city"});
                    return;
                }
                var message = {"msg":"Select within Dubai Only"}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
               // $.growl.error({message : "Select within Dubai Only"});
                return;
            }
        });
    } else {
        var message = {"msg":"Please enter your city"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        //$.growl.error({message : "Please enter your city"});
        return;
    }


});

$(".edit-address").click(function(){
    $("#map").html("");
    updateAddress = true;
    addressId = $(".address-select option:selected").attr("data-id");
    var count = 0, addressType, defaultAddress;
    for (var k in allAddress.data) {
        count++;
        if (allAddress.data[k]["_id"] === addressId) {
            var latlong = allAddress.data[k]["locationLongLat"];
            editlat = latlong[0];
            editlong = latlong[1];
            initMap(latlong[0], latlong[1]);
            count--;
            break;
        }
    }
    addressType = allAddress.data[count].addressType;
    defaultAddress = allAddress.data[count].IsdefaultAddress;
    $("#nickName").val(allAddress.data[count].nickName);
    $("#apartmentNo").val(allAddress.data[count].apartmentNo);
    $("#streetAddress").val(allAddress.data[count].streetAddress);
    $("#community").val(allAddress.data[count].community);
    $("#city").val(allAddress.data[count].city);
    //$("#emirate").val(allAddress.data[count].emirate);
    if (addressType == "VILLA") {
        var addressHtml = '<option selected value="VILLA">VILLA</option><option value="APARTMENT">APARTMENT</option>';
        $("#addressType").html(addressHtml);
    } else {
        var addressHtml = '<option selected value="APARTMENT">APARTMENT</option><option value="VILLA">VILLA</option>';
        $("#addressType").html(addressHtml);
    }
    if (defaultAddress == "FALSE") {
        var defaultAddressHtml = '<option selected value="FALSE">NO</option><option value="TRUE">YES</option>';
        $("#defaultAddress").html(defaultAddressHtml);
    } else {
        var defaultAddressHtml = '<option selected value="TRUE">YES</option><option value="FALSE">NO</option>';
        $("#defaultAddress").html(defaultAddressHtml);
    }
});

$(".addAddress").click(function() {
    $("#searchMap").val("");
    $("#map").html("");
    $("#nickName").val("");
    $("#apartmentNo").val("");
    $("#streetAddress").val("");
    $("#community").val("");
    $("#city").val("");
    //$("#emirate").val("");
    $("#addressType").html("<option value='APARTMENT' selected>APARTMENT</option><option value='VILLA'>VILLA</option>");
    $("#defaultAddress").html('<option value="TRUE" selected>YES</option><option value="FALSE">NO</option>');
    var latlong = countryInfo.loc.split(",");
    // getLocation();
    editlat = "25.2048";
    editlong = "55.2708";
    initMap(editlat, editlong);
});

$(".delete-address").click(function(){
    var addressId = $(".address-select option:selected").attr("data-id");
    var params = {};params.data = {};
    params._method = "DELETE";
    params.Auth = udata.data.accessToken;
    callApi.queryUrl(apiUrl + "/api/customer/removeAddress?addressId=" + addressId ,params, function(err, results){
        if (!err) {
            var message = {"msg":"Your address has been deleted"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.notice({message : "Your address has been deleted"});
            getAndUpdateCustomerAddress();
        } else {
            callApi.error(err);
        }
    });
});
function getTwoDigit(digit) {
    var digit  = digit;
    if(parseInt(digit) < 10 && digit.length < 2) {
        digit =  0 + digit;
    }
    return digit;
}
$(".book-box").click(function(){
    var issue = $(".select-issue option:selected").val();
    if(!issue){
        var message = {"msg":"Please choose need or issue"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    var data = new FormData();
    var workingday = day[new Date(scheduleddate).getDay()];
    var genieSelected;
    $('input[type="radio"]').each(function(){
        if($(this).is(":checked")){
            if (favgenie) {
                genieSelected = $(this).attr("value");
            } else if($(this).attr("value") == "FALSE") {
                genieSelected = $(this).attr("value");
            }
        }
    });
    if ($("#file-input").prop("files")[0]) {
        data.append("problemImage", $("#file-input").prop("files")[0]);
    }
    /*if ($("#file-inputs").prop("files")[0]) {
        data.append("problemImage", $("#file-inputs").prop("files")[0]);
    }*/
    if ($("#file-input1").prop("files")[0]) {
        data.append("problemImage", $("#file-input1").prop("files")[0]);
    }
    /*if ($("#file-inputs1").prop("files")[0]) {
        data.append("problemImage", $("#file-inputs1").prop("files")[0]);
    }*/

    if (!$(".select-issue option:selected").attr("ans-id")) {
        var message = {"msg":"Please select additional information"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if ($(".leave-instruction").val()) {
        data.append("problemDetails", $(".leave-instruction").val());
    }
    if (!scheduleddate) {
        // $.growl.error({message : "Please select a date"});
        var message = {"msg":"Please select a date"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (workingday === "FRIDAY" && serviceBasedType == "SCHEDULED") {
        var message = {"msg":"Please select a working day except FRIDAY."}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (!$(".address-select option:selected").attr("data-id")) {
        // $.growl.error({message : "It seems you have not added any addresses. Please add a new address"});
        var message = {"msg":"It seems you have not added any addresses. Please add a new address"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    if (scheduleddate) {
        var twodigit;
        console.log(scheduleddate);
        scheduleddate = scheduleddate.split("T");
        twodigit = scheduleddate[0].split("-");
        twodigit[2] = getTwoDigit(twodigit[2]);
        scheduleddate[0] = twodigit[0] + "-" + twodigit[1] + "-" + twodigit[2];
        scheduleddate[1] = parseInt($(".select-time option:selected").val())*60*60;
        var date = new Date(null);
        date.setSeconds(scheduleddate[1]); // specify value for SECONDS here
        scheduleddate[1] = date.toISOString().substr(11, 13);
        scheduleddate = scheduleddate[0] + "T" + scheduleddate[1];
        workingday = day[new Date(scheduleddate).getDay()];
        console.log(scheduleddate);
        //return;
    }
    data.append("subcategoryID", $(".issue-booking").attr("data-id"));
    data.append("answerID", $(".select-issue option:selected").attr("ans-id"));
    data.append("addressID", $(".address-select option:selected").attr("data-id"));
    data.append("serviceType", serviceBasedType);
    data.append("locationLat", udata.data.userDetails.currentLocation[0]);
    data.append("locationLong", udata.data.userDetails.currentLocation[1]);
    data.append("scheduleDate",scheduleddate);
    data.append("day", workingday);
    data.append("slot", $(".select-time option:selected").val());
    if (genieSelected) {
        data.append("favouriteGenie", genieSelected.toString());
    }
    if (promo) {
        data.append("promoID", promo);
    }

    data.Auth = udata.data.accessToken;
    data._method = "POST";
    $("#reviewBooking").click();
    $(".reviewDone").click(function(){
        // $.LoadingOverlay("show");
        callApi.queryUrl(apiUrl + "/api/appointment/bookAppointment", data, function(err, result){
            if (!err) {
                $("#review").modal('hide');
                var message = {"msg":"You have successfully booked an appointment"}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                // $.growl.notice({message : "You have successfully booked an appointment"});
                $.LoadingOverlay("hide", true);
                var jobid = JSON.parse(result);
                jobid = jobid.data.appointmentId;
                window.location.href = "../html/genieSearch.html?id=" + jobid;
            } else {
                $("#review").modal('hide');
                $.LoadingOverlay("hide", true);
                if(err.statusCode === 401) {
                    callApi.error(err);
                } else {
                    err = JSON.parse(err);
                    var message = {"msg":err.message}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }

            }
        });
    });
});
$("#add-info").click(function(){
    var issue = $(".select-issue option:selected").val();
    if(!issue){
        var message = {"msg":"Please choose need or issue."}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
});
// $(".book-box").click(function(){
//     console.log($('.servicetype-select option:selected').val());
//     if(!$('.servicetype-select option:selected').val()){
//         var message = {"msg":"Please choose need or issue first"}
//         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
//         $('.msg-modal').modal('show');
//         return;
//     }
//     var data = new FormData();
//     var workingday = day[new Date(scheduleddate).getDay()];
//     var genieSelected;
//     $('input[type="radio"]').each(function(){
//         if($(this).is(":checked")){
//             if (favgenie) {
//                 genieSelected = $(this).attr("value");
//             } else if($(this).attr("value") == "FALSE") {
//                 genieSelected = $(this).attr("value");
//             }
//         }
//     });
//     if ($("#file-input").prop("files")[0]) {
//         data.append("problemImage", $("#file-input").prop("files")[0]);
//     }
//     /*if ($("#file-inputs").prop("files")[0]) {
//         data.append("problemImage", $("#file-inputs").prop("files")[0]);
//     }*/
//     if ($("#file-input1").prop("files")[0]) {
//         data.append("problemImage", $("#file-input1").prop("files")[0]);
//     }
//     /*if ($("#file-inputs1").prop("files")[0]) {
//         data.append("problemImage", $("#file-inputs1").prop("files")[0]);
//     }*/

//     if (!$(".select-issue option:selected").attr("ans-id")) {
//         var message = {"msg":"Please select additional information"}
//         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
//         $('.msg-modal').modal('show');
//         //$.growl.error({message : "Please select additional information"});
//         return;
//     }
//     if ($(".leave-instruction").val()) {
//         data.append("problemDetails", $(".leave-instruction").val());
//     }
//     if (!scheduleddate) {
//         var message = {"msg":"Please select a date"}
//         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
//         $('.msg-modal').modal('show');
//         //$.growl.error({message : "Please select a date"});
//         return;
//     }
//     if (workingday === "FRIDAY") {
//         var message = {"msg":"Please select a working day except FRIDAY."}
//         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
//         $('.msg-modal').modal('show');
//         //$.growl.error({message : "Please select a working day except FRIDAY."});
//         return;
//     }
//     if (!$(".address-select option:selected").attr("data-id")) {
//         var message = {"msg":"It seems you have not added any addresses. Please add a new address"}
//         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
//         $('.msg-modal').modal('show');
//         //$.growl.error({message : "It seems you have not added any addresses. Please add a new address"});
//         return;
//     }
//     if (scheduleddate) {
//         scheduleddate = scheduleddate.split("T");
//         scheduleddate[1] = parseInt($(".select-time option:selected").val())*60*60;
//         var date = new Date(null);
//         date.setSeconds(scheduleddate[1]); // specify value for SECONDS here
//         scheduleddate[1] = date.toISOString().substr(11, 13);
//         scheduleddate = scheduleddate[0] + "T" + scheduleddate[1];
//         console.log(scheduleddate);
//     }
//     data.append("subcategoryID", $(".issue-booking").attr("data-id"));
//     data.append("answerID", $(".select-issue option:selected").attr("ans-id"));
//     data.append("addressID", $(".address-select option:selected").attr("data-id"));
//     data.append("serviceType", "SCHEDULED");
//     data.append("locationLat", udata.data.userDetails.currentLocation[0]);
//     data.append("locationLong", udata.data.userDetails.currentLocation[1]);
//     data.append("scheduleDate",scheduleddate);
//     data.append("day", workingday);
//     data.append("slot", $(".select-time option:selected").val());
//     if (genieSelected) {
//         data.append("favouriteGenie", genieSelected.toString());
//     }
//     if (promo) {
//         data.append("promoID", promo);
//     }

//     data.Auth = udata.data.accessToken;
//     data._method = "POST";
//     $("#reviewBooking").click();
//     $(".reviewDone").click(function(){
//         $.LoadingOverlay("show");
//         callApi.queryUrl(apiUrl + "/api/appointment/bookAppointment", data, function(err, result){
//             if (!err) {
//                 $.LoadingOverlay("hide", true);
//                 var jobid = JSON.parse(result);
//                 jobid = jobid.data.appointmentId;
//                 //$.growl.notice({message : "You have successfully booked an appointment"});
                
//                 console.log(jobid);
//                 window.location.href = "../html/genieSearch.html?id=" + jobid;
//             } else {
//                 $.LoadingOverlay("hide", true);
//                 callApi.error(err);
//             }
//         });
//     });
// });
$('#file-input, #file-inputs, #file-input1, #file-inputs1').change( function(event) {
    var id = $(this).parent().attr("id");
    $("#" + id + " img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
});
$('input[type=radio]').click(function(){
    var genieval = $(this).attr("value");
    if (genieval == "TRUE" && favgenie == false) {
        $("#nogenie").click();
        return;
    }
    $(".genieSelected").attr("src", "../images/geniefade.png");
    $("#" + $(this).parent().attr("id") + " img").attr("src", "../images/genieicon.png");
});