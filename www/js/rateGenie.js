/**
 * Created by sumit on 30/10/16.
 */
var udata, appointmentId, rating, favgenie = false;

function getGenie() {

    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        var params, genie;
        udata = JSON.parse(userData);
        appointmentId = document.URL;
        if (appointmentId.indexOf("genieid") !== -1) {
            appointmentId = (appointmentId.split("genieid=")[1]).trim();
        } else {
            window.location.href = "../index.html";
        }
        params = new FormData();
        params.Auth = udata.data.accessToken;
        params.append("appointmentId", appointmentId);
        params._method = "POST";
        $.LoadingOverlay("show");
        callApi.queryUrl(apiUrl + "/api/customer/getJobDetails", params, function(err, response){
            if (!err) {
                var data = JSON.parse(response);
                data = data.data[0];
                var driverData = data.driverData;
                $(".genie-name").html(driverData.name);
                $(".genie-img").attr("src",driverData.profilePicURL.thumbnail);
                $(".active-jobid").html("JOBID : " + data.uniqueCode);
                $(".cat-img").attr("src", data.categoryImage.thumbnail);
                $(".issue").html(data.categoryName + " (" + data.subCategory.subCategoryName + ")");
                $.LoadingOverlay("hide", true);
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    } else {
        window.location.href = "../index.html";
    }

}

$("input[type=radio]").click(function(){
    rating = $(this).attr("value");
});
$("input[type=checkbox]").click(function(){
    favgenie = true;
});

$("#rateGenie").click(function(){
    var params = new FormData();
    if (!rating) {
        $.growl.error({message : "Please rate your Genie, It'll help us to improve our services."});
        var message = {"msg":"Please rate your Genie, It'll help us to improve our services."}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
    params.append("appointmentId", appointmentId);
    params.append("driverRating", rating);
    params.append("favouriteGenie", favgenie);
    var comment = ($("textarea").val());
    if (comment) {
        params.append("driverComment", comment);
    }
    params._method = "POST";
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/driverRatingComments", params, function(err,results){
        if(!err){
            $.LoadingOverlay("hide");
            // $.growl.notice({message : "You have rated your genie"});
            var message = {"msg":"You have rated your genie"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //console.log(results);
            setTimeout(function(){
                window.location.href = "../index.html";
            },1000);
        } else {
            $.LoadingOverlay("hide");
            callApi.error(err);
        }
     });
});


$(document).ready(function(){
    getGenie();
});