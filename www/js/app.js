// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','ngCordova'])
.run(function($ionicPlatform, $cordovaPushV5, $cordovaAppVersion) {
  $ionicPlatform.ready(function() {
    console.log('into ionic platform ready')
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(false);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    //console.log(document.URL);
    /*$ionicPlatform
    .registerBackButtonAction(function(e) {
            if ($ionicHistory.currentView().stateName == 'app.bookings') {
              $rootScope.confirmationMessage = 'Are you sure you want to sign-out ?'
              $rootScope.backButtonModal.show();
              $rootScope.accept = function(){
                ionic.Platform.exitApp();
              }
              $rootScope.reject = function(){
                console.log('into reject')
                $rootScope.backButtonModal.hide();
              }
            }
            else

            if ($rootScope.backButtonPressedOnceToExit) {
                ionic.Platform.exitApp();
            } else if ($ionicHistory.backView()) {
                $ionicHistory.goBack();
            } else {
                $rootScope.backButtonPressedOnceToExit = true;
                window.plugins.toast.showShortCenter(
                    "Press back button again to exit",
                    function(a) {},
                    function(b) {}
                );
                setTimeout(function() {
                    $rootScope.backButtonPressedOnceToExit = false;
                }, 2000);
            }
            e.preventDefault();
            return false;
        }, 101)*/
    
    /*FCMPlugin.onTokenRefresh(function(token){
      console.log(token);
      localStorage.setItem('deviceToken',token);
    });
    console.log('into platform');
    FCMPlugin.getToken(function(token){
      localStorage.setItem('deviceToken',token);
    });
    FCMPlugin.onNotification(function(data){
      console.log('into not');
      console.log(data);
      if(data.wasTapped){
        //Notification was received on device tray and tapped by the user.
        console.log( JSON.stringify(data) );
      }else{
        //Notification was received in foreground. Maybe the user needs to be notified.
        console.log( JSON.stringify(data) );
       
      }
      notificationManager(data);
    });*/
    document.addEventListener("deviceready", function () {

    $cordovaAppVersion.getVersionNumber().then(function (version) {
      var appVersion = version;
      showAppVer(appVersion);
    });
  }, false);
  });
 
});
