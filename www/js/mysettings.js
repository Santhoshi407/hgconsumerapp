/**
 * Created by sumit on 20/10/16.
 */
var udata;
function updateSettings() {
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
        $.LoadingOverlay("show");
        updateDefaultAddress();
        $(".name").html(udata.data.userDetails.name);
        $(".phnNo").html(udata.data.userDetails.phoneNo);
        //$("#favGenie").html(udata.data.userDetails.favouriteGenie.length + " Favorite Genie");
        //console.log(udata);
        if (udata.data.defaultCards && udata.data.defaultCards.isDefault) {
            //console.log("hello");
            $('#checkoutCardNumber').val(udata.data.defaultCards.Digit);
            var $cardinput = $('#checkoutCardNumber');
            var cardicon = "";

            $('#checkoutCardNumber').validateCreditCard(function(result) {
                //console.log(result);
                if (result.card_type != null)
                {
                    switch (result.card_type.name)
                    {
                        case "visa":
                            cardicon = "fa fa-cc-visa";
                            break;

                        case "visa_electron":
                            cardicon = "fa fa-cc-visa";
                            break;

                        case "mastercard":
                            cardicon = "fa fa-cc-mastercard";
                            break;

                        case "maestro":
                            cardicon = "fa fa-cc-maestro";
                            break;

                        case "discover":
                            cardicon = "fa fa-cc-discover";
                            break;

                        case "amex":
                            cardicon = "fa fa-cc-amex";
                            break;

                        default:
                            $cardinput.css('background-position', '3px 3px');
                            break;
                    }
                } else {
                    $cardinput.css('background-position', '3px 3px');
                }
            });

            $("#defaultCard").html(udata.data.defaultCards.Digit);
            $("#icon").addClass(cardicon + " fa-2x");
        } else {
            $("#defaultCard").html("No saved card");
            $("#defaultCard").css("display","block");
            $("#debitCards p:nth-child(3)").hide();
        }

    } else {
        window.location.href = "../index.html";
    }
}

function updateDefaultAddress() {
    var params = {};
    params.Auth = udata.data.accessToken;
    callApi.getUrl(apiUrl + "/api/customer/getAllAddress", params, function(err, response){
        if (!err) {
            updateFavoriteGenies();
            var data = response.data;
            if(data.length > 0) {
                for (var k in data) {
                    if (data[k]["IsdefaultAddress"] === "TRUE") {
                        var address = data[k];
                        $("#address").html( address.community + ", " + address.city);
                        $.LoadingOverlay("hide", true);
                        break;
                    } else {
                        $(".defaultAddress p:nth-child(2)").html("No default address added");
                        $(".defaultAddress p:nth-child(3)").hide();
                        $.LoadingOverlay("hide", true);
                    }
                }
            } else {
                $(".defaultAddress p:nth-child(2)").html("No saved addresses");
                $(".defaultAddress p:nth-child(3)").hide();
                $.LoadingOverlay("hide", true);
            }
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}
function getAllCards() {
    var params = {};
    params.Auth = udata.data.accessToken;
    callApi.getUrl(apiUrl + "/api/customer/getAllMyCard", params, function(err, response){
        if (!err) {
            console.log(response);
        }
    });
}

function updateFavoriteGenies(){
    var params = {};
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getMyFavGenie", params, function(err, results){
        if (!err) {
            //console.log(results);
            var data = {};
            var genie = results.data;

            $("#favGenie").html(genie.length + " favorite Genie");

        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

$(document).ready(function(){
    updateSettings();
    $("#setting-pwd").click(function(){
        $("#change-pwd>div>.cross").removeClass("hidden");
    });
});