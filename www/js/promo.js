/**
 * Created by sumit on 30/10/16.
 */
//GET /api/customer/getMyFavGenie
var udata, month;
month = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
function getAllPromo(){
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "/";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getAllPromo", params, function(err, results){
        if (!err) {
            console.log(results);
            var data = {};
            var promotions = results;
            if (promotions.length > 0) {
                for(var k in results) {
                    var date = new Date(results[k].endTime);
                    results[k].endTime = "Valid till " + date.getDate() + " " + month[date.getMonth()] + ", " + date.getFullYear();
                }
                data.promo = results;
                $("#promo").html(Mustache.render(promoTemplate, data));
                $.LoadingOverlay("hide", true);
            } else {
                $("#promo").html("<p class='text-center' style='font-size:20px;color:#7cf2b1;'>There is no promotion avail to redeem.</p>");
                $.LoadingOverlay("hide", true);
            }
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        $("#showErrMsg").html("Copy not supported or blocked. Press Ctrl+c to copy.");
        $("#msgmodal").removeClass("hidden");
    } else {
        $("#showErrMsg").html("ID copied.");
        $("#msgmodal").removeClass("hidden");
    }
    if (typeof msgElem === "string") {
        msgElem = document.getElementById(msgElem);
    }
}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

function copyPromoCode(id, id1) {
    copyToClipboardMsg(document.getElementById(id), "msg");
    /*document.getElementById(id1).addEventListener("click", function() {
        copyToClipboardMsg(document.getElementById(id), "msg");
    });*/


}

$("#hideerrorbox").click(function(){
    $("#msgmodal").addClass("hidden");
});

$(document).ready(function(){
    getAllPromo();
});

var promoTemplate = '{{#promo}}<div class="col-xs-12 col-sm-12 col-md-12 column" style="background:#f7f7f7;margin-bottom:10px;">\n' +
    '                        <div class="row clearfix">\n' +
    '                            <div class="col-xs-2 col-sm-2 col-md-1 column promo-img-xs">\n' +
    '                                <div class="background-icon text-center vertical-align">\n' +
    '                                    {{#categoryID}}<img src="{{imageURL.thumbnail}}" style="margin:0 auto;">{{/categoryID}}\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-7 col-sm-7 col-md-8 column info-row vertical-align">\n' +
    '                                <span>\n' +
    '                                    <p class="active-color promo-text">{{description}}</p>\n' +
    '                                    <p id="{{_id}}">{{name}}</p>\n' +
    '                                    <p>{{endTime}}</p>\n' +
    '                                </span>\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-3 col-sm-3 col-md-3 column info-row vertical-align" style="padding:10px !important;">\n' +
    '                                <div class="setting-btn-row">\n' +
    '                                    <button class="setting-btn cc-btn" id="{{_id}}-click" onclick="copyPromoCode(\'{{_id}}\',\'{{_id}}-click\')" style="white-space:nowrap;">COPY CODE</button>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>{{/promo}}';
