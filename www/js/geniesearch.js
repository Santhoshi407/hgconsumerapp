/**
 * Created by sumit on 5/11/16.
 */
var udata, genieData,jobID, count = 0, longerCount = 0, bookingId;

function getJobID() {
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
        $.LoadingOverlay("show");
        var jobid = document.URL;
        if (jobid.indexOf("id=") !== -1) {
            jobID = jobid.split("id=")[1];
            $(".jobid").html(jobID);
            $(".continue-search").attr("onclick", "getmybookings(" + jobID + ");");
            getmybookings(jobID);
            $.LoadingOverlay("hide", true);

        }
    } else {
        window.location.href = "../index.html";
    }
}




function getmybookings(jobid) {
    var params = {};
    params.Auth = udata.data.accessToken;
    callApi.getUrl(apiUrl + "/api/customer/getmybookings", params, function(err, result){
        if (!err) {
            var bookingData = result.data.upcomingAppointment;
            for (var k in bookingData) {
                if(bookingData[k]["uniqueCode"] == jobid) {
                    var bookingservice = bookingData[k]["serviceType"];
                    bookingId = bookingData[k]["_id"];
                    if (bookingData[k]["status"] == "ASSIGNED") {
                        //window.location.href = "/genieFound.html?id=" + bookingData[k]["_id"];
                        var data = new FormData();
                        data.append("appointmentId",bookingData[k]["_id"]);
                        data.Auth = udata.data.accessToken;
                        data._method = "POST";
                        callApi.queryUrl(apiUrl + "/api/customer/getJobDetails", data, function(err, response){
                            if (!err) {
                                var result;
                                result = (JSON.parse(response)).data[0];
                                genieData = result.driverData;
                                $(".genie-name").html(genieData.name);
                                $(".genie-img").attr("src", genieData.profilePicURL.thumbnail);
                                $(".cat-img").attr("src", result.categoryImage.thumbnail);
                                $("#partnername").html(genieData.companyName);
                                $(".issue").html(result.categoryName + " ( " + result.subCategory.subCategoryName + " )");
                                window.location.href = '../index.html'
                                $("#genieSearch").hide();
                                $("#closenote").hide();
                                $("#genieFound").show();
                                window.onbeforeunload = '';
                                //break;
                            }
                        });
                    } else if (count < 90) {
                        //console.log("count", count);
                        setTimeout(function(){
                            //console.log("timeout", count);
                            count++;
                            longerCount++;
                            getmybookings(jobID);
                        },10000);
                    } else {
                        count = 0;
                        if (bookingservice == "EMERGENCY" || bookingservice == "SAMEDAY") {
                            $("#difficultSearch").show();
                            $("#genieFound").hide();
                            $("#genieSearch").hide();
                            if (longerCount >= 180) {
                                //console.log("longer", count);
                                count = 0;
                                $("#difficultSearch").hide();
                                $("#genieFound").hide();
                                $("#genieSearch").hide();
                                $("#longerSearch").show();
                            }
                        }
                    }
                }
            }
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}


function contactGenie() {
    $(".genie-btn").html("CONTACT GENIE ( <span class='hidden-sm hidden-md hidden-lg'><a href='tel:" + genieData.phoneNo + "'>0" + genieData.phoneNo + "</a></span><span class='hidden-xs'>" + genieData.phoneNo + "</span> )")
}


function cancelSearchRequest(){
    $.LoadingOverlay("show");
    var params = new FormData();
    params.append("jobId",bookingId);
    params.Auth = udata.data.accessToken;
    params._method = "PUT";
    callApi.queryUrl(apiUrl + "/api/customer/cancelBookingInRequestState", params, function(err, result){
        if (!err) {
            $("#cancelModal").click();
            $.LoadingOverlay("hide", true);
            window.onbeforeunload = '';
        }
    });

}
function getGenie() {
    window.location.href="../index.html";
}
$("#difficultSearch .continue-search").click(function(){
    $("#difficultSearch .spinner").attr("src","../images/spinner.gif");
});
$("#longerSearch .continue-search").click(function(){
    $("#longerSearch .spinner").attr("src","../images/spinner.gif");
});

$("#sameday").click(function(){
    var time = new Date().getHours();
        if (time > 7 && time < 16) {
           window.location.href = "/app/booking.html?service=" + servicetype; 
        } else {
            // $.growl.error({message : "Same day bookings are only available from 8 AM to 2 PM from Sat to Thu."});
            var message = {"msg":"Same day bookings are only available from 8 AM to 2 PM from Sat to Thu."}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$("#messageerror").click();
        }
});

// window.onbeforeunload = function() { return "You work will be lost."; };
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
$(document).ready(function(){
    $("#genieFound").hide();
    $("#difficultSearch").hide();
    $("#longerSearch").hide();
    getJobID();
});