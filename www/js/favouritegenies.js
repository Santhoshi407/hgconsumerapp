/**
 * Created by sumit on 30/10/16.
 */
//GET /api/customer/getMyFavGenie
var udata;
function getFavouriteGenie(){
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "/";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getMyFavGenie", params, function(err, results){
        if (!err) {
            //console.log(results);
            var data = {};
            var genie = results.data;
            if (genie.length > 0) {
                for (var k in results.data) {
                    if (results.data[k]["driverID"]["profilePicURL"]["thumbnail"] === null) {
                        results.data[k]["driverID"]["profilePicURL"]["thumbnail"] = "../images/genieicon.png"
                    }
                    var genieStar = Math.round(results.data[k]["driverID"]["averageRating"]);
                    results.data[k]["avgDriverRating"] = genieStar;
                    var genieStarObj = {};
                    for (var count = 0; count < genieStar; count++) {
                        genieStarObj[count] = "star-filled";
                    }
                    var keys = Object.keys(genieStarObj);
                    if(keys.length < 5) {
                        var genieStarArrayLength = keys.length;
                        var leftStar = 5 - genieStarArrayLength;
                        for (var count = 0; count < leftStar; count++) {
                            genieStarObj[genieStarArrayLength + count] = "star-blank";
                        }
                    }
                    results.data[k]["genieStar"] = genieStarObj;
                    
                }
                data.favGenie = results.data;
                $("#favouritegenie").html(Mustache.render(favGenieTemplate, data));
                $.LoadingOverlay("hide", true);
            } else {
                $("#favouritegenie").html("<p class='text-center' style='font-size:20px;color:#7cf2b1;'>You don't have any favorite Genies.</p>");
                $.LoadingOverlay("hide", true);
            }
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function hideCurrentModal(elem){
    var id = ((((elem.parentNode).parentNode).parentNode).parentNode);
    id = $(id).attr("id");
    $("#" + id).modal("hide");
}

function deleteFavouriteGenie(genieid) {
   // PUT /api/customer/deleteFavouriteGenie
    var params = new FormData();
    params.append("favGenieId", genieid);
    params.Auth = udata.data.accessToken;
    params._method = "PUT";
    $("#deleteFav").click();
    $("#deleteGenie").click(function(){
        $.LoadingOverlay("show");
        callApi.queryUrl(apiUrl + "/api/customer/deleteFavouriteGenie", params, function(err, results){
            if (!err) {
                $.LoadingOverlay("hide", true);
                $("#genieDelete").modal("hide");
                getFavouriteGenie();
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    });
}

$(document).ready(function(){
    getFavouriteGenie();
});

var favGenieTemplate =
    '{{#favGenie}}<div class="col-xs-12 col-sm-12 col-md-12 column add-review-list">\n' +
    '    <div class="col-xs-12 col-sm-6 col-md-6 column address-col genie-col pad0-xs fav-genie-col">\n' +
    '        <div class="genie-name-box deletegeniebox" style="margin-top:10px;height:100px;">\n' +
    '            <div class="col-xs-4 col-sm-2 col-md-2 column genie-img-div delete-genie-div complaint-genie">\n' +
    '                <img src="{{driverID.profilePicURL.thumbnail}}" class="img-responsive genie-img">\n' +
    '                <div class="col-xs-12 hidden-sm hidden-md hidden-lg padding0">\n' +
    '                    <div class="row clearfix text-center">\n' +
    '                        <div class="col-xs-12 column">\n' +
    '                            <p class="genie-name">{{driverID.name}}</p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 column padding0">\n' +
    '                        {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:12px;">\n' +
    '                        <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:12px;">\n' +
    '                        <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:12px;">\n' +
    '                        <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:12px;">\n' +
    '                        <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:12px;">\n' +
    '                        <span style="position:relative;top:-3px;left:5px;">({{avgDriverRating}}/5)</span>{{/genieStar}}\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="hidden-xs col-sm-10 col-md-10 column">\n' +
    '                <p class="genie-name">{{driverID.name}}</p>\n' +
    '                {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:15px;">\n' +
    '                <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:15px;">\n' +
    '                <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:15px;">\n' +
    '                <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:15px;">\n' +
    '                <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:15px;">\n' +
    '                <span style="position:relative;top:-3px;left:5px;">({{avgDriverRating}}/5)</span>{{/genieStar}}\n' +
    '                <br/>\n' +
    '                <div class="col-md-12 col-sm-12 hidden-xs column" style="padding:0px;">\n' +
    '                    <div class="col-md-2 col-sm-2 hidden-xs column" style="padding:0px;width:10%;">\n' +
    '                        <img src="{{categoryID.imageURL.thumbnail}}" class="img-responsive cat-img" style="max-width:25px;">\n' +
    '                    </div>\n' +
    '                    <div class="col-sm-10 col-md-10 column" style="padding:0px;padding-left:5px;">\n' +
    '                        <p class="issue" style="top:0px;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">{{categoryID.name}} ({{subcategoryID.subCategoryName}})</p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <p class="delete-genie-issue"></p>\n' +
    '                <!--</p>-->\n' +
    '            </div>\n' +
    '            <div class="col-xs-8 hidden-sm hidden-md hidden-lg column" style="">\n' +
    '                <div class="col-xs-12 column" style="padding:0px;height:28px;margin-top:10px;">\n' +
    '                    <div class="vertical-align" style="float:right;max-width:85%;">\n' +
    '                        <img src="{{categoryID.imageURL.thumbnail}}" class="img-responsive cat-img fav-cat-img">\n' +
    '                        <p class="" style="margin-left:5px;font-size:16px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;margin-bottom:0px;">{{categoryID.name}} ({{subcategoryID.subCategoryName}})</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-12 column padding0">\n' +
    '                    <span class="span-float delete-btn-right">\n' +
    '                    <button class="delete-btn" onclick="deleteFavouriteGenie(\'{{_id}}\')">DELETE</button>\n' +
    '                    </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="hidden-xs col-sm-6 col-md-6 column address-col delete-geniebtn" style="height:100px;">\n' +
    '        <span class="span-float" style="margin-top:35px;">\n' +
    '        <button class="delete-btn" onclick="deleteFavouriteGenie(\'{{_id}}\')">DELETE</button>\n' +
    '        </span>\n' +
    '    </div>\n' +
    '</div>{{/favGenie}}';