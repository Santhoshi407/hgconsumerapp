/**
 * Created by Sumit on 9/27/2016.
 */
var udata, senturl, service,
    servicesArray = [
    {
        "value": "AC",
        "data": "AC"
    },
    {
        "value": "AC cooling issue",
        "data": "AC | AC cooling issue"
    },
    {
        "value": "AC duct cleaning",
        "data": "AC | AC duct cleaning"
    },
    {
        "value": "AC tripping or not starting",
        "data": "AC | AC tripping or not starting"
    },
    {
        "value": "Digital AC controller  (Installation)",
        "data": "AC | Digital AC controller  (Installation)"
    },
    {
        "value": "AC leakage",
        "data": "AC | AC leakage"
    },
    {
        "value": "AC service",
        "data": "AC | AC service"
    },
    {
        "value": "AC too noisy",
        "data": "AC | AC too noisy"
    },
    {
        "value": "Other or multiple requests (Call back to discuss)",
        "data": "AC | Other or multiple requests (Call back to discuss)"
    },
    {
        "value": "Plumbing",
        "data": "Plumbing"
    },
    {
        "value": "Small fittings (faucet, shower, etc) issue",
        "data": "Plumbing | Small fittings (faucet, shower, etc) issue"
    },
    {
        "value": "Large fixture (WC, bathtub, etc) issue",
        "data": "Plumbing | Large fixture (WC, bathtub, etc) issue"
    },
    {
        "value": "Wet appliances (washing machine, dishwasher,  RO) issue",
        "data": "Plumbing | Wet appliances (washing machine, dishwasher,  RO) issue"
    },
    {
        "value": "Drain or sewer smelling or clogged",
        "data": "Plumbing | Drain or sewer smelling or clogged"
    },
    {
        "value": "Water leakage (or mold)",
        "data": "Plumbing | Water leakage (or mold)"
    },
    {
        "value": "Water tank issue",
        "data": "Plumbing | Water tank issue"
    },
    {
        "value": "No or low  pressure water issue",
        "data": "Plumbing | No or low  pressure water issue"
    },
    {
        "value": "Other or multiple requests  (Call back to discuss)",
        "data": "Plumbing | Other or multiple requests  (Call back to discuss)"
    },
    {
        "value": "Handyman",
        "data": "Handyman"
    },
    {
        "value": "Wall fixtures (paintings, mirror, etc.) mounting",
        "data": "Handyman | Wall fixtures (paintings, mirror, etc.) mounting"
    },
    {
        "value": "Curtains or blinds (installation)",
        "data": "Handyman | Curtains or blinds (installation)"
    },
    {
        "value": "Furniture assembly (e.g.IKEA)",
        "data": "Handyman | Furniture assembly (e.g.IKEA)"
    },
    {
        "value": "Decorative and festive lights (installation)",
        "data": "Handyman | Decorative and festive lights (installation)"
    },
    {
        "value": "Bulbs and lamps replacement",
        "data": "Handyman | Bulbs and lamps replacement"
    },
    {
        "value": "TV mounting (less than 65 inches)",
        "data": "Handyman | TV mounting (less than 65 inches)"
    },
    {
        "value": "TV mounting (65 inch or more)",
        "data": "Handyman | TV mounting (65 inch or more)"
    },
    {
        "value": "Other or multiple requests  (Call back to discuss )",
        "data": "Handyman | Other or multiple requests  (Call back to discuss )"
    },
    {
        "value": "Carpentry",
        "data": "Carpentry"
    },
    {
        "value": "Wooden parquet or deck flooring (repair or installation)",
        "data": "Carpentry | Wooden parquet or deck flooring (repair or installation)"
    },
    {
        "value": "Cabinets or cupboards (repair and installation)",
        "data": "Carpentry | Cabinets or cupboards (repair and installation)"
    },
    {
        "value": "Fly screens (repair or installation)",
        "data": "Carpentry | Fly screens (repair or installation)"
    },
    {
        "value": "Locks and handles  (repair)",
        "data": "Carpentry | Locks and handles  (repair)"
    },
    {
        "value": "Wooden doors or windows (repair)",
        "data": "Carpentry | Wooden doors or windows (repair)"
    },
    {
        "value": "Metal gate, fence or window (repair)",
        "data": "Carpentry | Metal gate, fence or window (repair)"
    },
    {
        "value": "Pergola or Gazebo (repair)",
        "data": "Carpentry | Pergola or Gazebo (repair)"
    },
    {
        "value": "Other or multiple requests  ( Call back to discuss )",
        "data": "Carpentry | Other or multiple requests  ( Call back to discuss )"
    },
    {
        "value": "Masonry",
        "data": "Masonry"
    },
    {
        "value": "False Ceiling (Installation)",
        "data": "Masonry | False Ceiling (Installation)"
    },
    {
        "value": "Flooring (internal) (installation)",
        "data": "Masonry | Flooring (internal) (installation)"
    },
    {
        "value": "Flooring (external) (installation)",
        "data": "Masonry | Flooring (external) (installation)"
    },
    {
        "value": "Wall Tiling (installation)",
        "data": "Masonry | Wall Tiling (installation)"
    },
    {
        "value": "Glass Partitions or enclosures (installation)",
        "data": "Masonry | Glass Partitions or enclosures (installation)"
    },
    {
        "value": "Drop ceiling (Kitchen or bathroom) tiles (repair)",
        "data": "Masonry | Drop ceiling (Kitchen or bathroom) tiles (repair)"
    },
    {
        "value": "Other or multiple requests ( Call back to discuss )",
        "data": "Masonry | Other or multiple requests ( Call back to discuss )"
    },
    {
        "value": "Cleaning",
        "data": "Cleaning"
    },
    {
        "value": "General cleaning (by hours)",
        "data": "Cleaning | General cleaning (by hours)"
    },
    {
        "value": "General cleaning (by size)",
        "data": "Cleaning | General cleaning (by size)"
    },
    {
        "value": "Deep cleaning (by rooms)",
        "data": "Cleaning | Deep cleaning (by rooms)"
    },
    {
        "value": "Window Cleaning",
        "data": "Cleaning | Window Cleaning"
    },
    {
        "value": "Water tank cleaning (underground)",
        "data": "Cleaning | Water tank cleaning (underground)"
    },
    {
        "value": "Villa facade washing",
        "data": "Cleaning | Villa facade washing"
    },
    {
        "value": "Sofa upholstery cleaning",
        "data": "Cleaning | Sofa upholstery cleaning"
    },
    {
        "value": "Carpet cleaning",
        "data": "Cleaning | Carpet cleaning"
    },
    {
        "value": "Curtain cleaning",
        "data": "Cleaning | Curtain cleaning"
    },
    {
        "value": "Other or multiple requests ( Call back to discuss)",
        "data": "Cleaning | Other or multiple requests ( Call back to discuss)"
    },
    {
        "value": "Painting",
        "data": "Painting"
    },
    {
        "value": "Wall painting (decorative)",
        "data": "Painting | Wall painting (decorative)"
    },
    {
        "value": "Interior home painting (basic or move-out, unfurnished)",
        "data": "Painting | Interior home painting (basic or move-out, unfurnished)"
    },
    {
        "value": "Interior home painting (long-life, unfurnished)",
        "data": "Painting | Interior home painting (long-life, unfurnished)"
    },
    {
        "value": "Interior home painting (basic or long-life, furnished)",
        "data": "Painting | Interior home painting (basic or long-life, furnished)"
    },
    {
        "value": "Exterior villa painting",
        "data": "Painting | Exterior villa painting"
    },
    {
        "value": "Room painting (furnished or unfurnished )",
        "data": "Painting | Room painting (furnished or unfurnished )"
    },
    {
        "value": "Door re-painting (or  polishing)",
        "data": "Painting | Door re-painting (or  polishing)"
    },
    {
        "value": "Other or multiple requests ( Call back to  discuss )",
        "data": "Painting | Other or multiple requests ( Call back to  discuss )"
    },
    {
        "value": "Pest Control",
        "data": "Pest Control"
    },
    {
        "value": "General pest control",
        "data": "Pest Control | General pest control"
    },
    {
        "value": "Bed bug fumigation",
        "data": "Pest Control | Bed bug fumigation"
    },
    {
        "value": "Rodent control",
        "data": "Pest Control | Rodent control"
    },
    {
        "value": "Cockroach gel + fumigation",
        "data": "Pest Control | Cockroach gel + fumigation"
    },
    {
        "value": "Termite infestation",
        "data": "Pest Control | Termite infestation"
    },
    {
        "value": "Ants granule treatment",
        "data": "Pest Control | Ants granule treatment"
    },
    {
        "value": "Other or More than one service",
        "data": "Pest Control | Other or More than one service"
    },
    {
        "value": "Appliances",
        "data": "Appliances"
    },
    {
        "value": "Refrigerator or Deep freezer (repair)",
        "data": "Appliances | Refrigerator or Deep freezer (repair)"
    },
    {
        "value": "Dishwasher (repair)",
        "data": "Appliances | Dishwasher (repair)"
    },
    {
        "value": "Cooking oven (repair)",
        "data": "Appliances | Cooking oven (repair)"
    },
    {
        "value": "Microwave",
        "data": "Appliances | Microwave"
    },
    {
        "value": "Washing machine and dryer not working (repair)",
        "data": "Appliances | Washing machine and dryer not working (repair)"
    },
    {
        "value": "Water Purifier (RO) (repair)",
        "data": "Appliances | Water Purifier (RO) (repair)"
    },
    {
        "value": "Extractor hood & exhaust fans (repair)",
        "data": "Appliances | Extractor hood & exhaust fans (repair)"
    },
    {
        "value": "Appliance (installation)",
        "data": "Appliances | Appliance (installation)"
    },
    {
        "value": "Other or multiple requests (Call back to discuss)",
        "data": "Appliances | Other or multiple requests (Call back to discuss)"
    },
    {
        "value": "Gardening",
        "data": "Gardening"
    },
    {
        "value": "Cleaning ",
        "data": "Gardening | Cleaning "
    },
    {
        "value": "Sprinkler system",
        "data": "Gardening | Sprinkler system"
    },
    {
        "value": "Landscaping",
        "data": "Gardening | Landscaping"
    },
    {
        "value": "Artifical grass",
        "data": "Gardening | Artifical grass"
    },
    {
        "value": "Other or More than one service",
        "data": "Gardening | Other or More than one service"
    },
    {
        "value": "Cleaning ",
        "data": "Gardening | Cleaning "
    },
    {
        "value": "Pool",
        "data": "Pool"
    },
    {
        "value": "Water overflow",
        "data": "Pool | Water overflow"
    },
    {
        "value": "Leakage (water level dropping)",
        "data": "Pool | Leakage (water level dropping)"
    },
    {
        "value": "Cleaning",
        "data": "Pool | Cleaning"
    },
    {
        "value": "Pool lights (repair)",
        "data": "Pool | Pool lights (repair)"
    },
    {
        "value": "Pool surface (repair)",
        "data": "Pool | Pool surface (repair)"
    },
    {
        "value": "Water heater or chiller(repair)",
        "data": "Pool | Water heater or chiller(repair)"
    },
    {
        "value": "Water pump (repair)",
        "data": "Pool | Water pump (repair)"
    },
    {
        "value": "Disinfection",
        "data": "Pool | Disinfection"
    },
    {
        "value": "Other or  multiple requests ( Call back to discuss )",
        "data": "Pool | Other or  multiple requests ( Call back to discuss )"
    },
    {
        "value": "Gadgets",
        "data": "Gadgets"
    },
    {
        "value": "CCTV/ Security Systems (installation)",
        "data": "Gadgets | CCTV/ Security Systems (installation)"
    },
    {
        "value": "Laptop or PC (repair)",
        "data": "Gadgets | Laptop or PC (repair)"
    },
    {
        "value": "Smartphone  (repair)",
        "data": "Gadgets | Smartphone  (repair)"
    },
    {
        "value": "Fire alarm systems (repair)",
        "data": "Gadgets | Fire alarm systems (repair)"
    },
    {
        "value": "Garage doors (repair)",
        "data": "Gadgets | Garage doors (repair)"
    },
    {
        "value": "Projector  (installation)",
        "data": "Gadgets | Projector  (installation)"
    },
    {
        "value": "Home theatre or entertainment system (installation)",
        "data": "Gadgets | Home theatre or entertainment system (installation)"
    },
    {
        "value": "Other or multiple requests (Call back to discuss)",
        "data": "Gadgets | Other or multiple requests (Call back to discuss)"
    },
    {
        "value": "Furnishing",
        "data": "Furnishing"
    },
    {
        "value": "Curtains",
        "data": "Furnishing | Curtains"
    }
];

/*function bookServices(data) {
    var select_Template, service_Template, html, service = {};
    service_Template = '<option value="" style="width:220px;">Type</option>{{#service}}<option>{{.}}</option>{{/service}}';
    select_Template = '<option value="">Choose need or issue</option>{{#serviceType}}<option value="{{.}}">{{.}}</option>{{/serviceType}}';
    service.service = Object.keys(data);
    for(var idx in service.service) {
        var serviceTypeArray= [];
        var serviceType = data[service.service[idx]].subcategories;
        for (var k in serviceType) {
            serviceTypeArray.push(serviceType[k]["subCategoryName"]);
            if (serviceType[k]["unitCharges"]["firstUnitCharges"]) {
                fixedPriceServiceArray.push(serviceType[k]["subCategoryName"])
            }
        }
        data[service.service[idx]].serviceTypeArray = serviceTypeArray;
    }
    $(".service-select").html(Mustache.render(service_Template, service));

    $(".service-select").on("change", function(){
        service.serviceType = data[$(".service-select option:selected").val()]["serviceTypeArray"];
        html = Mustache.render(select_Template, service);
        $(".servicetype-select").html(html);
    });
    $(window).on("load", function(){
        $(".servicetype-select").html('<option value="">Choose need or issue</option>');
    });
}*/

$(".servicetype-select").click(function(){
    if (!$(".service-select option:selected").val()) {
        showerror(msg.MESSAGE_SERVICE_ERROR);
        return;
    }
});

function showerror(errMsg) {
    var timeout = "";
    $(".error-message").html(errMsg);
    $(".error-message").parent().show();
    timeout = 10000;
    setTimeout(function(){
        $(".error-message").parent().hide();
    },timeout);
}
$(".service-select").click(function(){
    //console.log(udata);
    if (!udata) {
        showerror(msg.ALERT_LOGIN);
    }
});

/*$(".continue").click(function(){
    window.location.href = decodeURI(senturl);
});*/
function chooseCategory(val){
    var name = $(val).attr("name");
    senturl = "html/booking.html?service=" + name;
    // alert(senturl);
    window.location.href = decodeURI(senturl);

}
function callOn(){
    window.open('tel:+97104-448-9595','_system')
}
$(document).ready(function(){
    console.log(document.URL);
    var userdata = localStorage.getItem("h_user");
    if (userdata) {
        getCategories();
    }
    $('#input-book').devbridgeAutocomplete({
        lookup: servicesArray,
        minChars: 1,
        onSelect: function (suggestion) {
            service =  suggestion.data;
            if (service) {
                service = service.split(" | ")[0];
            }
        },
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Sorry, no matching results'
    });
    $("#booknow").click(function(){
        var userdata = localStorage.getItem("h_user");
        console.log(service);
        // return;
        if (userdata && userdata !== "null") {
            if(service) {
                senturl = "html/booking.html?service=" + service;
                window.location.href = decodeURI(senturl);
            }else {
                /*$("#displayerror").html("Please select a service to book an appointment.");
                $("#messageerror").click();*/
                var message = {"msg":"Please select a service to book an appointment."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
            }
        } else {
            /*$("#displayerror").html("Please SIGNIN to book an appointment.");
            $("#messageerror").click();*/
            var message = {"msg":"Please SIGNIN to book an appointment."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
        }
    });
    // $("#emergency, #sameday").click(function(){
    //     var userdata = localStorage.getItem("h_user");
    //     var servicetype = $(this).attr("id");
    //     if (userdata && userdata !== "null") {
    //         window.location.href = "html/booking.html?service=" + servicetype;
    //     } else {
    //         var message = {"msg":"Please SIGNIN to book an appointment."}
    //         $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
    //         $('.msg-modal').modal('show');
    //     }
    // });
    $("#emergency").click(function(){
        var servicetype = $(this).attr("id");
        var userdata = localStorage.getItem("h_user");
        if (userdata && userdata !== "null") {
            window.location.href = "html/booking.html?service=" + servicetype;
        } else {
            var message = {"msg":"Please SIGNIN to book an appointment."}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        }
    });
    $("#sameday").click(function(){
        var servicetype = $(this).attr("id");
        var userdata = localStorage.getItem("h_user");
        if (userdata && userdata !== "null") {
            var time = new Date().getHours();
            if (time > 7 && time < 16) {
               window.location.href = "html/booking.html?service=" + servicetype; 
            } else {
                var message = {"msg":"Same day bookings are only available from 8 AM to 2 PM from Sat to Thu."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
            }
            
        } else {
            var message = {"msg":"Please SIGNIN to book an appointment."}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        }
    });
});
    

/**
 * Created by Sumit on 9/27/2016.
 */



function bookServices(data) {
    var service = {};
    service.service = Object.keys(data);
    /*for (var idx in service.service) {
        var serviceCategory = {};
        serviceCategory.value = service.service[idx];
        serviceCategory.data = service.service[idx];console.log(serviceCategory.data);
        servicesArray.push(serviceCategory);
        for(var k in data[service.service[idx]]["subcategories"]){
            serviceCategory = {};
            serviceCategory.value = data[service.service[idx]]["subcategories"][k]["subCategoryName"];
            serviceCategory.data = service.service[idx] + " | " + data[service.service[idx]]["subcategories"][k]["subCategoryName"];
            servicesArray.push(serviceCategory);
        }
    }*/
}


//scroll to top when keyboard is open
window.addEventListener('native.keyboardshow', keyboardShowHandler);

function keyboardShowHandler(e){
    console.log('Keyboard height is: ' + e.keyboardHeight);
    console.log($("#input-book").offset().top);
    /*window.location.href = '#input-book';*/
    // var top = $("#input-book").offset().top
    // window.scrollBy(0, -top); 
}

window.addEventListener('native.keyboardhide', keyboardHideHandler);

function keyboardHideHandler(e){
    window.scrollTo(0, 0);
}