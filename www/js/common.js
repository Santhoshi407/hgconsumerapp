
var callApi = {}, countryInfo, info, udata, msg = {},terms, usercontact, phoneVerified = false, userEmail, userData;
    apiUrl = "http://api.homegenie.me:8007";
var validateEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|in|co|aero|jobs|museum|me|ae)\b/;

// userData = {"statusCode":201,"message":"Created Successfully","data":{"accessToken":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU4NDkxNjJhMmZhZDhiOWIwYTEzNTlmNiIsInR5cGUiOiJDVVNUT01FUiIsImlhdCI6MTQ4MTE4NDgxMH0.CgF9krl_nHEeUnseH6f48tmaJ6hKGFrHvDzI419A7mQ","userDetails":{"email":"homegtest@gmail.com","phoneNo":"555822309","countryCode":"+971","timezone":"Asia/Calcutta","newNumber":"+971-555822309","_id":"5849162a2fad8b9b0a1359f6","adminRegister":false,"promoCode":[],"isDelete":false,"uniqueCode":"IN472","otherOption":{"otherOptionDetails":null},"dob":null,"nationality":null,"AppointmentsId":[],"ratingPersonNo":0,"timestamp":"2016-12-08T08:13:30.027Z","rating":0,"profilePicURL":{"thumbnail":null,"original":null},"currentLocation":[17.4833,78.4167],"phoneVerified":false,"emailVerified":false,"language":"English","firstTimeLogin":false,"name":"Sumit","customerAddresses":[]},"pushData":[],"Unpaid_Appointments":[],"defaultAddress":[],"defaultCards":null}};
userData = {};
msg.MESSAGE_SERVICE_ERROR = "Please choose service category first";
msg.MESSAGE_SERVICE_TYPE_ERROR = "Please choose need or issue";
msg.MESSAGE_LOGIN_ERROR = "You have entered invalid credentials";
msg.MESSAGE_REGISTRATION = "Hi ";
msg.MESSAGE_LOGIN = "Hi again, ";
msg.ALERT_LOGIN = "Please login to book a service";
msg.MESSAGE_EMAIL_ERROR = "Please enter correct e-mail";
msg.MESSAGE_CONTACT_ERROR = "Please enter a valid contact number";
msg.MESSAGE_PASSWORD_RESET = "We have sent you a mail to reset your password. Please check your e-mail";
msg.MESSAGE_BAD_TOKEN = "It seems someone else has logged in from your account. If it's not you, please contact to us ";
msg.MESSAGE_PROFILE_UPDATE = "Your profile has been updated.";

callApi.getUrl = function (url, params, resultCallback) {
    //console.log(url, params, JSON.stringify(resultCallback));
    $.ajax({
        type: params._method || 'GET',
        url: url,
        contentType: 'application/json',
        beforeSend : function( xhr ) {
            if (params && params.Auth) {
                xhr.setRequestHeader( "Authorization", "Bearer " + params.Auth );
            }
        },
        success: function (data) {
            resultCallback(null, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var err = jqXHR.responseText;
            resultCallback(err);
        }
    });
};

callApi.queryUrl = function (url, body, resultCallback) {
    body = body || {};
    body._method = body._method || 'GET';
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": body._method,
        "processData": false,
        "contentType": body.content || false,
        "mimeType": "multipart/form-data",
        "data": body,
        beforeSend : function( xhr ) {
            if (body && body.Auth) {
                xhr.setRequestHeader( "authorization", "Bearer " + body.Auth);
            }
            if (body && body.Pay) {
                xhr.setRequestHeader("Access-Control-Allow-Origin", "*"); //http://localhost:63342
                xhr.setRequestHeader("Access-Control-Request-Method", "POST, GET");
            }
            if (body && body.CORS) {
                xhr.setRequestHeader("" + body.CORS);
            }
        },
        success: function (data) {
            resultCallback(null, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var err = jqXHR.responseText;
            resultCallback(err);
        }
    });
};
callApi.putUrl = function (url, body, resultCallback) {
    body = body || {};
    body._method = body._method || 'GET';
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": body._method,
        "processData": false,
        "contentType": body.content || false,
        "mimeType": "application/JSON",
        "data": body,
        beforeSend : function( xhr ) {
            if (body && body.Auth) {
                xhr.setRequestHeader( "authorization", "Bearer " + body.Auth);
            }
        },

        success: function (data) {
            resultCallback(null, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var err = jqXHR.responseText;
            resultCallback(err);
        }
    });
};

callApi.registerCustomer = function(){
    var params = new FormData();
    var email = $("#reg-email").val();
    var phoneNo = $("#reg-phnNo").val();
    var pwd = $("#reg-pwd").val();
    if (validateEmail.test(email)){
        params.append("email",email);
    } else {
            var message = {"msg":msg.MESSAGE_EMAIL_ERROR}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message : msg.MESSAGE_EMAIL_ERROR});
        return;
    }
    if (parseInt(phoneNo) == phoneNo){
        params.append("phoneNo", phoneNo);
    } else {
            var message = {"msg":msg.MESSAGE_CONTACT_ERROR}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message : msg.MESSAGE_CONTACT_ERROR});
        return;
    }
    params.append("name",$("#reg-name").val());
    params.append("countryCode", "+971");

    if (!terms) {
         var message = {"msg":"Please select the terms of use"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message : "Please select the terms of use"});
        return;
    }
    if (countryInfo) {
        params.append("country", countryInfo.country);
        params.append("latitude", countryInfo.loc.split(",")[0]);
        params.append("longitude", countryInfo.loc.split(",")[1]);
    }

    //params.facebookId = "";
    params.append("password",pwd);
    params.append("language", "English");
    params.append("deviceType", "ANDROID");
    params.append("deviceToken", "15");
    params.append("appVersion", "100");
    params.append("timezone", "Asia/Calcutta");
    params._method = "POST";
    $.LoadingOverlay("show");
    //params.profilePic = "";
    callApi.queryUrl(apiUrl + "/api/customer/register", params, function(err, results){
        if (!err) {
            var data = {}, userdata;
            $.LoadingOverlay("hide", true);
            results = JSON.parse(results);
            userData = results;
            $("#registration-success").modal('show');
            $("#sign-up").modal('hide');
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
};

function verifyRegistration() {
    var results, userdata, data = {};
    results = userData;
    userdata = results.data.userDetails;
    data.Auth = results.data.accessToken;
    data._method = "PUT";
    callApi.queryUrl(apiUrl + "/api/customer/resendOTP",data, function(err, results){
        if (!err) {
             var message = {"msg":"Please enter OTP to verify your account"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.notice({message : "Please enter OTP to verify your account"});
        }
    });
    $("#mobileotp").html(userdata.phoneNo);
    /*$("#login-box").modal('hide');
    $("#verifyloginotp").click();*/
}

callApi.loginCustomer = function(){
    var params = new FormData();
    var email = $("#log-email").val();
    //var email = 'homegenie2@gmail.com';
    if (validateEmail.test(email)){
        params.append("email",email.trim());
    } else {
         var message = {"msg":msg.MESSAGE_EMAIL_ERROR}
         console.log(msg.MESSAGE_EMAIL_ERROR);
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message : msg.MESSAGE_EMAIL_ERROR});
        return;
    }
    countryInfo = true;
    if (countryInfo) {
        params.append("latitude", 17.2345);
        params.append("longitude", 78.2345);
    }
    params.append("password",$("#log-pwd").val());
    params.append("deviceType", "ANDROID");
    params.append("appVersion", "100");
    params.append("deviceToken", '15');
    params.append("timezone", "Asia/Calcutta");
    params.append("flushPreviousSessions", true);
    params._method = "POST";
    $.LoadingOverlay("show");
    /*if(localStorage.deviceToken){
       params.append("deviceToken", (localStorage.deviceToken));*/
        callApi.queryUrl(apiUrl + "/api/customer/login", params, function(err, results){
        if (!err && results) {
            $.LoadingOverlay("hide", true);
            var data ={}, userdata;
            results = JSON.parse(results);
            userData = results;
             
            console.log(userData);
            userdata = results.data.userDetails;
            console.log(userdata);
            console.log(userdata.name);
            
            if (results.data.userDetails.phoneVerified || results.data.userDetails.emailVerified) {
                $.LoadingOverlay("hide",true);
                // $.growl.notice({message:"Hi again, " + results.data.userDetails.name});
                udata = results;
                var cdata = JSON.stringify(results);
                $("#login-box").modal('hide');
                //createCookie("h_user",cdata,365);
                localStorage.setItem('h_user', cdata);
                updateUserInfo(results);
                getCategories();
            } else {
                data.Auth = results.data.accessToken;
                data._method = "PUT";
                callApi.queryUrl(apiUrl + "/api/customer/resendOTP",data, function(err, results){
                    if (!err) {
                        var message = {"msg":"Please enter OTP to verify your account"}
                        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                        $('.msg-modal').modal('show');
                       // $.growl.notice({message : "Please enter OTP to verify your account"});
                    }
                });
                $("#mobileotp").html(userdata.phoneNo);
                $("#login-box").modal('hide');
                $("#verifyloginotp").click();
            }
        } else {
            $.LoadingOverlay("hide", true);
            var err = {"message":msg.MESSAGE_LOGIN_ERROR};
            $("#err-display-signup").html(Mustache.render(errorMsgDisplayTemp, err));
            console.log(Mustache.to_html(errorMsgDisplayTemp, err));
            $('#err-modal').modal('show');
            //$.growl.error({message : msg.MESSAGE_LOGIN_ERROR});
            //callApi.error(err);
        }
    });
  /*}*/
    
};
function verifyLoginOTP() {
    console.log('entered into verified otp');
    var data, userdata;
    data = new FormData();
    data.Auth = userData.data.accessToken;
    data._method = "PUT";
    userdata = userData.data.userDetails;
    data.append("countryCode", "+971");
    data.append("phoneNo", userdata.phoneNo);
    data.append("OTPCode", $("#otplogin").val());
    data.append("timezone", userdata.timezone);
    data.append("latitude", userdata.currentLocation[0]);
    data.append("longitude", userdata.currentLocation[1]);
    callApi.queryUrl(apiUrl + "/api/customer/verifyOTP", data, function(err, results){
        if (!err) {
            var results = userData;
            $("#verify-otplogin").modal('hide');
            $.LoadingOverlay("hide",true);
             var message = {"msg":"Hi, " + results.data.userDetails.name}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.notice({message:"Hi, " + results.data.userDetails.name});
            udata = results;
            var cdata = JSON.stringify(results);
            $("#login-box").modal('hide');
            //createCookie("h_user",cdata,365);
            localStorage.setItem('h_user', cdata);
            setTimeout(function(){
                $("#verified-modal").click();
            },500);
            updateUserInfo(results);
            getCategories();
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function resendLoginOTP() {
    console.log("into resend otp");
    var data = {};
    data.Auth = userData.data.accessToken;
    data._method = "PUT";
    callApi.queryUrl(apiUrl + "/api/customer/resendOTP",data, function(err, results){
        if (!err) {
             var message = {"msg":"OTP Re-sent"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.notice({message : "OTP Re-sent"});
        }
    });
}

callApi.forgotPassword = function(){
    var regenerate;
    var mob = false;
    var params = new FormData();
    var email = $("#pwd-email").val();
    phoneVerified = true;
    email = email.trim();
    if (usercontact) {
        params.append("email",usercontact);
        regenerate = true;
    } else if (email != parseInt(email)) {
        if (validateEmail.test(email)){
            params.append("email",$("#pwd-email").val());
            //params.email = email;
        } else {
             var message = {"msg":msg.MESSAGE_EMAIL_ERROR}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.error({message : msg.MESSAGE_EMAIL_ERROR});
            return;
        }
    } else if (email == parseInt(email)) {
        if (email.length == 9) {
            $("#mobile").html(email);
            usercontact = email;
            mob = true;
            params.append("email",$("#pwd-email").val());
        } else {
            var message = {"msg":"Please enter valid contact number"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.error({message : "Please enter valid contact number"});
            return;
        }
    } else {
            var message = {"msg":"Please enter valid contact number"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.error({message : "Please enter valid contact number"});
        return;
    }

    params._method = "PUT";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/getResetPasswordToken", params, function(err, results){
        if (!err) {
            if (regenerate) {
                $.LoadingOverlay("hide", true);
                var message = {"msg":"OTP Re-sent"}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                //$.growl.notice({message : "OTP Re-sent"});
                return;
            }
            if (mob) {
                $.LoadingOverlay("hide", true);
                var message = {"msg":"You will receive One Time Password via SMS at +971" + email}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                //$.growl.notice({message : "You will receive One Time Password via SMS at +971" + email});
                $("#change-pwd").modal('hide');
                setTimeout(function(){
                    $("#verifyotp").click();
                },500);
            } else {
                $.LoadingOverlay("hide", true);
                regenerate = false;
                usercontact = null;
                var message = {"msg":msg.MESSAGE_PASSWORD_RESET}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                //$.growl.notice({message : msg.MESSAGE_PASSWORD_RESET});
                $("#change-pwd").modal('hide');
                $('.msg-continue').click(function(){
                    $("#login-box").modal('show');
                })
            }

        } else{
            $.LoadingOverlay("hide", true);
            callApi.error(err);
            /*var message = {"msg":err.message}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');*/
        }
    });
};

callApi.verifyOTP = function(){
    var data = new FormData();
    var otp = $("#otp").val();
    if (!otp) {
            var message = {"msg": "Please enter the OTP"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({"msg" : "Please enter the OTP"});
        return;
    }
    data.append("phoneNo", usercontact);
    data.append("OTPCode", otp);
    data._method = "PUT";
    $.LoadingOverlay("show");
    if (!phoneVerified) {
        var data = new FormData;
        data.append("email", userEmail);
        data._method = "PUT";

    } else {
        callApi.queryUrl(apiUrl + "/api/customer/ResetPasswordverifyOTP", data, function(err, results){
            if (!err) {
                $("#verify-otp").modal('hide');
                $.LoadingOverlay("hide",true);
                setTimeout(function(){
                    $("#setpwd").click();
                },500);
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    }
};

callApi.setNewPassword = function(){
    var data = new FormData();
    var newpwd = $("#new-pwd").val();
    if (!newpwd) {
         var message = {"msg":"Please enter your password"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({"msg" : "Please enter your password"});
        return;
    }
    data.append("phoneNo", usercontact);
    data.append("newPassword", newpwd);
    data._method = "PUT";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/OTPResetPassword", data, function(err, results){
        if (!err) {
            $("#set-pwd").modal('hide');
            $.LoadingOverlay("hide", true);
             var message = {"msg":"Your password has been set"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            //$.growl.notice({message : "Your password has been set"});
            setTimeout(function(){
                $(".login").click();
            },500);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
};

callApi.resetPassword = function(){
    var params = {}, email, token;
    token = (document.URL).split("token=")[1].trim();
    //console.log(token);
    email = $("#reset-email").val();
    params._method = "GET";
    if (validateEmail.test(email)) {
        params.email = email;
    } else {
         var message = {"msg":msg.MESSAGE_EMAIL_ERROR}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message : msg.MESSAGE_EMAIL_ERROR});
        return;
    }
    params.passwordResetToken = token;
    params.newPassword = $("#new-pwd").val();
    params.url = apiUrl + "/api/customer/resetPassword?email=" + params.email + "&passwordResetToken=" + params.passwordResetToken + "&newPassword=" + params.newPassword;
    callApi.getUrl(params.url, function(err, response){
        if (!err) {
            createCookie("reset","pwd",1);
            if((document.URL).split('www/')[1] == 'index.html'){
                window.location.href = "index.html";
            } else{
                window.location.href = "../index.html";
            }
        }
    });
};

callApi.logout = function(){
    var params = new FormData();
    //console.log(udata);
    params.Auth = udata.data.accessToken;
    params._method = "OPTIONS";
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/logout", params, function(err, response){
        console.log(err, response);
        if (!err) {
            console.log('into !err');
            localStorage.setItem("h_user", null);
            localStorage.setItem('notification','false')
            /*$("#nav-login").show();
            $("#user-menu").addClass("hidden");
            $(".login").click();*/
            createCookie("reset","pwd",1);
            if((document.URL).split('www/')[1] == 'index.html'){
                window.location.href = "index.html";
            } else{
                window.location.href = "../index.html";
            }
            
        }
    });
};
// $.get("http://ipinfo.io", function(response) {
//     countryInfo = response;
// }, "jsonp");

callApi.error = function(err) {
    var err = JSON.parse(err);
    if (err.statusCode === 401) {
        console.log('401');
            var message = {"msg":msg.MESSAGE_BAD_TOKEN}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
        //$.growl.error({message:msg.MESSAGE_BAD_TOKEN});
        localStorage.setItem("h_user",null);
        setTimeout(function(){
            if((document.URL).split('www/')[1] == 'index.html'){
                window.location.href = "index.html";
            } else{
                window.location.href = "../index.html";
            }
        },300);
    }else {
        console.log(err);
        //$('#err-modal').modal('show');
        $("#err-display-signup").html(Mustache.render(errorMsgDisplayTemp, err));
        console.log(Mustache.to_html(errorMsgDisplayTemp, err));
        $('#err-modal').modal('show');
    }
    /*$('.err-modal-continue').click(function(){
    console.log('into error modal');
    $('#err-modal').modal('hide');
});*/
};

function getUserInfo(){
    var userdata = localStorage.getItem("h_user");
    var pwdreset = readCookie("reset");
    if (userdata && userdata !== 'null') {
        udata = JSON.parse(userdata);
        updateUserInfo(udata);
        return;
    }
    setTimeout(function(){
        $(".login").click();
    },300);
    /*if (pwdreset && pwdreset !== "null"){
        createCookie("reset", "null",1);
        $(".login").click();
    }*/
}
function updateUserInfo(results) {
    info = results;
    console.log(info.data);
    $("#sidenav").html(Mustache.render(sidenav, info.data));
    if(info.data.userDetails.profilePicURL.thumbnail){
        $('#userImage').attr('src',info.data.userDetails.profilePicURL.thumbnail);
    } else{
        $('#userImage').attr('src','images/genieicon.png');
    }
    $('#userName').text(info.data.userDetails.name);
    
    if (info.data.userDetails.profilePicURL && info.data.userDetails.profilePicURL.thumbnail) {
        $(".user-img").attr("src",info.data.userDetails.profilePicURL.thumbnail);
    } else {
        $(".user-img").attr("src","images/genieicon.png");
    }
    $(".user-name").text(info.data.userDetails.name);
    $("#nav-login").hide();
    $("#user-menu").removeClass("hidden");
}

function getCategories() {
    var output = $("#output");    
    var template = $("#test1").html();
    var userInfo = JSON.parse(localStorage.getItem("h_user"));
    udata = userInfo;
    if (userInfo && userInfo !== "null") {
        $.LoadingOverlay("show");
        var params = {};
        params.Auth = udata.data.accessToken;
        callApi.getUrl(apiUrl + "/api/category/getAllCategories", params, function(err, results) {
            if (!err && results) {
                $.LoadingOverlay("hide",true);
                var category = {};
                var cat = results.data;
                // console.log(cat);
                $("#category").html(Mustache.to_html(categoryTemplate, results));
                for (var k in cat) {
                    category[cat[k].name] = cat[k];
                    // console.log(category[cat[k].name].imageURL.original);
                }
                bookServices(category);
            } else {
                $.LoadingOverlay("hide",true);
                callApi.error(err);
            }
        });
    }
}
function makePayment(getIdAmount) {
    console.log("inot pay");
    var id = $(getIdAmount).attr('id');
    var amount = $(getIdAmount).attr('amount');
    console.log(id,amount);
    var url = document.URL.split('/');
    var paymentdata = {};
    paymentdata.id = id;
    paymentdata.a = amount;
    paymentdata = JSON.stringify(paymentdata);
    createCookie("hg_p",paymentdata,1);
    if(url.length == 6){
        window.location.href="html/paymentmethod.html";
    }else{
        window.location.href="../html/paymentmethod.html";
    }
}
function acceptJob(jobid) {
    console.log('into accept or reject job')
    console.log(jobid);
    var status;
    var id = $(jobid).attr('id');
    var checkstatus = $(jobid).attr('status');
    if(checkstatus == 'NOW'){
        status = 'APPROVE';
    }
    console.log(id,status);
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("jobId",id);
    data.append("status",status);
    if (status == "APPROVE") {
        $.LoadingOverlay("show");
        callApi.putUrl(apiUrl + "/api/customer/acceptOrRejectedJobOnce/", data, function(err, results){
            if (!err){
                // $.growl.notice({message : "Your job has been updated successfully."});
                var message = {"msg":"Your job has been updated successfully."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                $("#popupmodal").modal("hide");
                $('#view-estimate-later-modal').modal('hide');
                $('#view-estimate-modal').modal('hide');
                $("body").removeClass("modalOpen");
                $.LoadingOverlay("hide", true);
                //getmybookings();
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    } 
}
// $('.msg-modal-continue').click(function(){
//     console.log('in');
//     $('.msg-modal').modal('hide');
// });
function notificationManager(notificationData){
    var data = JSON.parse(notificationData.message);
    if(data.status == 'ASSIGNED'){
        console.log('assigned'); 
        var url = (document.URL).split("/");
        var  imgsrc = "";
        var starsrc1 = '';
        var starsrc2 = '';
        var starsrc3 = '';
        var starsrc4 = '';
        var starsrc5 = '';
        if(data.thumbnail){
            imgsrc = data.thumbnail;
        }else{
            if(url.length == 6){
                imgsrc = 'images/genieicon.png'
            }
            else{
                imgsrc = '../images/genieicon.png';
            }
        }
        if(Math.round(data.rating) == 1){ 
            if(url.length == 6){
                starsrc1 = 'images/yellow-star.png';
                starsrc2 = 'images/grey-star.png';
                starsrc3 = 'images/grey-star.png';
                starsrc4 = 'images/grey-star.png';
                starsrc5 = 'images/grey-star.png';
            } else{
                starsrc1 = '../images/yellow-star.png';
                starsrc2 = '../images/grey-star.png';
                starsrc3 = '../images/grey-star.png';
                starsrc4 = '../images/grey-star.png';
                starsrc5 = '../images/grey-star.png';
            }
            
        }else if(Math.round(data.rating) == 2){
            if(url.length == 6){
                starsrc1 = 'images/yellow-star.png';
                starsrc2 = 'images/yellow-star.png';
                starsrc3 = 'images/grey-star.png';
                starsrc4 = 'images/grey-star.png';
                starsrc5 = 'images/grey-star.png';
            } else{
                starsrc1 = '../images/yellow-star.png';
                starsrc2 = '../images/yellow-star.png';
                starsrc3 = '../images/grey-star.png';
                starsrc4 = '../images/grey-star.png';
                starsrc5 = '../images/grey-star.png';
            }
            
        }else if(Math.round(data.rating) == 3){
            if(url.length == 6){
                starsrc1 = 'images/yellow-star.png';
                starsrc2 = 'images/yellow-star.png';
                starsrc3 = 'images/yellow-star.png';
                starsrc4 = 'images/grey-star.png';
                starsrc5 = 'images/grey-star.png';
            } else{
                starsrc1 = '../images/yellow-star.png';
                starsrc2 = '../images/yellow-star.png';
                starsrc3 = '../images/yellow-star.png';
                starsrc4 = '../images/grey-star.png';
                starsrc5 = '../images/grey-star.png';
            }
           
        }else if(Math.round(data.rating) == 4){
            if(url.length == 6){
                starsrc1 = 'images/yellow-star.png';
                starsrc2 = 'images/yellow-star.png';
                starsrc3 = 'images/yellow-star.png';
                starsrc4 = 'images/yellow-star.png';
                starsrc5 = 'images/grey-star.png';
            } else{
                starsrc1 = '../images/yellow-star.png';
                starsrc2 = '../images/yellow-star.png';
                starsrc3 = '../images/yellow-star.png';
                starsrc4 = '../images/yellow-star.png';
                starsrc5 = '../images/grey-star.png';
            }
            
        }else{
            if(url.length == 6){
                starsrc1 = 'images/yellow-star.png';
                starsrc2 = 'images/yellow-star.png';
                starsrc3 = 'images/yellow-star.png';
                starsrc4 = 'images/yellow-star.png';
                starsrc5 = 'images/yellow-star.png';
            } else{
                starsrc1 = '../images/yellow-star.png';
                starsrc2 = '../images/yellow-star.png';
                starsrc3 = '../images/yellow-star.png';
                starsrc4 = '../images/yellow-star.png';
                starsrc5 = '../images/yellow-star.png';
            }
            
        }
        data.imgsrc = imgsrc;
        data.starsrc1 = starsrc1;
        data.starsrc2 = starsrc2;
        data.starsrc3 = starsrc3;
        data.starsrc4 = starsrc4;
        data.starsrc5 = starsrc5;
        $('#show-assign-genie').html(Mustache.render(assign_genie_template, data));
        $('#assign-genie-modal').modal('show');
      } 
      else if(data.status == 'NOW'){
        localStorage.setItem('notification','true');
        console.log('into estimate');
        var url = (document.URL).split("/");
        var newhref= "";
        var  imgsrc= "";
        console.log(url);
        if(url.length == 6){
            newhref = 'html/mybookings.html';
            imgsrc = 'images/estimate-not.png'
        }else if((document.URL).split('www/')[1] == 'html/mybookings.html'){
            newhref = 'mybookings.html';
            imgsrc = '../images/estimate-not.png'
        }else{
            newhref = '../html/mybookings.html';
            imgsrc = '../images/estimate-not.png'
        }
        data.newhref = newhref;
        data.imgsrc = imgsrc;
        $('#view-estimate').html(Mustache.render(view_estimate_template, data));
        $('#view-estimate-modal').modal('show');
        localStorage.setItem('id',data.appointmentID);
      }
      else if(data.status == 'RESCHEDULED'){
        localStorage.setItem('notification','true');
        var url = (document.URL).split("/");
        var newhref= "";
        var  imgsrc= "";
        data.showScheduleDate = '';
        console.log(url);
        if(url.length == 6){
            newhref = 'html/mybookings.html';
            imgsrc = 'images/estimate-not.png'
        }else if((document.URL).split('www/')[1] == 'html/mybookings.html'){
            newhref = 'mybookings.html';
            imgsrc = '../images/estimate-not.png'
        }else{
            newhref = '../html/mybookings.html';
            imgsrc = '../images/estimate-not.png'
        }
        data.showScheduleDate = data.message.split('on')[1];
        data.newhref = newhref;
        data.imgsrc = imgsrc;
        $('#view-estimate-later').html(Mustache.render(view_estimate_later_template , data));
        $('#view-estimate-later-modal').modal('show');
        localStorage.setItem('id',data.appointmentID);
      }
      else if(data.status == 'COMPLETED'){
        var url = (document.URL).split("/");
        var bookingshref = '';
        if(url.length == 6){
            bookingshref = 'html/mybookings.html';
        }else if((document.URL).split('www/')[1] == 'html/mybookings.html'){
           bookingshref = 'mybookings.html';
        }else{
            bookingshref = '../html/mybookings.html';
        }
        data.bookingshref = bookingshref;
        $('#job-completed').html(Mustache.render(job_completed_template, data));
        $('#job-completed-modal').modal('show');
        localStorage.setItem('id',data.appointmentID);
      }
      else if(data.status == 'NOT COMPLETED'){
        $('#complaints').html(Mustache.render(complaints_template, data));
        $('#complaints-modal').modal('show');
      }
}
function rejectEstimate(id){
    var data = {};
    var id=$(id).attr('id');
    data.status = 'REJECTED';
    data.id = id; 
    $('#view-estimate-modal').modal('hide');
    $('#view-estimate-later-modal').modal('hide');
    console.log('into reject estimate')
    $('#estimate-rejected').html(Mustache.render(estimate_rejected_template, data));
    $('#estimate-rejected-modal').modal('show');
}
function reestimateOrClose(statusId){
    var id = $(statusId).attr('id');
    var status = $(statusId).attr('status');
    console.log(id,status);
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("jobId",id);
    data.append("status",status);
    callApi.putUrl(apiUrl + "/api/customer/acceptOrRejectedJobOnce/", data, function(err, results){
        if (!err){
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
    $('#estimate-rejected-modal').modal('hide');
}
function rejectedJob(checkstatus){
    var status = $(checkstatus).attr('status');
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("status", status);
    callApi.putUrl(apiUrl + "/api/customer/acceptOrRejectedJobOnce/", data, function(err, results){
        if (!err){
            $.growl.notice({message : "Your job has been updated successfully."});
            $("#popupmodal").modal("hide");
            $("body").removeClass("modalOpen");
            $.LoadingOverlay("hide", true);
            //getmybookings();
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}
function assignGenie(getid){
    var id = $(getid).attr('id');
    $('#assign-genie-modal').modal('hide');
    localStorage.setItem('notification','true');
    var url = (document.URL).split("/");
    if(url.length == 6){
        window.location.href = 'html/mybookings.html';
    }else if((document.URL).split('www/')[1] == 'html/mybookings.html'){
        window.location.href = 'mybookings.html';
    }else{
        window.location.href = '../html/mybookings.html';
    }
    localStorage.setItem('id',id);
}
function complaint(id){
    var complaintData = $('#registerComplaint').val();
    var id = $(id).attr('id');
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("jobId", id);
    data.append("complaints", complaintData);
    callApi.putUrl(apiUrl + "/api/customer/registerComplaints/", data, function(err, results){
        if (!err){
            $.LoadingOverlay("hide", true);
            $('#complaints-modal').modal('hide');
            console.log(results);
            //getmybookings();
        } else {
            $.LoadingOverlay("hide", true);
            $('#complaints-modal').modal('hide');
            callApi.error(err);
        }
    });
}
function showAppVer(appVersion){
    var version = {};
    console.log(appVersion);
    version.version = appVersion;
    console.log(version);
    $("#show-version").text(version);
    $("#sidenav").html(Mustache.render(sidenav,version));
}
$(document).ready(function(){
    getUserInfo();
    var pageHeading = {};
    
    console.log(document.URL);
    /*if((document.URL).split('www/')[1] != 'index.html'){
        var url = (document.URL.split('html/')[1]).split('.')[0];
        if(url == 'booking'){
            pageHeading.heading = "DETAILS";
            $('.dash-o-div').addClass('dash-det-di');
        } else if(url == 'promotions'){
            pageHeading.heading = 'PROMOTIONS';
        } else if(url == 'mysettings'){
            pageHeading.heading = 'MY SETTINGS';
        } else if(url == 'mybookings'){
            pageHeading.heading = 'MY BOOKINGS';
        } else if(url == 'support'){
            pageHeading.heading = 'SUPPORT';
        } else if(url == 'addaddress'){
            pageHeading.heading = 'ADD ADDRESS';
        } else if(url == 'basicinfo'){
            pageHeading.heading = 'BASIC INFO';
        } else if(url == 'bookNow'){
            pageHeading.heading = 'BOOK NOW';
        } else if(url == 'difficultysearching'){
            pageHeading.heading = 'DIFFIULTY SEARCHING';
        } else if(url == 'earnGenie'){
            pageHeading.heading = 'EARN GENIE';
        } else if(url == 'favouritegenies'){
            pageHeading.heading = 'FAVORITE GENIES';
        } else if(url == 'rateGenie'){
            pageHeading.heading = 'RATE GENIE';
        } else if(url == 'savedcards'){
            pageHeading.heading = 'SAVED CARDS';
        } else if(url == 'transaction'){
            pageHeading.heading = 'ADD ADDRESS';
        }

    }*/
    $("#navHeader").html(Mustache.render(navHeader,pageHeading));
    
    /*$('#sidenav').html(sidenav);*/
    $("#register").click(function(){
        callApi.registerCustomer();
    });
    $("#login").click(function(){
        callApi.loginCustomer();
    });
    $("#forgot-pwd").click(function(){
        callApi.forgotPassword();
    });
    $("#logout").click(function(){
        callApi.logout();
    });
    $("#reset").click(function(){
        callApi.resetPassword();
    });
    $("#terms").click(function(){
        if ($(this).is(':checked')) {
            terms = true;
        } else {
            terms = false;
        }
    });
    //open sidenav
    $('#openNav').click(function(){
         console.log( $('.content').css('transform'));
        if($('.content').css('transform') == 'matrix(1, 0, 0, 1, 0, 0)' || $('.content').css('transform') == 'none'){
            console.log( $('.content').css('transform'));
            $('.fixed-top-nav').css('transform','translate3d(200px , 0px , 0px)');
            $('.foot').css('transform','translate3d(200px , 0px , 0px)');
            $('body').css('overflow','hidden');
            $('.content').css('transform','translate3d(200px , 0px , 0px)');
            $('.fixed-top-nav').css('transform','translate3d(200px , 0px , 0px)');
            $('.foot').css('transform','translate3d(200px , 0px , 0px)');
            $('body').css('overflow','hidden');
        } else{
            console.log('into else')
            $('.content').css('transform','translate3d(0px , 0px , 0px)');
            $('.fixed-top-nav').css('transform','translate3d(0px , 0px , 0px)');
            $('.foot').css('transform','translate3d(0px , 0px , 0px)');
            $('body').css('overflow','scroll');
        }
       
       /* if($('#mySidenav').css('width') != '200px'){
            $('#mySidenav').css("width","200");
            $('body').css('left','200px');
            $('body').css('position','fixed');
            $('.scroll').css('overflow','hidden');
        }
        else{
            $('#mySidenav').css("width","0");
            $('body').css('left','0px');
            $('.scroll').css('overflow','scroll');
        }*/

    });

    
    
});

// function showDetails(){

// }

var sidenav = '<div id="mySidenav" class="sidenav">\
        <div class="m-b-50">\
        <div class="genie-side-img">\
            {{#userDetails.profilePicURL.thumbnail}}<img src="{{userDetails.profilePicURL.thumbnail}}" class="sidenav-genie img-circle">{{/userDetails.profilePicURL.thumbnail}}\
            {{^userDetails.profilePicURL.thumbnail}}<img src="../images/genieicon.png" class="sidenav-genie img-circle">{{/userDetails.profilePicURL.thumbnail}}\
        </div>\
            <p class="john-text text-center" style="text-transform: uppercase">{{userDetails.name}}</p>\
        </div>\
        <a class="sidenav1 hidden">\
          <span></span>\
          <span><img src="../images/notification.png" class="side-menu-img mar-rt"></span>\
          <span>Notifications</span>\
          <span>(5)</span>\
        </a>\
        <a href="../index.html">\
            <span>\
            <img src="../images/getgenie.png" class="side-menu-img mar-side-ic10">\
            </span>Get Genie\
        </a>\
        <a href="../html/mybookings.html">\
            <span>\
            <img src="../images/bookings.png" class="side-menu-img mar-side-ic10">\
            </span>My Bookings\
        </a>\
        <a href="../html/promotions.html">\
            <span>\
            <img src="../images/promotions.png" class="side-menu-img mar-side-ic10">\
            </span>Promotions\
        </a>\
        <a href="../html/mysettings.html">\
            <span>\
            <img src="../images/setttings.png" class="side-menu-img mar-side-ic10">\
            </span>My Settings\
        </a>\
        <a href="../html/support.html">\
            <span>\
            <img src="../images/support.png" class="side-menu-img mar-side-ic10">\
            </span>Support\
        </a>\
        <a id="logout">\
            <span>\
            <img src="../images/logout.png" class="side-menu-img mar-side-ic10">\
            </span>SIGNOUT\
        </a>\
        <div>\
            <span class="font18">VERSION</span>\
            <span class="font18">{{version}}</span>\
        </div>\
    </div>'

var navHeader =
    '<div class="container-fluid">\
    <div class="row clearfix">\
<div class="col-xs-8 col-sm-4 col-md-4 column padding0 nav-login">\
    <span class="login-span pull-right" id="nav-login">\
    <span class="fa fa-sign-in"></span>\
    <span class="login" data-toggle="modal" data-target="#login-box"> SIGNIN </span>\
    <span class="login-separator">|</span>\
    <span class="signup pointer" data-toggle="modal" data-target="#sign-up"> SIGNUP</span>\
    </span>\
    <div class="userbox dropdown-toggle hidden pointer" data-toggle="dropdown" id="user-menu">\
    <img src="../images/user.png" class="img-responsive img-circle user-img"><span style="position: relative;top:-22px;"><p class="user-name"></p><span class="fa fa-caret-down user-menu-arrow"></span></span>\
</div>\
    <!-- header -->\
    <div class="row rm-row back-blue text-align center-align fixed-top-nav">\
        <div class="col-md-2 col-xs-2 col-sm-2" style="height:20px;width:60px;">\
            <img src="../images/menu.png" class="vector" id="openNav">\
        </div>\
        <div class="col-md-10 col-sm-10 col-xs-10 dash-o-div">\
            <p class="p dash-fo" style="text-transform: uppercase;">{{heading}}</p>\
        </div>\
    </div>\
<div id="login-box" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal auto-height">\
    <div class="clearfix auto-height">\
    <div class="col-md-12 column text-center login-text" style="margin-top:22px;">\
    <p class="text-center head-font-42-sscf">SIGNIN</p>\
    <p class="mbottom font18 txt-gray">Already have an account?</p>\
<p class="font18 txt-gray">SIGNIN by entering your details below!</p>\
</div>\
<div class="col-md-12 column text-center">\
    <form>\
    <input type="text" placeholder="Email Address/Username" class="input-login" id="log-email">\
    <input type="password" placeholder="Password" class="input-login" id="log-pwd">\
    </form>\
    <p class="forgot pointer"><span  data-toggle="modal" data-target="#change-pwd" data-dismiss="modal">Forgot password?</span></p>\
<p class="signup-p">Don\'t have an account? <span class="active-color pointer" data-toggle="modal" data-target="#sign-up" data-dismiss="modal">SIGNUP here</span></p>\
</div>\
<div class="col-md-12 column signup-account text-center pointer signHome center-align" id="login" style="margin-top:18px;">\
    <p class="sign-home-footer-text">SIGN IN TO MY ACCOUNT</p>\
</div>\
</div>\
</div>\
</div>\
</div>\
<div id="sign-up" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal auto-height">\
    <div class="clearfix auto-height">\
    <div class="col-md-12 column text-center login-text" style="margin-top:22px;">\
    <p class="text-center head-font-42-sscf">SIGNUP</p>\
    <p class="mbottom font18 txt-gray">Don\'t have an account?</p>\
<p class="font18 txt-gray">Enter your details below and SIGNUP!</p>\
</div>\
<div class="col-md-12 column text-center">\
    <form>\
    <input type="text" placeholder="Your Name" class="input-login" id="reg-name">\
    <input type="text" placeholder="Email Address" class="input-login" id="reg-email">\
    <input type="number" placeholder="+971 Phone" class="input-login" id="reg-phnNo">\
    <input type="password" placeholder="Password" class="input-login" id="reg-pwd"><br/>\
    <input type="checkbox" id="terms" class="signup-checkbox"> By signing up, I agree to <a href="http://www.homegenie.me/term-of-use/" class="active-color">terms of use</a>\
    <p class="signup-p font18">I already have an account.<span class="active-color pointer" data-toggle="modal" data-target="#login-box" data-dismiss="modal"> SIGNIN here</span></p>\
    </form>\
</div>\
<div class="col-md-12 column signup-account text-center pointer signHome center-align" id="register">\
    <p class="sign-home-footer-text">SIGN UP NOW</p>\
</div>\
</div>\
</div>\
</div>\
</div>\
<div id="registration-success" class="modal fade" role="dialog">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal text-center">\
    <div class="col-md-12 column text-center login-text" style="margin-top:42px;">\
    <p class="registration-msg head-font-42-sscf">CONGRATULATIONS</p>\
    </div>\
    <div class="col-md-12 column text-center success-msg">\
    <p class="registration-msg" style="font-size:22pt;">You have successfully registered with HomeGenie.</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p>We have sent you a confirmation of your registration on your email address and your registered mobile number. Please verify your account by clicking on the link sent in the email or the SMS message sent to you.</p>\
<p>Verification of account is required for you to continue with your booking process</p>\
<p>Thankyou!</p>\
</div>\
<div class="col-md-12 column signup-account text-center pointer signHome center-align" data-toggle="modal" data-dismiss="modal" data-target="#verify-otplogin" onclick="verifyRegistration();">\
    <p class="sign-home-footer-text">CONTINUE</p>\
</div>\
\
</div>\
</div>\
</div>\
<span class="hidden" id="verified-modal" data-toggle="modal" data-target="#pwd-verification"></span>\
<div id="pwd-verification" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal text-center">\
    <div class="col-md-12 column text-center login-text" style="margin-top:42px;">\
    <p class="registration-msg head-font-42-sscf">VERIFIED</p>\
    </div>\
    <div class="col-md-12 column text-center success-msg">\
    <p class="registration-msg">You have successfully verified your account</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p>You can now go to My account in your account settings to reset the default password according to your requirements</p>\
<p>Verification of account is required for you to continue with your booking process</p>\
<p><span class="resetpwd pointer" data-toggle="modal" data-target="#change-pwd" data-dismiss="modal">Reset password now</span></p>\
</div>\
<div class="col-md-12 column signup-account text-center pointer signHome center-align" data-dismiss="modal">\
    <p class="sign-home-footer-text">CONTINUE BOOKING SERVICE</p>\
</div>\
</div>\
</div>\
</div>\
<div id="change-pwd" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal change-pwd text-center">\
    <div class="col-md-12 column text-center login-text" style="margin-top:42px;">\
    <p class="registration-msg head-font-42-sscf">CHANGE OR FORGOT PASSWORD?</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p >Enter your email address or mobile number and we\'ll send you a link or code to reset your password.</p>\
<input type="text" placeholder="Email/Mobile No.(+971-XXXXXXXXX)" class="input-login" id="pwd-email">\
    </div>\
    <div class="col-md-12 column signup-account text-center pointer center-align" id="forgot-pwd">\
    <p class="sign-home-footer-text">RESET PASSWORD</p>\
</div>\
</div>\
</div>\
</div>\
<span id="verifyotp" data-toggle="modal" data-target="#verify-otp"></span>\
<div id="verify-otp" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal change-pwd text-center">\
    <div class="col-md-12 column text-center login-text" style="margin-top:42px;">\
    <p class="registration-msg head-font-42-sscf">VERIFICATION</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p style="color: gray;">Please enter the One Time Password you just received via SMS at +971<span id="mobile"></span></p>\
<input type="text" placeholder="Enter One Time Password" class="input-login" id="otp">\
<p class="text-center">Not Received OTP?</p>\
<p class="text-center"><button class="re-generate-otp" onclick="callApi.forgotPassword();">Re-Generate OTP</button></p>\
    </div>\
    <div class="col-md-12 column signup-account text-center pointer signHome center-align" onclick="callApi.verifyOTP()" id="confirm-otp">\
    <p class="sign-home-footer-text">CONFIRM</p>\
</div>\
</div>\
</div>\
</div>\
</div>\
<span id="verifyloginotp" data-toggle="modal" data-target="#verify-otplogin"></span>\
<div id="verify-otplogin" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal change-pwd text-center">\
    <div class="col-md-12 column text-center login-text" style="margin-top:42px;">\
    <p class="registration-msg head-font-42-sscf">VERIFICATION</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p class="txt-gray">Please enter the One Time Password you just received via SMS at +971<span id="mobileotp"></span></p>\
<input type="number" placeholder="Enter One Time Password" class="input-login" id="otplogin">\
<p class="text-center">Not Received OTP?</p>\
<p class="text-center"><button class="re-generate-otp" onclick="resendLoginOTP();">Re-Generate OTP</button></p>\
    </div>\
    <div class="col-md-12 column signup-account text-center pointer signHome center-align" onclick="verifyLoginOTP()" id="confirm-otp">\
    <p class="sign-home-footer-text">CONFIRM</p>\
</div>\
</div>\
</div>\
</div>\
</div>\
<span id="setpwd" data-toggle="modal" data-target="#set-pwd"></span>\
<div id="set-pwd" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
    <div class="modal-content login-modal change-pwd newpwd text-center">\
    <div class="col-md-12 column text-center login-text">\
    <p class="registration-msg">NEW PASSWORD</p>\
</div>\
<div class="col-md-12 column text-center thankyou" style="padding:0px 15px">\
    <p style="padding-left:5px;padding-right:5px;">Please set your new password below</p>\
<input type="password" placeholder="New Password" class="input-login" id="new-pwd">\
    </div>\
    <div class="col-md-12 column signup-account text-center pointer signHome center-align" onclick="callApi.setNewPassword();" id="confirm-otp">\
    <p class="sign-home-footer-text">CONFIRM</p>\
</div>\
</div>\
</div>\
</div>\
</div>\
</div>\
</div>';

var categoryTemplate = '{{#data}}<div class="col-md-4 col-xs-4 col-sm-4  col-category text-center" name={{name}} onclick="chooseCategory(this)">\
                            <div style="width:100%;height:70px;" class="category-img-align">\
                                <img src="{{imageURL.thumbnail}}" style="max-width:50px;margin:0 auto;">\
                            </div>\
                            <p class="gray-text">{{name}}</p>\
                        </div>{{/data}}';


var errorMsgDisplayTemp = 
'<div class="modal fade" id="err-modal" role="dialog" data-backdrop="static" data-keyboard="false">\
<div class="modal-dialog">\
<!-- Modal content-->\
<div class="modal-content">\
<div class="modal-body">\
<p class="p hw-md txt-gray">{{message}}</p>\
</div>\
<div class="modal-footer md-hw-foot err-modal-continue center-align" style="height:40px;">\
<div style="font-size:18px;" class="width-100">CONTINUE</button>\
</div>\
</div>\
</div>\
</div>';

var MsgDisplayTemp = 
'<div class="modal fade msg-modal" role="dialog" data-backdrop="static" data-keyboard="false">\
<div class="modal-dialog">\
<!-- Modal content-->\
<div class="modal-content">\
<div class="modal-body">\
<p class="p hw-md txt-gray">{{msg}}</p>\
</div>\
<div class="modal-footer md-hw-foot msg-modal-continue re-direct-home center-align msg-continue" style="height:40px;" data-dismiss="modal">\
<div style="font-size:18px;" class="width-100">CONTINUE</button>\
</div>\
</div>\
</div>\
</div>';

var assign_genie_template =
'<div class="modal fade" id="assign-genie-modal" role="dialog">\
    <div class="modal-dialog">\
      <!-- Modal content-->\
      <div class="modal-content">\
           <p class="notification-text text-center found-genie" style="margin-top:20px">We found you a Genie!</p>\
            <div class="col-md-12 col-xs-12 col-sm-12 ass-gen-img">\
                <img src="{{imgsrc}}" class="width-100" style="border:3px solid gray;border-radius:50%;">\
            </div>\
            <div style="margin-top:90px;padding: 15px;">\
            <div class="col-xs-12 col-sm-12 col-md-12">\
                <p class="txt-gray text-center g-name">\
                    {{name}}\
                    <img src="{{starsrc1}}" id="star1" class="star-h">\
                    <img src="{{starsrc2}}" id="star2" class="star-h">\
                    <img src="{{starsrc3}}" id="star3" class="star-h">\
                    <img src="{{starsrc4}}" id="star4" class="star-h">\
                    <img src="{{starsrc5}}" id="star5" class="star-h">\
                </p>\
            </div>\
            <p class="text-center ass-congr">Congratulations</p>\
            <p class="assign-genie text-center p" style="font-size:17px;">\
                We have successfully assigned Genie ({{name}})<br>\
                to your booking.If you wish to speak with\
                him,please reach out to him directly on\
                <a href="tel:+{{driverPhn}}">{{driverPhn}}</a> or his supervisor on\
                <a href="tel:+{{supplierPhn}}">{{supplierPhn}}</a>\
            </p>\
        </div>\
        <div class="modal-footer assign-genie-footer center-align" id={{appointmentID}} onclick="assignGenie(this)">\
            <p class="width-100 text-center p">OKAY</p>\
        </div>\
      </div>\
    </div>\
  </div>'

  var view_estimate_template =
  '<div class="modal fade" id="view-estimate-modal" role="dialog">\
    <div class="modal-dialog">\
      <!-- Modal content-->\
      <div class="modal-content">\
            <div class="col-md-12 col-xs-12 col-sm-12 est-not-img">\
                <img src={{imgsrc}} class="width-100">\
            </div>\
            <div style="margin-top: 110px;">\
                <p class="notification-text text-center p">Kindly accept the estimate for</p>\
                <p class="notification-text text-center p">Your genie to start working on the</p>\
                <p class="notification-text text-center p" style="margin-bottom: 30px !important">booking</p>\
                <p class="notification-text text-center p">cost estimate for the job is AED.</p>\
                <p class="notification-text text-center p">{{estimate}}</p>\
                <div class="col-md-12 col-xs-12 col-sm-12 rm-col">\
                    <div class="col-md-6 col-sm-6 col-xs-6 accept-div center-align">\
                        <p class="width-100 text-center p" status={{status}} id={{appointmentID}} onclick="acceptJob(this)">APPROVE</p>\
                    </div>\
                    <div class="col-md-6 col-sm-6 col-xs-6 reject-div center-align" id={{appointmentID}} onclick="rejectEstimate(this)">\
                        <p class="width-100 text-center p">REJECT</p>\
                    </div>\
                </div>\
                <a id="a-vie-est" href="{{newhref}}"><p class="text-center view-est">View Estimate</p></a>\
            </div>\
      </div>\
    </div>\
  </div>'

var job_completed_template = '<div class="modal fade" id="job-completed-modal" role="dialog">\
    <div class="modal-dialog">\
        <div class="modal-content">\
            <div class="modal-body">\
                <div class="green-check-bag">\
                    <i class="fa fa-check fa-green-check"></i>\
                </div>\
                <p class="txt-gray compalints-txt text-center p">Your GENIE has successfully</p>\
                <p class="txt-gray compalints-txt text-center p">completed job</p>\
            </div>\
            <div class="modal-footer rm-mod-foot">\
                <div class="col-md-6 col-xs-6 col-sm-6 not-view-pro vi-de-co">\
                    <a href="{{bookingshref}}" id="a-completed"><p class="p width-100 text-center">VIEW DETAILS</p></a>\
                </div>\
                <div class="col-md-6 col-xs-6 col-sm-6 not-view-pro pro-pay-co">\
                   <div id={{appointmentID}} amount={{totalAmount}} onclick="makePayment(this)"><p class="p width-100 text-center">PROCEED TO PAY</p></div>\
                </div>\
            </div>\
        </div>\
    </div>\
  </div>'

var complaints_template = '<div class="modal fade" role="dialog" id="complaints-modal" data-keyboard="false" data-backdrop="static">\
    <div class="modal-dialog">\
        <div class="modal-content">\
            <div class="col-md-12 col-xs-12 col-sm-12" style="padding: 20px 0px;">\
                <div class="complaints-exc-circle">\
                    <i class="fa fa-exclamation excla"></i>\
                </div>\
            </div>\
            <p class="txt-gray compalints-txt text-center p">The service has not been</p>\
            <p class="txt-gray compalints-txt text-center p">completed upon multiple attempts.</p>\
            <p class="txt-gray compalints-txt text-center p">Please register your comments or</p>\
            <p class="txt-gray compalints-txt text-center p" style="margin-bottom: 30px !important;">complaint below.</p>\
            <div class="col-sm-12 col-xs-12 col-md-12">\
                <input type="text" class="width-100 complaints-box input textarea" id="registerComplaint">\
            </div>\
            <div class="submit-div center-align modal-footer">\
                <p class="text-center width-100" style="margin: 0px;font-size: 20px;" id={{appointmentID}} onclick="complaint(this)">SUBMIT</p>\
            </div>\
        </div>\
    </div>\
  </div>'

  var view_estimate_later_template =
  '<div class="modal fade" id="view-estimate-later-modal" role="dialog">\
    <div class="modal-dialog">\
      <!-- Modal content-->\
      <div class="modal-content">\
            <div class="col-md-12 col-xs-12 col-sm-12 est-not-img">\
                <img src={{imgsrc}} class="width-100">\
            </div>\
            <div style="margin-top: 110px;">\
                <p class="notification-text text-center p">Your GENIE will be completing</p>\
                <p class="notification-text text-center p" style="margin-bottom: 30px !important">the service on {{showScheduleDate}}</p>\
                <p class="notification-text text-center p">cost estimate for the job is AED.</p>\
                <p class="notification-text text-center p">{{estimate}}</p>\
                <div class="col-md-12 col-xs-12 col-sm-12 rm-col">\
                    <div class="col-md-6 col-sm-6 col-xs-6 accept-div center-align">\
                        <p class="width-100 text-center p"  id={{appointmentID}} status={{status}} onclick="acceptJob(this)">APPROVE</p>\
                    </div>\
                    <div class="col-md-6 col-sm-6 col-xs-6 reject-div center-align" onclick="rejectEstimate(\'{{appointentID}}\')">\
                        <p class="width-100 text-center p">REJECT</p>\
                    </div>\
                </div>\
                <a id="a-vie-est" href="{{newhref}}"><p class="text-center view-est">View Estimate</p></a>\
            </div>\
      </div>\
    </div>\
  </div>'

 var estimate_rejected_template = '<div class="modal fade" id="estimate-rejected-modal" role="dialog">\
    <div class="modal-dialog">\
        <div class="modal-content">\
            <div class="modal-body">\
                <p class="txt-gray compalints-txt text-center p">Are you sure you want to reject the job</p>\
            </div>\
            <div class="modal-footer rm-mod-foot reestimateorclose">\
                <div class="col-md-6 col-xs-6 col-sm-6 not-view-pro vi-de-co" id={{id}} status="REJECTED" onclick="reestimateOrClose(this)">\
                    <p class="p width-100 text-center">REVIEW ESTIMATE</p>\
                </div>\
                <div class="col-md-6 col-xs-6 col-sm-6 not-view-pro pro-pay-co reestimateorclose" id={{id}}  status="CLOSED" onclick="reestimateOrClose(this)">\
                   <p class="p width-100 text-center">CLOSE</p>\
                </div>\
            </div>\
        </div>\
    </div>\
  </div>'
