/**
 * Created by sumit on 13/10/16.
 */
var udata, updateAddress, addressId,addresses, addressData = {}, editlat, editlong;

function getExistingAddress(data) {
    addresses = data;
    //console.log(data.data);
    for (var k in data.data) {
        if (data.data[k]["IsdefaultAddress"] == "TRUE") {
            data.data[k]["IsdefaultAddress"] = true;
        } else {
            data.data[k]["IsdefaultAddress"] = false;
        }
    }
    var existingAddress = Mustache.render(address_template, data);
    $("#existingAddress").html(existingAddress);
    $.LoadingOverlay("hide", true);
    getAddressId();
}

function getAndUpdateAddress(){
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "../index.html";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getAllAddress", params, function(err, result){
        if (!err) {
            getExistingAddress(result);
        } else {
            callApi.error(err);
        }
    });
}
function initMap(lat, long) {
    setTimeout(function() {
        var map_options = {
            center: new google.maps.LatLng(lat, long),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var myLatlng = new google.maps.LatLng(lat, long);
        var map = new google.maps.Map(document.getElementById("map"), map_options);
        var markers = new google.maps.Marker({
            position:myLatlng,
            draggable:true,
            map:map
        });
        
        markers.setMap(map);

        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(markers, 'dragend', function(event) {
            if (updateAddress) {
                editlat = this.getPosition().lat();
                editlong = this.getPosition().lng();
            }
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, markers);
                        var streetaddress = "";
                        for (var k in results[0]["address_components"]) {
                            /*if (results[0]["address_components"][k]["types"][0] == "street_number") {
                                streetaddress += results[0]["address_components"][k]["long_name"] + " ";
                            }
                            if (results[0]["address_components"][k]["types"][0] == "route") {
                                streetaddress += results[0]["address_components"][k]["long_name"];
                            }*/
                            if (results[0]["address_components"][k]["types"][0] == "locality") {
                                var city = results[0]["address_components"][k]["long_name"];
                                $("#city").val(city);
                            }
                        }
                        //$("#streetAddress").val(streetaddress);
                    }
                }
            });
        });
        google.maps.event.addListener(map, 'click', function(event) {
            if (updateAddress) {
                editlat = this.getPosition().lat();
                editlong = this.getPosition().lng();
            }
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, markers);
                        var streetaddress = "";
                        for (var k in results[0]["address_components"]) {
                            //if (results[0]["address_components"][k]["types"][0] == "street_number") {
                            //    streetaddress += results[0]["address_components"][k]["long_name"] + " ";
                            //}
                            //if (results[0]["address_components"][k]["types"][0] == "route") {
                            //    streetaddress += results[0]["address_components"][k]["long_name"];
                            //}
                            if (results[0]["address_components"][k]["types"][0] == "locality") {
                                var city = results[0]["address_components"][k]["long_name"];
                                $("#city").val(city);
                            }
                        }
                        //$("#streetAddress").val(streetaddress);
                    }
                }
            });
        });

        var input = document.getElementById("searchMap");
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);

        var marker = new google.maps.Marker({map: map, draggable:true});
        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'dragend', function(event) {
            if (updateAddress) {
                editlat = this.getPosition().lat();
                editlong = this.getPosition().lng();
            }
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                        for (var k in results[0]["address_components"]) {
                            //if (results[0]["address_components"][k]["types"][0] == "route") {
                            //    var streetaddress = results[0]["address_components"][k]["long_name"];
                            //    $("#streetAddress").val(streetaddress);
                            //}
                            if (results[0]["address_components"][k]["types"][0] == "locality") {
                                var city = results[0]["address_components"][k]["long_name"];
                                $("#city").val(city);
                            }
                        }
                    }
                }
            });
        });

        //var addressWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, "place_changed", function()
        {
            //infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(15);
            }

            markers.setPosition(place.geometry.location);

            if (place["formatted_address"]) {
                infowindow.setContent(place.formatted_address);
                var address;
                address = place.formatted_address;
                address = address.split("-");
                var addressLen = address.length;
                if (addressLen < 3) {
                    //$("#streetAddress").val(address[0]);
                    $("#city").val(address[1]);
                } else {
                    //$("#streetAddress").val(address[addressLen-3]);
                    $("#city").val(address[addressLen-2]);
                }
            }

        });

        /* google.maps.event.addListener(map, "click", function(event) {
         marker.setPosition(event.latLng);
         });*/
         $("#map-modal").on("shown.bs.modal", function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter(myLatlng);
        });

    }, 1000);
}


$('#map-footer,.msg-modal-continue').click(function(){
    $("#map-modal").modal("hide");
    $('.msg-modal').modal('hide');
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
});
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}
function showPosition(position) {
    initMap(position.coords.latitude,position.coords.longitude);
}
function getAddressId() {
    $(".edit-btn").click(function(){
        updateAddress = true;
        addressId = $(this).attr("data-attr");
        var count = 0, addressType, defaultAddress;
        for (var k in addresses.data) {
            count++;
            if (addresses.data[k]["_id"] === addressId) {
                count--;
                var latlong = addresses.data[k]["locationLongLat"];
                editlat = latlong[0];
                editlong = latlong[1];
                initMap(latlong[0],latlong[1]);
                break;
            }
        }
        addressType = addresses.data[count].addressType;
        defaultAddress = addresses.data[count].IsdefaultAddress;
        $("#nickName").val(addresses.data[count].nickName);
        $("#apartmentNo").val(addresses.data[count].apartmentNo);
        $("#streetAddress").val(addresses.data[count].streetAddress);
        $("#community").val(addresses.data[count].community);
        $("#city").val(addresses.data[count].city);
        $("#emirate").val(addresses.data[count].emirate);
        if (addressType == "VILLA") {
            var addressHtml = '<option selected value="VILLA">VILLA</option><option value="APARTMENT">APARTMENT</option>';
            $("#addressType").html(addressHtml);
        } else {
            var addressHtml = '<option selected value="APARTMENT">APARTMENT</option><option value="VILLA">VILLA</option>';
            $("#addressType").html(addressHtml);
        }
        if (defaultAddress == false) {
            var defaultAddressHtml = '<option selected value="FALSE">NO</option><option value="TRUE">YES</option>';
            $("#defaultAddress").html(defaultAddressHtml);
        } else {
            var defaultAddressHtml = '<option selected value="TRUE">YES</option><option value="FALSE">NO</option>';
            $("#defaultAddress").html(defaultAddressHtml);
        }
    });

    $(".delete-btn").click(function(){
        var params = {};
        updateAddress = false;
        addressData.that = $(this);
        addressData.addressId = $(this).attr("data-attr");
        params.data = {};
        params._method = "DELETE";
        params.Auth = udata.data.accessToken;
        addressData.params = params;
        $("#confirm-delete").click();
    });
}

function deleteAddress() {
    var addressId, params, that;
    addressId = addressData.addressId;
    params = addressData.params;
    that = addressData.that;
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/removeAddress?addressId=" + addressId ,params, function(err, results){
        if (!err) {
            that.parent().parent().parent().hide();
            $("#delete-address").modal("hide");
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}



$(".submit-address").click(function(){
    var data = new FormData();
    var name, apartmentNo, streetAddress, community, city, emirate, lat, long;
    name = $("#nickName").val();
    apartmentNo = $("#apartmentNo").val();
    streetAddress = $("#streetAddress").val();
    community = $("#community").val();
    city = $("#city").val();
    emirate = $("#emirate").val();
    $('#add-address').css('overflow-y','scroll');
    if (city) {
        if (!streetAddress) {
            //$.growl.error({message : "Please enter your street address"});
            var message = {"msg":"Please enter your street address"}
            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
            $('.msg-modal').modal('show');
            return;
        }
        /*if (!emirate) {
            $.growl.error({message : "Please enter your emirate"});
            return;
        }*/
        var geocoder =  new google.maps.Geocoder();
        var place = city + ", " + emirate;
        geocoder.geocode( { 'address':place }, function(results, status) {
            if (results[0]) {
                lat = results[0].geometry.location.lat();
                long  = results[0].geometry.location.lng();
                if (!name) {
                    //$.growl.error({message : "Please enter a nickname"});
                    var message = {"msg":"Please enter a nickname"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                if (!apartmentNo) {
                    // $.growl.error({message : "Please enter your Apartment No."});
                    var message = {"msg":"Please enter your Apartment No."}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                if (!community) {
                    // $.growl.error({message : "Please enter your community"});
                    var message = {"msg":"Please enter your community"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                if (!lat) {
                    // $.growl.error({message : "Sorry, Not able to find your latitude. Please enter the correct city"});
                    var message = {"msg":"Sorry, Not able to find your latitude. Please enter the correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                if (!long) {
                    //$.growl.error({message : "Sorry, Not able to find your longitude. Please enter the correct city"});
                    var message = {"msg":"Sorry, Not able to find your longitude. Please enter the correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                data.append("nickName", name);
                data.append("apartmentNo", apartmentNo);
                data.append("streetAddress", streetAddress);
                data.append("communtity", community);
                data.append("city", city);
                data.append("emirate", "UAE");
                data.append("addressType",$("#addressType option:selected").val());
                data.append("IsdefaultAddress",$("#defaultAddress option:selected").val());
                if (updateAddress) {
                    data.append("locationLat",editlat);
                    data.append("locationLong",editlong);
                    data._method = "PUT";
                    data.Auth = udata.data.accessToken;
                    data.append("addressesID",addressId);
                    $.LoadingOverlay("show");
                    callApi.queryUrl(apiUrl + "/api/customer/editAddress", data, function(err, result){
                        if (!err) {
                            $("#add-address").modal('hide');
                            $.LoadingOverlay("hide", true);
                            // $.growl.notice({message : "Your address has been updated."});
                            var message = {"msg":"Your address has been updated."}
                            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                            $('.msg-modal').modal('show');
                            getAndUpdateAddress();
                        } else {
                            $.LoadingOverlay("hide", true);
                            callApi.error(err);
                        }
                    });
                } else {
                    data.append("locationLat",lat);
                    data.append("locationLong",long);
                    data._method = "POST";
                    data.Auth = udata.data.accessToken;
                    $.LoadingOverlay("show");
                    callApi.queryUrl(apiUrl + "/api/customer/addNewAddress", data, function(err, result){
                        if (!err) {
                            $("#add-address").modal('hide');
                            $.LoadingOverlay("hide", true);
                            // $.growl.notice({message : "Your address has been added."});
                            var message = {"msg":"Your address has been added."}
                            $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                            $('.msg-modal').modal('show');
                            getAndUpdateAddress();
                        } else {
                            $.LoadingOverlay("hide", true);
                            callApi.error(err);
                        }
                    });
                }
            } else {
                if (streetAddress.match(/unnamed/i)) {
                    // $.growl.error({message : "Please enter correct street address"});
                    var message = {"msg":"Please enter correct street address"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                if (!city) {
                    // $.growl.error({message : "Please enter correct city"});
                    var message = {"msg":"Please enter correct city"}
                    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                    $('.msg-modal').modal('show');
                    return;
                }
                // $.growl.error({message : "Select within Dubai Only"});
                var message = {"msg":"Select within Dubai Only"}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                return;
            }
        });
    } else {
        // $.growl.error({message : "Please enter your city"});
        var message = {"msg":"Please enter your city"}
        $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
        $('.msg-modal').modal('show');
        return;
    }
});

$("#addnewAddress").click(function(){
    $("#searchMap").val("");
    $("#map").html("");
    $("#nickName").val("");
    $("#apartmentNo").val("");
    $("#streetAddress").val("");
    $("#community").val("");
    $("#city").val("");
    //$("#emirate").val("");
    $("#addressType option:selected").val();
    $("#defaultAddress option:selected").val();
    $("#addressType").html("<option value='APARTMENT' selected>APARTMENT</option><option value='VILLA'>VILLA</option>");
    $("#defaultAddress").html('<option value="TRUE" selected>YES</option><option value="FALSE">NO</option>');
    var latlong = countryInfo.loc.split(",");
    // getLocation();
    editlat = "25.2048";
    editlong = "55.2708";
    initMap(editlat, editlong);
});
document.addEventListener('keyup', function(e) {
    if (e.keyCode == 27) {
        $("#add-address").modal("hide");
    }
});

$(document).ready(function(){
   getAndUpdateAddress();
});

var address_template =
    '{{#data}}<div class="col-xs-12 col-sm-12 col-md-12 column add-review-list">\
        <div class="col-xs-12 col-sm-6 col-md-6 column address-col" style="padding-bottom:0px;">\
            <p class="address-list active-color" style="margin-top:15px;">{{nickName}} {{#IsdefaultAddress}}(Default){{/IsdefaultAddress}}</p>\
            <p class="address-list" style="margin-top:10px;">{{#apartmentNo}}{{apartmentNo}},{{/apartmentNo}} {{#streetAddress}}{{streetAddress}},{{/streetAddress}} {{#community}}{{community}},{{/community}} {{city}}</p>\
        </div>\
        <div class="col-xs-12 col-sm-6 col-md-6 column address-col">\
            <span class="span-float" style="margin-top:25px;">\
                <button class="edit-btn editbag" data-attr="{{_id}}" data-toggle="modal" data-target="#add-address">EDIT</button>\
                <button class="delete-btn" data-attr="{{_id}}">DELETE</button>\
            </span>\
        </div>\
    </div>{{/data}}';
