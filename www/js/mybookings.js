/**
 * Created by sumit on 19/10/16.
 */
var udata, bookingData,
    subcategory = [],
    monthArray = ["Jan", "Feb", "Mar", "Apr","May", "June","July", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dayArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri","Sat"];

function autocompleteSearch() {
    $('#search-ongoing, #search-completed').devbridgeAutocomplete({
        lookup: subcategory,
        minChars: 1,
        onSelect: function (suggestion) {
            var bookingsearchinput = suggestion.value;
            $('.ongoingbookingsearch').each(function () {
                var service = $(this).attr("data");
                if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                    $(this).addClass('hidden');
                }
                else {
                    $(this).removeClass('hidden');
                }
            });
            $('.completedbookingsearch').each(function () {
                var service = $(this).attr("data");
                if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                    $(this).addClass('hidden');
                }
                else {
                    $(this).removeClass('hidden');
                }
            });
        },
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Sorry, no matching results'
    });
}
function updateOngoingAndCompletedBooking(results) {
    var data = {}, htmlongoing, htmlcompleted, bookingOngoing, bookingPast;
    bookingOngoing = results.data.upcomingAppointment;
    bookingPast = results.data.pastBooking;
    for (var k in bookingOngoing) {
        var slot, date, month, day, year, bookingdate;
        slot = {
            "8":"8AM - 10AM",
            "10":"10AM - 12PM",
            "12":"12PM - 2PM",
            "14":"2PM - 4PM",
            "16":"4PM - 6PM",
            "18":"6PM - 8PM"
        };
        bookingOngoing[k]["slotBooked"]["slot"] = slot[bookingOngoing[k]["slotBooked"]["slot"]];
        date = new Date(bookingOngoing[k]["scheduleDate"]);
        month = monthArray[date.getMonth()];
        day = dayArray[date.getDay()];
        year = date.getFullYear();
        date = date.getDate();
        bookingOngoing[k]["scheduleDate"] = day + " " + date + " " + month + " " + year + " | " + bookingOngoing[k]["slotBooked"]["slot"];
        bookingOngoing[k]["dateScheduled"] = bookingOngoing[k]["slotBooked"]["slot"] + ", " + month + " " + date;
        bookingdate = bookingOngoing[k]["utc_timing"]["requestedTime"];
        bookingdate = new Date(bookingdate).toLocaleString();
        bookingdate = bookingdate.split(",");

        bookingOngoing[k]["utc_timing"]["requestedTime"] = bookingdate;
        if (bookingOngoing[k]["favouriteGenie"] === "FALSE") {
            bookingOngoing[k]["favouriteGenie"] = false;
        }
        //for total charges
        var totalCharge = bookingOngoing[k]["charges"]["totalCharges"];
        if (!totalCharge && bookingOngoing[k]["status"] !== "CANCELLED") {
            bookingOngoing[k]["charges"]["totalCharges"] = "To be decided";
        }
        if (bookingOngoing[k]["charges"]["unitCharges"]) {
            bookingOngoing[k]["services"] = "Fixed price";
            bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["unitCharges"];
        } else {
            bookingOngoing[k]["services"] = "Inspection based";
        }
        if (bookingOngoing[k]["charges"]["estimateCharges"]) {
            bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["estimateCharges"];
        }
        if (totalCharge == bookingOngoing[k]["charges"]["cancellationCharges"]) {
            bookingOngoing[k]["charges"]["totalCharges"] = totalCharge;
        }
        if (bookingOngoing[k]["payment"] && bookingOngoing[k]["payment"]["payment_type"] === "null") {
            bookingOngoing[k]["payment"]["payment_type"] = "On Completion";
        }


        bookingOngoing[k]["Payment"] = true;
        var status = bookingOngoing[k]["status"];
        switch(status) {
            case "REQUESTED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = true;
                break;
            case "ASSIGNED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = true;
                break;
            case "CANCELLED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "PAYMENT_PENDING":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = false;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "IN_SERVICE":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "RATING":
                bookingOngoing[k]["RateGenie"] = true;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = false;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "COMPLAINT_REGISTERED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "INSPECTION":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "RESCHEDULED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            case "REJECTED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = true;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                bookingOngoing[k]["charges"]["totalCharges"] = bookingOngoing[k]["charges"]["callOutCharges"];
                break;
            case "ENROUTE":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = true;
                break;
            case "UNFINISHED":
                bookingOngoing[k]["RateGenie"] = false;
                bookingOngoing[k]["Payment"] = false;
                bookingOngoing[k]["View"] = true;
                bookingOngoing[k]["Cancel"] = false;
                break;
            default:
                break;
        }
    }
    for (var k in bookingPast) {
        var slot = {
            "8":"8AM - 10AM",
            "10":"10AM - 12PM",
            "12":"12PM - 2PM",
            "14":"2PM - 4PM",
            "16":"4PM - 6PM",
            "18":"6PM - 8PM"
        };
        if (bookingPast[k]["payment_status"] == "PENDING") {
            bookingPast[k]["payment_status"] = "SETTLED";
        }
        bookingPast[k]["slotBooked"]["slot"] = slot[bookingPast[k]["slotBooked"]["slot"]];
        var date = bookingPast[k]["scheduleDate"];
        date = new Date(date);
        month = monthArray[date.getMonth()];
        day = dayArray[date.getDay()];
        year = date.getFullYear();
        date = date.getDate();
        bookingPast[k]["scheduleDate"] = day + " " + date + " " + month + " " + year + " | " + bookingPast[k]["slotBooked"]["slot"];
        bookingPast[k]["dateScheduled"] = bookingPast[k]["slotBooked"]["slot"] + ", " + month + " " + date;
        var bookingdate = bookingPast[k]["utc_timing"]["requestedTime"];
        bookingdate = new Date(bookingdate).toLocaleString();
        bookingdate = bookingdate.split(",");

        bookingPast[k]["utc_timing"]["requestedTime"] = bookingdate;
        if (bookingPast[k]["favouriteGenie"] === "FALSE") {
            bookingPast[k]["favouriteGenie"] = false;
        }
        if (bookingPast[k]["charges"]["unitCharges"]) {
            bookingPast[k]["services"] = "Fixed price";
        } else {
            bookingPast[k]["services"] = "Inspection based";
        }
        if (bookingPast[k]["payment"] && bookingPast[k]["payment"]["payment_type"] === "null") {
            bookingPast[k]["payment"]["payment_type"] = "";
        }
    }
    data.ongoing = bookingOngoing;
    data.completed = bookingPast;

    if ((data.ongoing).length > 0) {
        //console.log("booking");
        htmlongoing = Mustache.render(ongoingBooking_template, data);
        $("#ongoing").html(htmlongoing);
    } else {
        $("#ongoing").html("<p class='booking-msg'>You have not booked any services at yet.</p>");
    }
    if ((data.completed).length > 0) {
        htmlcompleted = Mustache.render(completedBooking_template, data);
        $("#completed").html(htmlcompleted);
    } else {
        $("#completed").html("<p class='booking-msg'>You have not booked any services or your booking has not completed at yet.</p>");
    }


    var params = {};
    callApi.getUrl(apiUrl + "/api/subcategory/getAllSubCategories",params,function(err, response){
        if (!err) {
            //console.log(response.data);
            var subcategoryArray = response.data;
            for(var k in subcategoryArray) {
                subcategory.push(subcategoryArray[k].subCategoryName);
            }
            autocompleteSearch();
        } else {
            callApi.error(err);
        }
    });

    initOngoingBookingSearch();
    $.LoadingOverlay("hide", true);

}

function getmybookings() {
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        window.location.href = "../index.html";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getmybookings", params, function(err, result){
        if (!err) {
            bookingData = result;
            updateOngoingAndCompletedBooking(result);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function getBookingDetails(id, bookingstatus) {
    //$("body").addClass("modalOpen");
    var appntmntId = id;
    var params = new FormData();
    params.append("appointmentId",appntmntId);
    params.Auth = udata.data.accessToken;
    params._method = "POST";
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/getJobDetails", params, function(err, response){
        if (!err) {

            var data = {}, notes, acceptRejectTemplate;
            acceptRejectTemplate = '{{#id}}<div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{id}}" onclick="acceptOrRejectJob(\'{{id}}\', \'APPROVE\')" style="background: #67e9b4;">\
                    <p style="position:relative;top:7px;">ACCEPT</p>\
                </div>\
                <div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{id}}" onclick="acceptOrRejectJob(\'{{id}}\', \'REJECTED\')">\
                    <p style="position:relative;top:7px;">REJECT</p>\
                </div>{{/id}}';
            var results = JSON.parse(response);
            data.id = results.data[0]["_id"];
            data.pricedetails = results.data;
            data.servicedetails = results.data;
            data.Notes = (results.data[0].subCategory.Notes).split("Note");
            notes = data.Notes;
            if (notes[0]) {
                data.Notes1 = (notes[0]).split("-");
            }
            if (notes[1]) {
                data.Notes2 = (notes[1]).split("-");
            }
            //console.log(notes);
            if (results.data[0].charges.unitCharges) {
                data.serviceBasedType = "FIXED PRICE SERVICE";
                data.seviceBasedPrice = results.data[0].charges.unitCharges;
                data.serviceCharge = "Service Charges : ";
            } else {
                data.serviceBasedType = "INSPECTION BASED SERVICE";
                data.seviceBasedPrice = results.data[0].charges.callOutCharges;
                data.serviceCharge = "Call Out Charges* : ";
                data.totalCharges = "To be decided";
            }
            data.genie = results.data[0].driverData;
            if (data.genie && data.genie.profilePicURL && !data.genie.profilePicURL.thumbnail) {
                data.genie.profilePicURL.thumbnail = "../images/genieicon.png";
                data.genie.appointmentID = appntmntId;
            }
            data.address = results.data[0].address;

            if (bookingstatus === "past") {
                var detailData = {}, resultdata;
                resultdata = results.data[0];
                var slot, date, month, day, year;
                slot = {
                    "8":"8AM - 10AM",
                    "10":"10AM - 12PM",
                    "12":"12PM - 2PM",
                    "14":"2PM - 4PM",
                    "16":"4PM - 6PM",
                    "18":"6PM - 8PM"
                };
                resultdata["slot"] = slot[resultdata["slot"]];
                resultdata.services = data.serviceBasedType;

                if (resultdata["payment"]["payment_type"] == "null") {
                    resultdata["payment"]["payment_type"] = null;
                    //console.log("here")
                }

                var date = results.data[0]["scheduleDate"];
                date = new Date(date);
                month = monthArray[date.getMonth()];
                day = dayArray[date.getDay()];
                year = date.getFullYear();
                date = date.getDate();
                resultdata.scheduleDate = day + " " + date + " " + month + " " + year + " | " + resultdata.slot;
                resultdata["dateScheduled"] = resultdata["slot"] + ", " + month + " " + date;

                var bookingdate = resultdata["utc_timing"]["requestedTime"];
                bookingdate = new Date(bookingdate).toLocaleString();
                bookingdate = bookingdate.split(",");
                resultdata["utc_timing"]["requestedTime"] = bookingdate;
                var jobStatus = results.data[0].status;
                if (jobStatus == "CANCELLED") {
                    resultdata["payment_status"] = "SETTLED";
                }
                detailData.viewDetails = resultdata;

                var detailHtml = Mustache.render(viewDetailsTemplate, detailData);
                //console.log(detailHtml);
                $("#popupmodal").html(detailHtml);
                if (jobStatus == "REJECTED") {
                    $("#" + id + "-total").html(resultdata.charges.callOutCharges);
                }
                var problemImage = results.data[0].problemImages;
                if (problemImage && problemImage.length > 0) {
                    var problemData = {}, problemImageArray = [], problemImageTemplate;

                    for (var k in problemImage) {
                        problemImageArray.push(problemImage[k].thumbnail);
                    }
                    problemData.problemImage = problemImageArray;

                    problemImageTemplate = '<p class="active-color">Uploaded Photo</p>\
                        {{#problemImage}}<div class="genie-box">\
                            <img src="{{.}}" class="img-responsive" alt="{{genieName}}" style="width:100%;">\
                        </div>{{/problemImage}}';

                    $(".problemImage-" + id).html(Mustache.render(problemImageTemplate,problemData));
                }
                $("#pricing-modal").html(Mustache.render(pricingtemplate, data));
                $("#address-" + id).html(Mustache.render(addressTemplate, data));
                //$(".payprice-" + id).html(Mustache.render(pricingtemplate, data));
                //$("#service-modal").html(Mustache.render(serviceTemplate, data));
                //console.log(Mustache.render(pricingtemplate, data));

                $(".payservice-" + id).html(Mustache.render(serviceTemplate, data));
                $(".genie-" + id).html(Mustache.render(genieTemplate, data));
                $(".genie-" + id).removeClass("hidden");
                $("." + id + "-req").html(jobStatus);
                $("#popup").click();
                $.LoadingOverlay("hide", true);
            } else if (bookingstatus === "ongoing") {
                var detailData = {}, resultdata;
                resultdata = results.data[0];
                var slot, date, month, day, year;
                slot = {
                    "8":"8AM - 10AM",
                    "10":"10AM - 12PM",
                    "12":"12PM - 2PM",
                    "14":"2PM - 4PM",
                    "16":"4PM - 6PM",
                    "18":"6PM - 8PM"
                };
                resultdata["slot"] = slot[resultdata["slot"]];
                resultdata.services = data.serviceBasedType;

                if (resultdata["payment"]["payment_type"] == "null") {
                    resultdata["payment"]["payment_type"] = null;
                    //console.log("here")
                }

                var date = results.data[0]["scheduleDate"];
                date = new Date(date);
                month = monthArray[date.getMonth()];
                day = dayArray[date.getDay()];
                year = date.getFullYear();
                date = date.getDate();
                resultdata.scheduleDate = day + " " + date + " " + month + " " + year + " | " + resultdata.slot;
                resultdata["dateScheduled"] = resultdata["slot"] + ", " + month + " " + date;

                var bookingdate = resultdata["utc_timing"]["requestedTime"];
                bookingdate = new Date(bookingdate).toLocaleString();
                bookingdate = bookingdate.split(",");
                resultdata["utc_timing"]["requestedTime"] = bookingdate;
                detailData.viewDetails = resultdata;

                var detailHtml = Mustache.render(viewDetailsTemplate, detailData);
                //console.log(detailHtml);
                $("#popupmodal").html(detailHtml);

                var jobStatus = results.data[0].status;
                var problemImage = results.data[0].problemImages;
                if (problemImage && problemImage.length > 0) {
                    var problemData = {}, problemImageArray = [], problemImageTemplate;

                    for (var k in problemImage) {
                        problemImageArray.push(problemImage[k].thumbnail);
                    }
                    problemData.problemImage = problemImageArray;

                    problemImageTemplate = '<p class="active-color">Uploaded Photo</p>\
                        {{#problemImage}}<div class="genie-box">\
                            <img src="{{.}}" class="img-responsive" alt="{{genieName}}" style="width:100%;">\
                        </div>{{/problemImage}}';

                    $(".problemImage-" + id).html(Mustache.render(problemImageTemplate,problemData));
                }
                $("#pricing-modal").html(Mustache.render(pricingtemplate, data));
                $("#address-" + id).html(Mustache.render(addressTemplate, data));
                //$(".payprice-" + id).html(Mustache.render(pricingtemplate, data));
                //$("#service-modal").html(Mustache.render(serviceTemplate, data));
                //console.log(Mustache.render(pricingtemplate, data));
                $("#pricingnote").html('<p style="margin-top:10px;">For more information, check our <a href="http://www.homegenie.me/pricing-policies/">pricing policy.</a></p>')
                $(".payservice-" + id).html(Mustache.render(serviceTemplate, data));
                $(".genie-" + id).html(Mustache.render(genieTemplate, data));
                $(".genie-" + id).removeClass("hidden");
                $("." + id + "-req").html(jobStatus);

                var cancelTemplate = '{{#id}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" onclick="cancelAppointment(\'{{id}}\',\'{{id}}-d\')">\
                            <p style="position:relative;top:7px;">CANCEL REQUEST</p>\
                        </div>{{/id}}';
                var paymentTemplate = '{{#id}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{id}}\', {{totalCharges}})" style="background: #67e9b4;">\
                    <p style="position:relative;top:7px;">PAY NOW</p>\
                </div>{{/id}}';
                if (jobStatus == "PAYMENT_PENDING" || jobStatus == "CANCELLED") {
                    data.totalCharges = resultdata.charges.totalCharges;
                    $("#finalstatus").html(Mustache.render(paymentTemplate, data));
                    $("#" + id + "-view").html('<span class="PENDING">PAYMENT PENDING</span><button class="pay-now" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-pay" onclick="getBookingDetails(\'' + id + '\', \'ongoing\')"><span class="booking-btn-align">PAY NOW</span></button>');
                } else if (jobStatus == "COMPLAINT_REGISTERED" || jobStatus == "IN_SERVICE" || jobStatus == "RATING" || jobStatus == "UNFINISHED") {
                    $("." + id + "-total").html(results.data[0].charges.estimateCharges);
                    $("#finalstatus").html("");
                    if (jobStatus == "RATING") {
                        $("#" + id + "-view").html('<span class="pull-right"><span class="active-color">RATING </span><button class="pay-now" onclick="window.location.href=\'html/rateGenie.html?genieid=' + id +'\'"></button></span><br/>\
                            <span class="pull-right"><span class="active-color"></span><button class="view-detail view-detail-ongoing" onclick="getBookingDetails(\'' + id + '\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>');
                    }
                    if (jobStatus == "IN_SERVICE") {
                        $("#" + id + "-cancel").html("");
                    }
                    if (jobStatus == "COMPLAINT_REGISTERED") {
                        $("." + id + "-total").html("To be decided");
                    }
                } else if (jobStatus == "REJECTED") {
                    $("." + id + "-total").html(results.data[0].charges.callOutCharges);
                    data.totalCharges = results.data[0].charges.callOutCharges;
                    $("#finalstatus").html(Mustache.render(paymentTemplate, data));
                } else if (jobStatus == "INSPECTION" || jobStatus == "RESCHEDULED") {
                    if (results.data[0].isInspectionCompleted) {
                        var data = {};
                        data.id = id;
                        var html = Mustache.render(acceptRejectTemplate, data);
                        $("#finalstatus").html(html);
                        $("." + id + "-total").html(results.data[0].charges.estimateCharges);
                    } else {
                        $("#" + id + "-finalstatus").html("");
                        $("#" + id + "-cancel").html("");
                        $("." + id + "-total").html("To be decided");
                    }
                } else if (jobStatus == "REQUESTED" || jobStatus == "ASSIGNED" || jobStatus == "ENROUTE") {
                    $("#finalstatus").html(Mustache.render(cancelTemplate, data));
                    if (data.serviceBasedType == "INSPECTION BASED SERVICE") {
                        $("." + id + "-total").html("To be decided");
                    } else {
                        $("." + id + "-total").html(results.data[0].charges.unitCharges);
                    }
                }
                $.LoadingOverlay("hide", true);
                $("#popup").click();
            }
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function contactGenie(id) {
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data.append("appointmentID",id);
    data._method = "POST";
    // $.growl.notice({message : "Genie will contact you soon."});
    var message = {"msg":"Genie will contact you soon."}
    $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
    $('.msg-modal').modal('show');
    $.LoadingOverlay("show");
    callApi.queryUrl(apiUrl + "/api/customer/calltoDriver", data, function(err, response){
        if (!err) {
            $.LoadingOverlay("hide", true);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

function cancelAppointment(id, modalid) {
    var data = {};
    data.Auth = udata.data.accessToken;
    $("#cancel").click();
    $("#cancel-confirm").click(function(){
        $.LoadingOverlay("show");
        callApi.getUrl(apiUrl + "/api/customer/JobCancelChargeCalculation?jobId=" + id, data, function(err, results){
            if (!err){
                var params = new FormData();
                params._method = "PUT";
                params.Auth = udata.data.accessToken;
                params.append("jobId", id);
                callApi.queryUrl(apiUrl + "/api/customer/JobCancelCharge", params, function(err,results){
                    if (!err) {
                        $("#cancelConfirm").modal("hide");
                        $("body").removeClass("modalOpen");
                        $("#" + modalid).modal("hide");
                        // $.growl.notice({message : "You have successfully cancel the appointment"});
                        var message = {"msg":"You have successfully cancel the appointment"}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                        getmybookings();
                    } else {
                        $.LoadingOverlay("hide", true);
                        callApi.error(err);
                    }
                });
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    });
}

function cancelPopup() {
    $("#cancelConfirm").modal("hide");
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
}

function hideCurrentModal(elem){
    //$("body").addClass("modalOpen");
    var id = ((((elem.parentNode).parentNode).parentNode).parentNode);
    id = $(id).attr("id");
    $("#" + id).modal("hide");
    setTimeout(function(){
        $("body").addClass("modal-open");
    },500);
}

function acceptOrRejectJob(id, status) {
    var data = new FormData();
    data.Auth = udata.data.accessToken;
    data._method = "PUT";
    data.append("jobId",id);
    data.append("status",status);
    if (status == "APPROVE") {
        $.LoadingOverlay("show");
        callApi.putUrl(apiUrl + "/api/customer/approveOrRejectJob/", data, function(err, results){
            if (!err){
                // $.growl.notice({message : "Your job has been updated successfully."});
                var message = {"msg":"Your job has been updated successfully."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                $("#popupmodal").modal("hide");
                $("body").removeClass("modalOpen");
                $.LoadingOverlay("hide", true);
                getmybookings();
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    } else {
        $.LoadingOverlay("show");
        callApi.putUrl(apiUrl + "/api/customer/approveOrRejectJob/", data, function(err, results){
            if (!err){
                // $.growl.notice({message : "Your job has been updated successfully."});
                var message = {"msg":"Your job has been updated successfully."}
                $('.msg-display').html(Mustache.render(MsgDisplayTemp, message));
                $('.msg-modal').modal('show');
                $("#popupmodal").modal("hide");
                $("body").removeClass("modalOpen");
                $.LoadingOverlay("hide", true);
                getmybookings();
            } else {
                $.LoadingOverlay("hide", true);
                callApi.error(err);
            }
        });
    }
}

function initOngoingBookingSearch(){
    $("#search-ongoing").keyup(function(){
        var bookingsearchinput = this.value;
        $('.ongoingbookingsearch').each(function () {
            var service = $(this).attr("data");
            if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                $(this).addClass('hidden');
            } else {
                $(this).removeClass('hidden');
            }
        });
    });
    $("#search-completed").keyup(function(){
        var bookingsearchinput = this.value;
        $('.completedbookingsearch').each(function () {
            var service = $(this).attr("data");
            if (service.substring(0, bookingsearchinput.length).toLowerCase().trim() !== bookingsearchinput.toLowerCase().trim()) {
                $(this).addClass('hidden');
            }
            else {
                $(this).removeClass('hidden');
            }
        });
    });

    $("#search-ongoing-clear").click(function(){
        $("#brandsearchinput").val('');
        $('.branditem').each(function () {
            $(this).removeClass('hidden');
        });
    });
}

function makePayment(id, amount) {
    var paymentdata = {};
    paymentdata.id = id;
    paymentdata.a = amount;
    paymentdata = JSON.stringify(paymentdata);
    createCookie("hg_p",paymentdata,1);
    window.location.href="../html/paymentmethod.html";
}

$(document).ready(function(){
    getmybookings();
});

var ongoingBooking_template =
    '<div class="col-xs-12 col-sm-12 col-md-12 column search-tab-div padding0">\
        <div class="form-group">\
            <div class="icon-addon addon-md">\
                <input type="text" placeholder="Search" class="form-control" id="search-ongoing">\
                <label for="search-ongoing" class="glyphicon glyphicon-search" rel="tooltip" title="search-ongoing"></label>\
            </div>\
        </div>\
    </div>\
    {{#ongoing}}<div class="col-xs-12 col-sm-12 col-md-12 column padding0 ongoing-list-border ongoingbookingsearch" data="{{subcategory.subCategoryName}}">\
        <div class="col-xs-12 col-sm-7 col-md-6 column padding0 ongoing-list vertical-align">\
            <div class="service-img-div vertical-align">\
                <img src="{{category.whiteImage.original}}" class="img-resposive">\
            </div>\
            <div class="service-booking-txt" style="margin-left:10px;">\
                <p>{{}} {{}}</p>\
                <p style="color:#a1a1a2;">{{}}</p>\
                <p>{{}}</p>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-5 col-md-6 column padding0">\
            <div class="btn-grp-pay pull-right">\
                {{#Payment}}<span class="pull-right" id="{{_id}}-view"><span class="{{payment_status}}">{{#payment_status}}Payment {{payment_status}}{{/payment_status}}</span><button class="pay-now" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-pay" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">PAY NOW</span></button></span><br/>{{/Payment}}\
                {{#Cancel}}<span class="pull-right" id="{{_id}}-cancel"><span class="active-color"></span><button class="pay-now" onclick="cancelAppointment(\'{{_id}}\')" style="margin-top:0px;"><span class="booking-btn-align">CANCEL</span></button></span><br/>{{/Cancel}}\
                {{#View}}<span class="pull-right" id="{{_id}}-view"><span class="active-color {{_id}}-req">{{status}}</span><button class="view-detail view-detail-ongoing" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>{{/View}}\
                {{#RateGenie}}<span class="pull-right"><span class="active-color">{{status}}</span><button class="pay-now" onclick="window.location.href=\'rateGenie.html?genieid={{_id}}\'"></button></span><br/>\
                <span class="pull-right"><span class="active-color"></span><button class="view-detail view-detail-ongoing" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-d" onclick="getBookingDetails(\'{{_id}}\', \'ongoing\')"><span class="booking-btn-align">VIEW DETAILS</span></button></span>{{/RateGenie}}\
                <span class="{{_id}}"></span>\
                <span class="{{_id}}-pay"></span>\
            </div>\
        </div>\
    </div>{{/ongoing}}';

var completedBooking_template =
    '<div class="col-xs-12 col-sm-12 col-md-12 column search-tab-div padding0">\
        <div class="form-group">\
            <div class="icon-addon addon-md">\
                <input type="text" placeholder="Search" class="form-control" id="search-completed">\
                <label for="search-completed" class="glyphicon glyphicon-search" rel="tooltip" title="search-completed"></label>\
            </div>\
        </div>\
    </div>\
    {{#completed}}<div class="col-xs-12 col-sm-12 col-md-12 column padding0 ongoing-list ongoing-list-border completedbookingsearch" data="{{subcategory.subCategoryName}}">\
        <div class="col-xs-12 col-sm-7 col-md-6 column padding0 ongoing-list vertical-align">\
            <div class="service-img-div vertical-align">\
                <img src="{{category.whiteImage.original}}" class="img-resposive">\
            </div>\
            <div class="service-booking-txt" style="margin-left:10px;">\
                <p>{{category.whiteImage.name}} {{subcategory.subCategoryName}}</p>\
                <p style="color:#a1a1a2;">{{dateScheduled}}</p>\
                <p>{{nickName}}</p>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-5 col-md-6 column padding0">\
            <div class="btn-grp-pay completed-grp-pay pull-right">\
                <span class="pull-right"><span class="active-color">{{status}}</span>\
                <button class="view-detail completed-details" data-id="{{_id}}" data-toggle="modal" data-target="#{{_id}}-d" onclick="getBookingDetails(\'{{_id}}\', \'past\')"><span class="booking-btn-align">VIEW DETAILS</span></button>\
                </span>\
                <span class="{{_id}}"></span>\
            </div>\
        </div>\
    </div>{{/completed}}';

var pricingtemplate = '{{#pricedetails}}<div class="modal-dialog">\
                                <div class="modal-content containerSize">\
                                    <div class="row clearfix">\
                                        <div class="col-xs-12 col-md-12 col-sm-12 container-margin border-pricing-detail" style="margin-top:20px;">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail"><span class="fa fa-tags"></span>  PRICING DETAILSt</p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 air-conditioning-position">\
                                                <p class="issue-booking air-conditioning">\
                                                    <img src="{{categoryImage.thumbnail}}" class="ac-img">\
                                                    {{categoryName}} ({{subCategory.subCategoryName}})\
                                                </p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-xs-12 col-md-4 col-sm-4 inspection">\
                                                <p class="">{{serviceBasedType}}</p>\
                                            </div>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 col-xs-12 margin-charges">\
                                            <p class="charges">Charges</p>\
                                            <p class="call-out-charges">{{serviceCharge}}<span class="aed" id="callOutCharges">{{seviceBasedPrice}} AED</span></p>\
                                            {{#hourlyCharge}}<p class="call-out-charges" id="hourlycharges">Additional Hour Charges<span class="active-color">{{hourlyCharge}} AED</span></p>{{/hourlyCharge}}\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 col-xs-12 note" id="pricingnote">\
                                            <p class="note-margin note-margin-pricing">Note:</p>\
                                            <p class="note-call-out-charges">* Call-out charge is a minimum charge that includes  21 hour of labor to inspect and diagnose the issue or/and perform minor repairs, if time permits\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12 column pointer geniemsg" onclick="hideCurrentModal(this);" style="padding:0px;">\
                                            <p class="text-center">CONTINUE</p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>{{/pricedetails}}';

var serviceTemplate = '{{#servicedetails}}<div class="modal-dialog">\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-xs-12 col-sm-12 col-md-12 border-service-detail" style="margin-top:20px;">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail"><span class="fa fa-info-circle"></span>  SERVICE DETAILS DEMO</p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 air-conditioning-position">\
                                                <p class="air-conditioning">\
                                                <img src="{{categoryImage.thumbnail}}" class="ac-img">\
                                                {{categoryName}} ({{subCategory.subCategoryName}})\
                                            </p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">SERVICE DETAILS DEMO!</p>\
                                            </div>\
                                            <div class="col-xs-12 col-sm-4 col-md-4 fixed-service">\
                                                <p class="">{{serviceBasedType}}</p>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12">\
                                            <p class="charges whats-included-margin">What\'s Included</p>\
                                            <p class="whats-included" id="service-included">{{#Notes1}}-{{.}}<br/>{{/Notes1}}</p>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 col-xs-12 note note-font-servicedetails">\
                                            <p class="note-margin note-margin-top hidden">Note:-</p>\
                                            <p class="note-margin" id="serviceDetails-note" style="margin-top:0px;">Note{{#Notes2}}-{{.}}<br/>{{/Notes2}}</p>\
                                        </div>\
                                        <div class="col-xs-12 col-sm-12 col-md-12 column pointer geniemsg" onclick="hideCurrentModal(this)" style="padding:0px;">\
                                            <p class="text-center">CONTINUE</p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>{{/servicedetails}}';

var genieTemplate = '{{#genie}}<div class="genie-name-box detail-genie-name-box">\
                            <div class="col-xs-12 col-sm-2 col-md-2 column details-genie-img">\
                                <img src="{{profilePicURL.thumbnail}}" class="img-responsive genie-img">\
                            </div>\
                        <div class="hidden-xs col-sm-10 col-md-10 column">\
                            <p class="details-genie-name">{{name}} {{#favouriteGenie}}(favourite genie){{/favouriteGenie}}</p>\
                            <fieldset class="rating rating-detail-genie">\
                                <input type="radio" id="{{_id}}-5" name="rating" value="5" /><label class = "full" for="{{_id}}-5"></label>\
                                <input type="radio" id="{{_id}}-4" name="rating" value="4" /><label class = "full" for="{{_id}}-4"></label>\
                                <input type="radio" id="{{_id}}-3" name="rating" value="3" /><label class = "full" for="{{_id}}-3"></label>\
                                <input type="radio" id="{{_id}}-2" name="rating" value="2" /><label class = "full" for="{{_id}}-2"></label>\
                                <input type="radio" id="{{_id}}-1" name="rating" value="1" /><label class = "full" for="{{_id}}-1"></label>\
                            </fieldset>\
                            <br/>\
                        </div>\
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
                            <div class="row clearfix text-center">\
                                <div class="col-xs-12 column">\
                                    <p class="details-genie-name">{{name}}</p>\
                                </div>\
                                <div class="col-xs-12 column">\
                                    <fieldset class="rating rating-delete-genie">\
                                        <input type="radio" id="{{_id}}-10d" name="rating" value="5" /><label class = "full" for="{{_id}}-10d"></label>\
                                        <input type="radio" id="{{_id}}-9d" name="rating" value="4" /><label class = "full" for="{{_id}}-9d"></label>\
                                        <input type="radio" id="{{_id}}-8d" name="rating" value="3" /><label class = "full" for="{{_id}}-8d"></label>\
                                        <input type="radio" id="{{_id}}-7d" name="rating" value="2" /><label class = "full" for="{{_id}}-7d"></label>\
                                        <input type="radio" id="{{_id}}-6d" name="rating" value="1" /><label class = "full" for="{{_id}}-6d"></label>\
                                    </fieldset>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <button class="detail-contact" onclick="contactGenie(\'{{id}}\')">CONTACT GENIE</button>{{/genie}}';

var addressTemplate = '{{#address}}<span>{{addressType}}</span><br/>\
                        <span>{{apartmentNo}}, {{streetAddress}}</span><br/>\
                        <span>{{city}}</span><br/>\
                        <span>{{emirates}}</span>{{/address}}';

var viewDetailsTemplate =
    '{{#viewDetails}}<!--<div id="popupmodal{{#Accept}}{{_id}}-d{{/Accept}}{{#Details}}{{_id}}-d{{/Details}}{{#cancelRequest}}{{_id}}-d{{/cancelRequest}}{{#completeSettled}}{{_id}}-d{{/completeSettled}}{{#payNow}}{{_id}}-pay{{/payNow}}{{#paynow}}{{_id}}-d{{/paynow}}{{#Inspected}}{{_id}}-d{{/Inspected}}" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">-->\
        <div class="modal-dialog job-details-dialog">\
            <div class="modal-content job-details-modal">\
            <div class="row clearfix" style="margin:0px;background:#fff;">\
                <div class="col-xs-12 col-sm-12 col-md-12 column job-assignee-row">\
                    <span class="job-detail-id">\
                        <span class="active-color">JOB DETAILS(JOB ID: {{uniqueCode}}) | </span>\
                        <span>JOB STATUS: </span><span class="assigned {{_id}}-req">{{status}}</span>\
                    </span>\
                    <span class="bookingdate">{{utc_timing.requestedTime}}</span>\
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column issue-pricing-row">\
                    <div class="col-xs-12 col-sm-12 col-md-7 column issue-instruction">\
                        <p class="service-p">\
                            <img src="{{categoryImage.original}}" class="service-imgs service-img-{{_id}}" id="service-img-{{_id}}"> {{categoryName}}\
                            <span class=""><br/></span><span class="issue-detail"><span class="active-color">Issue </span><span class="pull-right">{{subCategory.subCategoryName}}</span></span>\
                            <br/>\
                            <span class="datetime-left"><span class="active-color">Date{{#payNow}} and Time{{/payNow}}{{#cancelRequest}} and Time{{/cancelRequest}}{{#Details}} and Time{{/Details}} </span><span class="pull-right">{{scheduleDate}}</span></span>\
                        </p>\
                        <div class="col-xs-12 col-sm-12 col-md-12 column instruction-row" style="padding-left:20px;padding-right:0px">\
                            {{#problemDetails}}<p class="active-color">Instructions</p>\
                            <p>{{problemDetails}}</p>{{/problemDetails}}\
                        <span class=problemImage-{{_id}}></span>\
                        <p class="active-color">Address (Default)</p>\
                        <span class="address-{{_id}}" id="address-{{_id}}">{{references.addressID.addressType}}</span><br/>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-12 column genie-{{_id}} hidden" id="genie-{{_id}}" style="padding-bottom:10px;">\
                        <div class="genie-name-box detail-genie-name-box">\
                            <div class="col-xs-12 col-sm-2 col-md-2 column details-genie-img">\
                                <img src="{{genieImage}}" class="img-responsive genie-img">\
                            </div>\
                        <div class="hidden-xs col-sm-10 col-md-10 column">\
                            <p class="details-genie-name">{{genieName}} (favourite genie)</p>\
                            <fieldset class="rating rating-detail-genie">\
                                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5"></label>\
                                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4"></label>\
                                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3"></label>\
                                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2"></label>\
                                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1"></label>\
                            </fieldset>\
                            <br/>\
                        </div>\
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
                            <div class="row clearfix text-center">\
                                <div class="col-xs-12 column">\
                                    <p class="details-genie-name">{{genieName}}</p>\
                                </div>\
                                <div class="col-xs-12 column">\
                                    <fieldset class="rating rating-delete-genie">\
                                        <input type="radio" id="star11" name="rating" value="5" /><label class = "full" for="star11"></label>\
                                        <input type="radio" id="star10" name="rating" value="4" /><label class = "full" for="star10"></label>\
                                        <input type="radio" id="star9" name="rating" value="3" /><label class = "full" for="star9"></label>\
                                        <input type="radio" id="star7" name="rating" value="2" /><label class = "full" for="star7"></label>\
                                        <input type="radio" id="star6" name="rating" value="1" /><label class = "full" for="star6"></label>\
                                    </fieldset>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <button class="detail-contact">CONTACT GENIE</button>\
                </div>\
            </div>\
            <div class="col-xs-12 col-sm-12 col-md-5 column summary-details-row">\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>Service Summary</span>\
                        <span class="pull-right active-color pointer" data-toggle="modal" data-target="#service-modal{{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}">\
                            <span class="fa fa-info-circle"></span> View Service Details\
                        </span>\
                        <div id="service-modal{{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}" class="modal fade {{#Accept}}service-{{_id}}{{/Accept}}{{#Details}}service-{{_id}}{{/Details}}{{#completeSettled}}service-{{_id}}{{/completeSettled}}{{#cancelRequest}}service-{{_id}}{{/cancelRequest}}{{#paynow}}service-{{_id}}{{/paynow}}{{#payNow}}payservice-{{_id}}{{/payNow}}" role="dialog">\
                            <div class="modal-dialog">\
                                <span class="cross"><img class="crossImg" src="../images/Cross.png"></span>\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-sm-12 col-md-12 border-service-detail">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail">SERVICE DETAILS DEMO2</p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 air-conditioning-position">\
                                                <p class="air-conditioning">\
                                                <img src="" class="ac-img">\
                                                \
                                            </p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">SERVICE DETAILS DEMO3</p>\
                                            </div>\
                                            <div class="col-sm-4 col-md-4 fixed-service">\
                                                <p class=""></p>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12">\
                                            <p class="charges whats-included-margin">What\'s Included</p>\
                                            <p class="whats-included" id="service-included"></p>\
                                        </div>\
                                        <div class="col-sm-12 col-md-12 note note-font-servicedetails">\
                                            <p class="note-margin note-margin-top">Note:-</p>\
                                            <p class="note-margin" id="serviceDetails-note"></p>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </p>\
                    <p><span class="active-color">Type</span> <span class="pull-right">{{services}}</span></p>\
                    <p><span class="active-color">Priority</span> <span class="pull-right">Standard (next day)</span></p>\
                    <p><span class="active-color">Frequency</span><span class="pull-right">One time</span></p>\
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>Pricing</span>\
                        <span class="pull-right active-color pointer" data-toggle="modal" data-target="#pricing-modal{{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}">\
                            <span class="fa fa-tags"></span> View Pricing Info\
                        </span>\
                        <div id="pricing-modal{{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}" class="modal fade {{#Accept}}price-{{_id}}{{/Accept}}{{#Details}}price-{{_id}}{{/Details}}{{#cancelRequest}}price-{{_id}}{{/cancelRequest}}{{#paynow}}price-{{_id}}{{/paynow}}{{#completeSettled}}price-{{_id}}{{/completeSettled}}{{#payNow}}payprice-{{_id}}{{/payNow}}" role="dialog" data-keyboard="false" data-backdrop="static">\
                            <div class="modal-dialog">\
                                <span class="cross"><img class="crossImg" src="../images/Cross.png"></span>\
                                <div class="modal-content containerSize ">\
                                    <div class="row clearfix">\
                                        <div class="col-md-12 col-sm-12 container-margin border-pricing-detail">\
                                            <div class="col-xs-12 no-padding hidden-sm hidden-md hidden-lg">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 air-conditioning-position">\
                                                <p class="issue-booking air-conditioning">\
                                                    <img src="" class="ac-img">\
                                                    \
                                                </p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 no-padding hidden-xs">\
                                                <p class="pricing-service-detail">PRICING DETAILS</p>\
                                            </div>\
                                            <div class="col-md-4 col-sm-4 inspection">\
                                                <p class=""></p>\
                                            </div>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 margin-charges">\
                                            <p class="charges">Charges</p>\
                                            <p class="call-out-charges">Call Out Charges*  :  <span class="aed" id="callOutCharges">{{charges.callOutCharges}} AED</span></p>\
                                        </div>\
                                        <div class="col-md-12 col-sm-12 note" id="pricingnote">\
                                            <p class="note-margin note-margin-pricing">Note:</p>\
                                            <p class="note-call-out-charges">* Call-out charge is a minimum charge that includes  12 hour of labor to inspect and diagnose the issue or/and perform minor repairs, if time permits\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </p>\
                    <p class="active-color"><span>Item</span><span class="pull-right">AED</span></p>\
                    <p><span>Labor (Including callout)</span><span class="pull-right">{{charges.labourCharges}}</span></p>\
                    <p><span>Material</span><span class="pull-right">{{charges.materialCharges}}</span></p>\
                    <p><span>Other Charges</span><span class="pull-right">{{charges.unitCharges}}</span></p>\
                    <p><span>(less) Discount (PROMO)</span><span class="pull-right">{{charges.discountCharges}}</span></p>\
                    <p><span>TOTAL</span><span class="pull-right active-color {{_id}}-total" id="{{_id}}-total">{{charges.totalCharges}}</span></p>\
                </div>\
                <div class="col-xs-12 col-sm-12 col-md-12 column padding0">\
                    <p class="summary-service">\
                        <span>Payment</span>\
                    </p>\
                    <p><span>Plan</span><span class="pull-right active-color">On Completion</span></p>\
                    {{#payment.payment_type}}<p><span>Method</span><span class="pull-right active-color">{{payment.payment_type}}</span></p>{{/payment.payment_type}}\
                    <p><span>Status</span><span class="pull-right active-color">{{payment_status}}</span></p>\
                </div>\
            </div>\
        </div>\
        <span id="finalstatus"><span>\
        {{#cancelRequest}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" onclick="cancelAppointment(\'{{_id}}\',\'{{#cancelRequest}}{{_id}}-d{{/cancelRequest}}\')">\
            <p style="position:relative;top:7px;">CANCEL REQUEST</p>\
        </div></span>{{/cancelRequest}}\
        {{#paynow}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{_id}}\', {{charges.totalCharges}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">PAY NOW</p>\
        </div></span>{{/paynow}}\
        {{#Accept}}<span id="{{_id}}-finalstatus"><div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'APPROVE\')" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">ACCEPT</p>\
        </div>\
        <div class="col-xs-6 col-sm-6 col-md-6 column text-center cancel-req pointer" data-id="{{_id}}" onclick="acceptOrRejectJob(\'{{_id}}\', \'REJECTED\')">\
            <p style="position:relative;top:7px;">REJECT</p>\
        </div></span>{{/Accept}}\
        {{#payNow}}<span id="{{_id}}-finalstatus"><div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id="{{_id}}" onclick="makePayment(\'{{_id}}\', {{charges.totalCharges}})" style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">PAY NOW</p>\
        </div></span>{{/payNow}}\
        <!--{{#completeSettled}}<div class="col-xs-12 col-sm-12 col-md-12 column text-center cancel-req pointer" data-id={{_id}} style="background: #67e9b4;">\
            <p style="position:relative;top:7px;">SETTLED</p>\
        </div>{{/completeSettled}}-->\
    </div>\
    </div></div>\
    <!--</div>-->{{/viewDetails}}';
