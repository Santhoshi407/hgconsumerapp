/**
 * Created by sumit on 19/10/16.
 */
var udata,
    monthArray = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function updateAllComplaintList(result) {
    var data = {}, html;
    var len = result.data;
    len = len.length;
    if (len > 0) {
        for(var k in result.data) {
            if (result.data[k]["driverImage"]["thumbnail"] === null) {
                result.data[k]["driverImage"]["thumbnail"] = "../images/genieicon.png"
            }
            var date = result.data[k]["scheduleDate"];
            date = new Date(date).getDate() + " " + monthArray[new Date(date).getMonth()] + ", " + new Date(date).getFullYear();
            result.data[k]["scheduleDate"] = date;
            var genieStar = result.data[k]["avgDriverRating"];
            var genieStarObj = {};
            for (var count = 0; count < genieStar; count++) {
                genieStarObj[count] = "star-filled";
            }
            var keys = Object.keys(genieStarObj);
            if(keys.length < 5) {
                var genieStarArrayLength = keys.length;
                var leftStar = 5 - genieStarArrayLength;
                for (var count = 0; count < leftStar; count++) {
                    genieStarObj[genieStarArrayLength + count] = "star-blank";
                }
            }
            result.data[k]["genieStar"] = genieStarObj;
            //result.data[k]["noOfStar"] = genieStar;
        }
        data.mycomplaint = result.data;
        html = Mustache.render(complaints_template, data);
        $("#mycomplaint").html(html);
        $.LoadingOverlay("hide", true);
    } else {
        $("#mycomplaint").html("<p class='text-center' style='font-size:20px;margin-top:10px;'>You don't have any complaints registered yet.</p>");
        $.LoadingOverlay("hide", true);
    }
}
function getComplaint() {
    var params = {};
    var userData = localStorage.getItem("h_user");
    if (userData && userData !== 'null') {
        udata = JSON.parse(userData);
        updateUserInfo(udata);
    } else {
        localStorage.setItem("h_user",null);
        window.location.href = "../index.html";
    }
    params.Auth = udata.data.accessToken;
    $.LoadingOverlay("show");
    callApi.getUrl(apiUrl + "/api/customer/getAllComplaintsList/", params, function(err, result){
        if (!err) {
            updateAllComplaintList(result);
        } else {
            $.LoadingOverlay("hide", true);
            callApi.error(err);
        }
    });
}

$(document).ready(function(){
    getComplaint();
});

var complaints_template =
    '{{#mycomplaint}}<div class="col-xs-12 col-sm-12 col-md-12 column add-review-list complaint-list" style="color:#737373;">\n' +
    '                            <div class="col-xs-12 col-sm-5 col-md-5 column address-col pad0-xs">\n' +
    '                                <div class="genie-name-box complaints-genie-name-box">\n' +
    '                                    <div class="col-xs-4 col-sm-2 col-md-2 column genie-img-div delete-genie-div complaint-genie">\n' +
    '                                        <img src="{{driverImage.thumbnail}}" class="img-responsive genie-img"/>\n' +
    '                                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg column padding0">\n' +
    '                                            <p class="genie-name" style="margin-top:7px;">{{driverName}}</p>\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg column padding0">\n' +
    '                                            {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:12px;"/>\n' +
    '                                            <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:12px;"/>\n' +
    '                                            <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:12px;"/>\n' +
    '                                            <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:12px;"/>\n' +
    '                                            <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:12px;"/>\n' +
    '                                            <span style="position:relative;top:-3px;left:5px;">({{avgDriverRating}}/5)</span>{{/genieStar}}\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="hidden-xs col-sm-10 col-md-10 column">\n' +
    '                                        <p class="genie-name">{{driverName}}</p>\n' +
    '                                        {{#genieStar}}<img src="../images/{{genieStar.0}}.png" style="margin-bottom:10px;max-width:15px;"/>\n' +
    '                                        <img src="../images/{{genieStar.1}}.png" style="margin-bottom:10px;max-width:15px;"/>\n' +
    '                                        <img src="../images/{{genieStar.2}}.png" style="margin-bottom:10px;max-width:15px;"/>\n' +
    '                                        <img src="../images/{{genieStar.3}}.png" style="margin-bottom:10px;max-width:15px;"/>\n' +
    '                                        <img src="../images/{{genieStar.4}}.png" style="margin-bottom:10px;max-width:15px;"/>\n' +
    '                                        <span style="position:relative;top:-3px;left:5px;">({{avgDriverRating}}/5)</span>{{/genieStar}}\n' +
    '                                        <br/>\n' +
    '                                        <span class="delete-genie-issue" style="position:relative;top:-5px;"> \n' +
    '                                           <img src="{{categoryBlueImage.original}}" class="img-responsive cat-img"/> \n' +
    '                                           <p class="issue issue-support" style="left:40px;">{{categoryName}} ({{subCategoryName}})</p> \n' +
    '                                        </span>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-8 hidden-sm hidden-md hidden-lg" style="padding:0px;">\n' +
    '                                        <div class="clearfix text-center">\n' +
    '                                            <div class="col-xs-12 hidden-sm hidden-md hidden-lg column" style="padding:0px;height:28px;">\n' +
    '                                                <div class="vertical-align" style="float:right;max-width:85%;">\n' +
    '                                                    <img src="{{categoryBlueImage.original}}" class="img-responsive cat-img fav-cat-img"/>\n' +
    '                                                    <p class="" style="margin-left:5px;font-size:16px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;margin-bottom:0px;">\n' +
    '                                                        <span>{{categoryName}} ({{subCategoryName}})</span>\n' +
    '                                                    </p>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-xs-12 col-sm-8 col-md-8 column complaint-box padding0" style="text-align:right;">\n' +
    '                                                <div class="complaint-text">\n' +
    '                                                    <p><span class="active-color">Job ID : </span>{{uniqueCode}}</p>\n' +
    '                                                    <p><span class="active-color">Date : </span>{{scheduleDate}}</p>\n' +
    '                                                    <p>{{complaints}}</p>\n' +
    '                                                </div>\n' +
    '                                                <span class="span-float resolved-complaints {{status}}" style="margin-top:25px;">{{status}}</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="hidden-xs col-sm-7 col-md-7 column complaint-box">\n' +
    '                                <div class="complaint-text">\n' +
    '                                    <p><span class="active-color">Job ID : </span>{{uniqueCode}}</p>\n' +
    '                                    <p><span class="active-color">Date : </span>{{scheduleDate}}</p>\n' +
    '                                    <p>{{complaints}}</p>\n' +
    '                                </div>\n' +
    '                                <span class="span-float resolved-complaints {{status}}" style="margin-top:25px;">{{status}}</span>\n' +
    '                            </div>\n' +
    '                        </div>{{/mycomplaint}}';